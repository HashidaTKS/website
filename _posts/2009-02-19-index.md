---
tags: []
title: Webエンジニア武勇伝インタビュー
---
先日、[ウェブキャリア](http://www.web-career.com/)の[Webエンジニア武勇伝](http://www.web-career.com/contents/buyuden/)の[インタビュー](http://www.web-career.com/kawai/2009/02/post-247.html)を受けました。Webエンジニア武勇伝には[ささださん](http://www.web-career.com/contents/buyuden/18.html)や[高橋さん](http://www.web-career.com/contents/buyuden/20_1.html)、[かくたにさん](http://www.web-career.com/contents/buyuden/26.html)、そして、[おぎのさん](http://www.web-career.com/contents/buyuden/32_1.html)までもが登場しているので、Ruby界隈の人なら一度は目にしたことがあるのではないでしょうか。（他にもRuby界隈で有名な人が登場しています。）
<!--more-->


きっかけは、[東京Ruby会議01](http://regional.rubykaigi.org/tokyo01)で[Rabbitについて話した](http://www.nicovideo.jp/watch/sm4368086)（[スライド](http://pub.cozmixng.org/~kou/archives/TokyoRubyKaigi01/)）ことでした。そのときに[気になった](http://www.web-career.com/kawai/2008/08/ruby01.html)とのことで、声をかけてもらいました。それが、先日のインタビューにつながっています。

公開は数ヶ月先とのことですが、それまで待てないという人は明日の[オープンソースカンファレンスでの展示スペース]({% post_url 2009-02-02-index %})に遊びにきて、声をかけてください。待っています。
