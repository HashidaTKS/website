---
tags:
- milter-manager
title: 2009年5月のmilter manager
---
1.0.0リリース時のリビジョンがr2898で現在のリビジョンがr3001と、1.0.0リリース後も継続して開発されている[milter manager](/software/milter-manager.html)ですが、5月は、開発だけではなくmilter managerの話をする機会もあります。5/19と5/26の2回です。
<!--more-->


### IAjapan 第7回 迷惑メール対策カンファレンス

5/19の方は財団法人インターネット協会主催の[IAjapan 第7回 迷惑メール対策カンファレンス](http://www.iajapan.org/anti_spam/event/2009/conf0519/)です。

<dl>






<dt>






日時






</dt>






<dd>


2009年5月19日(火) 10:00-16:20


</dd>








<dt>






場所






</dt>






<dd>


[コクヨホール](http://www.kokuyo.co.jp/showroom/hall/about/)


</dd>








<dt>






定員






</dt>






<dd>


280名


</dd>








<dt>






対象






</dt>






<dd>


  * メールサーバ管理者/メールサービス担当者
  * 迷惑メール対策の現状と将来に興味がある方


</dd>


</dl>

13:20-14:20の「３. 送信ドメイン認証活用に向けて」で「milterの有効活用 - milter manager」というタイトルで30分話します。同じ枠で、送信ドメイン認証SPF/Sender ID Framework/DKIM/DKIM ADSPに対応したmilterである[ENMA](http://enma.sourceforge.net/)の話も聞けます。

また、続く14:35-16:05の「４.【Ｑ＆Ａパネルディスカッション】送信ドメイン認証を導入にあたって」にもパネリストとして参加します。

迷惑メール対策に関心のある方は参加してみてはいかがでしょうか？

[トピックス: IAjapan第7回迷惑メール対策カンファレンスで講演](/topics/20090501-iajapan-anti-spam7.html)

### IPAX2009

5/26の方はIPA主催の[IPAX2009](http://www.ipa.go.jp/event/ipax2009/)です。

<dl>






<dt>






日時






</dt>






<dd>


  * 2009年5月26日（火）10:00〜17:30
  * 2009年5月27日（水）10:00〜17:00


</dd>








<dt>






場所






</dt>






<dd>


東京ドームシティ プリズムホール


</dd>


</dl>

milter managerはIPAのオープンソフトウェア事業による成果であるため、今回出展することになりました。オープンソフトウェア事業以外にも未踏に参加した人たちも出展しているので、技術的におもしろいことが聞けるのではないかと思います。

出展者のプレゼンテーション枠があり、5月26日11:55〜12:55の枠の中で10分間milter managerの話をします。ブースは両日とも出展していますので、ご来場の際は一声かけてください。

[トピックス: IPAX2009に出展](/topics/20090501-ipax2009.html)

### まとめ

このような機会がきっかけとなって、milter managerに興味を持ってくれる人が増えると嬉しいですね。
