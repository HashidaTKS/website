---
tags:
- ruby
title: Ruby 1.9.1とREXMLとXML宣言のエンコーディング
---
Ruby 1.9.1付属のREXMLではXML宣言のエンコーディングの扱いに問題があるためvalidなXMLでもパースできない場合があるという話です。
<!--more-->


### 問題

Ruby 1.9では文字列や正規表現がエンコーディング情報を持つため、REXMLのように正規表現ベースでXMLをパースしている場合は、エンコーディングを適切に設定しないとパースに失敗することがあります。

例えば、[tDiaryのseach-yahoo.rbプラグインがこの問題に遭遇](http://www.cozmixng.org/retro/projects/tdiary/tickets/155)しています。

### 原因

REXMLは内部でUTF-8を用いています。そのため、パース対象のXMLのエンコーディングをUTF-8に変換しながらパースします。この処理はREXML::SourceまたはREXML::IOSourceで行われます。

しかし、REXML::IOSourceに問題があり、UTF-8に変換しないままパースしてしまう場合があります。これは、入力XMLのエンコーディングがUTF-8に設定されていない、かつ、XML宣言のエンコーディングがUTF-8になっている場合です。ちなみに、REXML::Sourceではこの問題は起きません。

tDiaryのsearch-yahoo.rbでは入力XMLのエンコーディングがASCII-8BITでXML宣言のエンコーディングがUTF-8になっていたため問題に遭遇しました。

search-yahoo.rbではopen-uriを使って入力XMLをHTTP経由で取得しています。open-uriはContent-Typeを見て適切なエンコーディングを設定してくれますが、今回はcharsetが指定されていなかったとのことです。このため、open-uriで取得した入力XMLがASCII-8BITになっていました。

{% raw %}
```ruby
xml = open("http://.../xxx.xml") {|f| f.read}
xml.encoding # => ASCII-8BIT
document = REXML::Document.new(xml) # => パースエラー
```
{% endraw %}

### 解決法

この問題に遭遇してしまった場合は、以下のような解決法があります。

  * 入力XMLのエンコーディングをUTF-8に設定する。
  * REXML::IOSourceの代わりにREXML::Sourceを使う。
  * [パッチ付きでバグ報告済み](http://redmine.ruby-lang.org/issues/show/1449)なので修正されるのを待つ。

入力XMLのエンコーディングをUTF-8に設定する場合は以下のようになります。

{% raw %}
```ruby
xml = open("http://.../xxx.xml") {|f| f.read}
xml.force_encoding("utf-8")
document = REXML::Document.new(xml)
```
{% endraw %}

REXML::Sourceを使う場合は以下のようになります。

{% raw %}
```ruby
xml = open("http://.../xxx.xml") {|f| f.read}
document = REXML::Document.new(REXML::Source.new(xml))
```
{% endraw %}

修正されるのを待つ場合は、修正されるまで待ってください。

### まとめ

Ruby 1.9で正規表現ベースのコードがうまく動かない場合はマッチ対象の文字列のエンコーディングを確認しましょう。

ちなみに、REXML::IOSource#matchではエンコーディング関係のエラーを握りつぶしているため、実際に発生するREXML::ParseExceptionだけ見てもエンコーディングミスマッチがどこで起こっているかはわかりません。問題が発生したときは[問題解決につながるエラーメッセージ]({% post_url 2009-04-10-index %})を提供したいものですね。
