---
tags:
- ruby
title: 日本Ruby会議2009の資料公開
---
[日本Ruby会議2009で発表]({% post_url 2009-07-06-index %})した資料を公開しました。
<!--more-->


  * [ActiveLdap](/archives/RubyKaigi2009/activeldap/)
  * [CとRubyとその間](/archives/RubyKaigi2009/c-and-ruby-and-between-them/)

発表を聞いてくれたみなさん、ブースにきてくれたみなさん、スタッフのみなさん、ありがとうございました。
