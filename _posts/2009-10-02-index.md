---
tags:
- ruby
title: Ruby/groonga 0.0.7
---
[Ruby/groongaとActiveGroonga](http://groonga.rubyforge.org/)の新しいバージョンがリリースされました。
<!--more-->


いつも通り、以下のコマンドでインストールできます。

{% raw %}
```
% sudo gem install groonga
```
{% endraw %}

0.0.7は最新の[groonga](http://groonga.org/)0.1.4に対応しています。

groongaが正式リリース前なので、まだRuby/groongaユーザもあまり多くはありませんが、徐々に使われはじめています。例えば、[えにしテック](http://www.enishi-tech.com/)の[スープカレー好き](http://ascii.jp/elem/000/000/464/464469/)の[daraさん](http://d.hatena.ne.jp/darashi/)が作った[buzztter](http://buzztter.com/)でRuby/groongaが使われています。

daraさんからはRuby/groongaに対するパッチをいくつかもらったりもしたので、Ruby/groongaのコミッタになってもらいました。APIの相談にものってくれる頼もしいCTOです。

今回のリリースでも便利な機能が入っているので、いくつか紹介します。

### キーワードリンク

groongaを使って[はてなのようなキーワードリンクをRubyで付与する](http://d.hatena.ne.jp/tasukuchan/20090810/senna_autolink_ruby)ことができます。

グニャラくんのところではSennaを使っていますが、同様の機能をRuby/groongaにも取り込みました。

Ruby/groongaを使うと以下のように書けます。

{% raw %}
```ruby
# -*- coding: utf-8 -*-
require 'groonga'

Groonga::Context.default_options = {:encoding => "utf-8"}
Groonga::Database.create
words = Groonga::PatriciaTrie.create(:key_type => "ShortText",
                                     :key_normalize => true)
words.add('リンク')
words.add('リンクの冒険')
words.add('冒険')
words.add('㍊')
words.add('ｶﾞｯ')
words.add('ＭＵＴＥＫＩ')
text = 'muTEkiなリンクの冒険はミリバールでガッ'
tagged_text = words.tag_keys(text) do |record, word|
  "[#{word}(#{record.key})]"
end
puts tagged_text
  # => [muTEki(muteki)]な[リンクの冒険(リンクの冒険)]は
  #    [ミリバール(ミリバール)]で[ガッ(ガッ)]
```
{% endraw %}

### クエリからスニペット生成

groongaでは独自の構文のクエリから検索条件を指定することができます。buzztterで検索条件を指定するところでも使われています。例えば、以下のような構文があります。

  * 「単語1 単語2」: 単語1と単語2の両方にマッチする条件
  * 「単語1 OR 単語2」: 単語1または単語2にマッチする条件
  * 「単語1 - 単語2」: 単語1にマッチするが単語2にマッチしない条件

もう少し詳しい説明は[groongaのチュートリアル](http://github.com/groonga/groonga/blob/master/doc/ja/TUTORIAL)に載っています。

ここまでは前のリリースでもできたところです。今回のリリースからはさらにスニペット（検索語周辺のテキスト）も簡単に生成できるようになりました。

{% raw %}
```ruby
# "description"カラムに「ruby」または「groonga」が入っているレコードを検索
query = "ruby OR groonga"
records = table.select do |record|
  record["description"].match(query)
end

# 「ruby」または「groonga」が含まれる周辺のテキストを表示
tags = [["<", ">"]]
records.each do |record|
  puts record["name"]
  snippet = records.expression.snippet(tags, :normalize => true)
  snippet.execute(record["description"]).each do |text|
    puts "==="
    puts record["description"] # => Rubyでgroonga使って全文検索
    puts "---"
    puts text                  # => <Ruby>で<groonga>使って全文検索
  end
end
```
{% endraw %}

どのようになるかは[Ruby/groongaのサンプルアプリケーション](/search/)で試してみてください。ここの検索ボックスもクエリ文字列に対応しているので、「OR」や「-」を使ったクエリを使うことができます。

### まとめ

Ruby/groonga 0.0.7の新機能を紹介しました。

groongaの機能を手軽に使えるRuby/groongaを試してみてはいかがでしょうか。
