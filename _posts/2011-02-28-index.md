---
tags:
- ruby
- test
title: デバッグしやすいassert_equalの書き方
---
デバッグしやすいassert_equalの書き方とデバッグしにくいassert_equalの書き方があるのは知っていますか？[^0]
<!--more-->


デバッグしやすいassert_equalの書き方を2パターン紹介します。

### まとめたassert_equal

まず、1つ目のよくみるデバッグしにくいassert_equalの書き方です。

{% raw %}
```ruby
def test_parse
  assert_equal(29, parse_integer("29"))   # (1)
  assert_equal(29, parse_integer("+29"))  # (2)
  assert_equal(-29, parse_integer("-29")) # (3)
end
```
{% endraw %}

これがデバッグしにくいのは、(1)が失敗したら(2)、(3)が実行されないからです。すべてのassert_equalが実行されて、どのassert_equalが失敗したかを確認することでバグの傾向を見つけやすくなります。

例えば…(1)だけが失敗するなら符号が無い場合の処理が怪しいでしょう。(3)だけが失敗するなら、マイナス記号に対応していないのかもしれません。全部失敗するなら根本的におかしいのでしょう。

このように、どのassert_equalが失敗したかがデバッグのヒントになります。よって、↑のような書き方はデバッグがしにくいassert_equalの書き方といえます。

デバッグしやすく書くと、例えば、こうなります。

{% raw %}
```ruby
def test_parse_no_sign
  assert_equal(29, parse_integer("29"))
end

def test_parse_plus_sign
  assert_equal(29, parse_integer("+29"))
end

def test_parse_mius_sign
  assert_equal(-29, parse_integer("-29"))
end
```
{% endraw %}

### まとめないassert_equal

続いて、1つ目のよくみるデバッグしにくいassert_equalの書き方です。

{% raw %}
```ruby
def test_parse
  bob = MyJSONParser.parse('{"name": "bob", "age": 29}')
  assert_equal("bob", bob["name"])
  assert_equal(29, bob["age"])
end
```
{% endraw %}

1つ目の書き方のように複数のassert_equalが書いてあります。1つ目の書き方と同じように直すならこうなります。

{% raw %}
```ruby
def test_parse_string_value
  bob = MyJSONParser.parse('{"name": "bob", "age": 29}')
  assert_equal("bob", bob["name"])
end

def test_parse_numeric_value
  bob = MyJSONParser.parse('{"name": "bob", "age": 29}')
  assert_equal(29, bob["age"])
end
```
{% endraw %}

でも、本当にこれでよいでしょうか。この書き方では値の型だけを注目しています。値の型に注目するならば以下のように書いた方がよいでしょう。こうした方が余計なものがなくなり、より注目できています。

{% raw %}
```ruby
def test_parse_string_value
  bob = MyJSONParser.parse('{"name": "bob"}')
  assert_equal("bob", bob["name"])
end

def test_parse_numeric_value
  anonymous = MyJSONParser.parse('{"age": 29}')
  assert_equal(29, anonymous["age"])
end
```
{% endraw %}

もし、最初のコードが「複数のキーと値をパースできること」を確認したい場合はassert_equalを複数のテストに分割するのではなく、複数のassert_equalを1つのassert_equalにまとめるべきです。

{% raw %}
```ruby
def test_parse
  bob = MyJSONParser.parse('{"name": "bob", "age": 29}')
  assert_equal({"name" => "bob", "age" => 29},
               bob)
end
```
{% endraw %}

1つのassert_equalにまとめると、失敗する場合でも、一部の比較だけが実行されるのではなく、すべての比較が実行されます。そのため、"name"はパースに成功しているけど"age"は失敗している、というように、失敗の傾向がわかります。失敗の傾向がわかるとデバッグしやすくなるというのは前述の通りです。

### おまけ: Hashのassert_equal結果が見づらい

ところで、Hashのassert_equalの結果は見づらいですよね。これは、RSpecでも一緒です。

{% raw %}
```ruby
{"age" => 30, "name" => "bob"}.should == {"name" => "bob", "age" => 29}
```
{% endraw %}

この結果は以下のようになります。

<pre><code><span style="color: red">expected: {"name"=&gt;"bob", "age"=&gt;29}
     got: {"age"=&gt;30, "name"=&gt;"bob"} (using ==)
Diff:
@@ -1,2 +1,2 @@
-{"name"=&gt;"bob", "age"=&gt;29}
+{"age"=&gt;30, "name"=&gt;"bob"}</span></code></pre>


一見しただけではどこが違うのかわかりません。

しかし、最新の[test-unit 2](http:///test-unit.rubyforge.org/index.html.ja)は一味違います。

{% raw %}
```ruby
assert_equal({"name" => "bob", "age" => 29},
             {"age" => 30, "name" => "bob"})
```
{% endraw %}

この結果は以下のようになります。

<pre><code>&lt;<span style="color: white; background-color: green">{"age"=&gt;29, "name"=&gt;"bob"}</span>&gt; expected but was
&lt;<span style="color: white; background-color: red">{"age"=&gt;30, "name"=&gt;"bob"}</span>&gt;

diff:
<span style="color: blue">?</span> {"age"=&gt;<span style="color: white; background-color: green">29</span>, "name"=&gt;"bob"}
<span style="color: blue">?</span>         <span style="color: white; background-color: red">30</span></code></pre>


違いがわかりやすいですね。

### まとめ

デバッグしやすいassert_equalの書き方を2パターン紹介しました。今度から、assert_equalを書くときは、単に数を増やしたりするのではなく、どうすれば有用なassert_equalになるかを考えてみてください。assert_equalが失敗したときにいかにデバッグしやすいかがヒントになるはずです。

[^0]: assert_equalではなくて、should ==と読み替えてもよい。
