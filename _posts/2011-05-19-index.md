---
tags:
- ruby
title: 日本Ruby会議2011にGoldスポンサーとして参加
---
[昨年]({% post_url 2010-05-31-index %})に引き続き、クリアコードは今年も[日本Ruby会議2011のGoldスポンサー](http://rubykaigi.org/2011/ja/sponsors_gold#G02)になりました。
<!--more-->


まだでていませんが、発表などでも参加する予定なので、会場で見かけたら声でもかけてください。

開催は2ヶ月後で、まだ少し時間がありますが、[RubyKaigi2011の講演以外の企画について](http://rubykaigi.tdiary.net/20110514.html#p01)や[RubyKaigi Advent Calendar 2011](http://rubykaigi.tdiary.net/20110512.html#p01)など関連情報がいくつかでています。[Online.kaigi.rb](http://onsg.techtalk.jp/14)など日本Ruby会議2011以前に行われるイベントもあるようなので、興味のある方はそちらにも参加してみてはいかがでしょうか。
