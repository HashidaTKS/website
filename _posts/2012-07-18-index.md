---
tags:
- clear-code
title: 'クリアなコードの作り方: 同じことは同じように書く'
---
「同じことは同じように書く」ことがどうして大事かを説明します。
<!--more-->


### 具体例: `return`の有無

先日、[DevLOVE運営チーム主催のリーダブルコードイベント](http://kokucheese.com/event/index/41881/)が開催されました。イベントの前半は[リーダブルコード](https://amazon.co.jp/dp/9784873115658)の訳者である角さんによる[リーダブルコードの紹介](http://capsctrl.que.jp/kdmsnr/diary/20120706p01.html)で、後半は参加者が「リーダブルコードとはどういうコードか」をディスカッションしました。ディスカッションでは実際に参加者が書いたコードを読みながら「ここはリーダブルだね」「ここはこうした方がもっとリーダブルじゃないか」といったことを考えました。

さて、その中で使ったコードを見ながら「同じことは同じように書く」ことがどうして大事かを説明します。ここで使うコードは[dproject21/yaruo_tdd_triangle](https://github.com/dproject21/yaruo_tdd_triangle)のtriangle.rbです[^0]。

{% raw %}
```ruby
class Triangle
  attr_accessor :a, :b, :c
  def is_equilteral_triangle?
   not_nil? && preCondition? && @a == @b && @b== @c && @c == @a
  end
  def is_isoscales_triangle?
   not_nil? && preCondition? && (@a == @b || @b == @c || @c == @a)
  end
  def is_triangle?
   not_nil? && preCondition? && (a < b + c) && (b < a + c) && ( c < a + b) 
  end
  def is_scalene_triangle?
    return is_triangle? && !is_isoscales_triangle?
  end
  def preCondition?
    unless [@a,@b,@c].find{|n| n.is_a?(Numeric) && n > 0} 
      false
    else
      true
    end
  end

  def not_nil?
    if (@a.nil? || @b.nil? || @c.nil?) 
      false
    else
      true
    end
  end

end
```
{% endraw %}

ディスカッションのときは以下の`return`に注目しました。

{% raw %}
```ruby
def is_scalene_triangle?
  return is_triangle? && !is_isoscales_triangle?
end
```
{% endraw %}

このコードはRubyのコードなので最後に評価した式がメソッドの返り値になります。そのため、この場合は`return`を書いても書かなくても動作は変わりません。では、どのような観点で`return`に注目したかというと、他のメソッドでは`return`を使っていないのにこのメソッドでは使っているという観点です。コード全体を見れば、特に`return`を使うかどうかを使い分けていないだけという雰囲気を感じますが、通常はここで「何か意図があるのではないか」と考えます。それではどのように考えるかを説明します。

### 推測

まず、書かなくてもよい`return`を明示的に書いているということは、`return`を強調しているのではないかと考えます。つまり、「このメソッドだけは返り値が重要」で、他のメソッドでは「Rubyなので必ず値を返すけど、その値は重要ではない」という意図があるのではないかと考えます。それでは、`return`がない他のメソッドも見てみましょう。

{% raw %}
```ruby
def is_triangle?
 not_nil? && preCondition? && (a < b + c) && (b < a + c) && ( c < a + b) 
end
```
{% endraw %}

メソッド名に`?`がついていることに注目します。RubyではSchemeなどと同じように、真偽値を返すメソッドの名前の最後を`?`にする習慣があります。これを考慮すると、このメソッドは「真偽値を返す」ということが重要だと考えられます。しかし、明示的な`return`がないため、「`return`は返り値が重要であるということを示す意図がある」という考えと両立しません。よって、明示的な`return`は「返り値が重要」ということを示すためのものではなく、使っても使わなくてもどちらでもよいからたまたまついていただけなのだろうと考えます。

このように、周囲のコードとあわせて読めば`return`の意図を推測できますが読む人が大変です。しかし、すべてに`return`を書く、一切`return`を書かない、一定のルールで`return`を使う[^1]、など全体として統一された使い方になっていれば読む人が読みやすくなります。できるだけ読みやすいコードにしたいですね。

### まとめ

リーダブルコードのイベントのときに使ったコードを例にして、同じことが同じように書かれていない場合は読む人が大変という事を説明しました。自分がコードを書くときは同じことをするときは同じように書いて、どういう意図でこのコードを書いたかが読めばすぐにわかるコードを書きましょう。

なお、ここでは`return`を例にしましたが、他のコードや技術的な文書でも同じことが言えます。通常の文章では、同じことを何度か言う場合は同じ言い回しを避けます。これは読む人が飽きて読みづらくなることを避けるためです。一方、プログラミングや技術的な文書では「同じことは同じように書く」ことで、読む人がすぐに「同じこと」であると認識できるようにします。これにより、読む人が書いた人の意図を理解しやすくなります。

あわせて読みたい:

  * [ifとreturnの使い方 - ククログ(2012-03-28)]({% post_url 2012-03-28-best-practice-how-to-use-if-and-return %})
  * [クリアなコードの作り方: 余計なことを書かない - ククログ(2012-05-21)]({% post_url 2012-05-21-index %})
  * [リーダブルコードの解説 - ククログ(2012-06-11)]({% post_url 2012-06-11-index %})

[^0]: このコードはディスカッションの練習用に選んだもので、小さいスクリプトだったのが選んだ理由の1つ。他の理由はイベントでは紹介したので省略。また、他にディスカッションに使ったコードについても省略。

[^1]: 例えば、基本は`return`を使わないで、特別なケースを処理するとき（ガード節とか）だけ使うとか。
