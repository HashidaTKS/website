---
tags:
- company
title: 社宅制度による社会保険料と所得税の削減
---
### はじめに

来年4月の消費税率引き上げを前に、政府では軽減税率の導入や低所得層向けに現金給付する案などが検討されています。しかし、消費税負担額の増加は不可避な状況です。さらに健康保険、厚生年金といった社会保険料もその料率が年々引き上げられており、庶民の負担は増える一方です。
<!--more-->


そのような状況において、会社としては社員の負担を軽減したいところです。ただ、給与の引き上げは追加のコストが発生するため容易ではありません。しかし、社宅制度を導入すると、追加のコスト無しに社員の可処分所得を増やすことができます。

[以前紹介した退職金共済制度]({% post_url 2012-11-14-index %})も可処分所得を増やす仕組みではありますが、社員が可処分所得の増加を実感できるのは退職金を受け取った時であり、在職中はメリットを感じられません。しかし、今回紹介する社宅制度は社宅契約時から社員の費用負担を軽減し、さらに毎月の可処分所得が増加することからメリットを感じてもらいやすい制度です。

今回はクリアコードが導入している社宅制度を紹介するとともに、具体例からその金銭的メリットを紹介していきます。

### クリアコードの社宅制度

クリアコードの社宅制度は、いわゆる借り上げ社宅を対象としたものです。借り上げ社宅とは、貸主と会社が賃貸契約した不動産を、会社が社員に住宅として提供するものです。クリアコードでは入社や結婚による転居が生じるときに社宅制度[^0]が利用可能です。

社宅制度の内容は、ここでは費用についてのみ取り上げます。

この制度では、社宅契約時に発生する敷金、礼金、仲介手数料、火災保険料を会社が全額を負担します。毎月の家賃は全額を社員が負担しますが、負担方法がすこし変わっています。家賃の50%から55%を基本給から減額し、50%を社宅家賃として給与から天引きします。家賃を基本給の減額と社宅家賃の天引きに分割して社員が負担することがこの社宅制度の肝で、これが社員の可処分所得の増加につながります。では、なぜ2つに分けて家賃を負担させるのかというと、それは社宅に関する所得税の取り扱いによるものです。

### 社宅に対する所得税の取り扱い

会社が役員や社員に対して社宅を提供するとき、一定額以上の家賃を受け取っていれば、社員は給与として課税されません。逆に、一定額未満の家賃しか受け取っていない場合は、一定額と家賃の差額は社員に対する給与とみなされ所得税が課税されます。この一定額の算出方法は、固定資産税評価額から算出する方法などいくつか選択できます。クリアコードでは、家賃の50%を一定額としています[^1]。

国税庁が提供している社宅に関する情報は以下の通りです。

  * [No.2597 使用人に社宅や寮などを貸した時](http://www.nta.go.jp/taxanswer/gensen/2597.htm)
  * [No.2600 役員に社宅などを貸した時](http://www.nta.go.jp/taxanswer/gensen/2600.htm)

### 事例でみる社宅契約の効果

では、実際に個人で契約した場合と社宅契約とした場合に、社員の家賃支払い後の手取り給与額がどれだけ違ってくるか、以下の例で確認します。

月給40万円の社員が次の物件に引越すものとします。

<table>
  <tr><th>家賃</th><td class="price">100,000円</td></tr>
  <tr><th>仲介手数料</th><td class="price">105,000円</td></tr>
  <tr><th>礼金</th><td class="price">100,000円</td></tr>
  <tr><th>火災保険料（2年）</th><td class="price">25,000円</td></tr>
</table>


#### 個人で契約した場合の手取り給与額

個人で契約した場合、手取り給与額は月額331,342円となります。

<table>
  <tr><th colspan="2">月給</th><td class="price">400,000円</td></tr>
  <tr><th colspan="2">社会保険料</th><td class="price">▲56,808円</td></tr>
  <tr><th rowspan="3">[内訳]</th><th>健康保険料</th><td class="price">20,438円</td></tr>
  <tr><th>厚生年金保険料</th><td class="price">34,370円</td></tr>
  <tr><th>雇用保険料</th><td class="price">2,000円</td></tr>
  <tr><th colspan="2">源泉所得税</th><td class="price">▲11,850円</td></tr>
  <tr><th colspan="2">差引支給額</th><td class="price">331,342円</td></tr>
</table>


不動産の費用として、契約時に仲介手数料、礼金、火災保険料の合計230,000円を支払います。毎月の家賃として、100,000円を支払います。

一般的な賃貸契約の期間である2年間で家賃支払い後の手取り給与額を計算すると、以下の通り5,332,208円となります。

{% raw %}
```
(331,342円 * 24) - 230,000円 - (100,000円 * 24) = 5,322,208円
```
{% endraw %}

#### 社宅契約にした場合の手取り給与額

社宅契約にした場合は、基本給を50,000円減額して350,000円とします。また社宅家賃として50,000円を給与天引きします。なお、不動産契約時の費用はすべて会社が負担します。このとき、給与の手取り額は241,705円となります。

<table>
  <tr><th colspan="2">月給</th><td class="price">350,000円</td></tr>
  <tr><th colspan="2">社会保険料</th><td class="price">▲49,875円</td></tr>
  <tr><th rowspan="3">[内訳]</th><th>健康保険料</th><td class="price">17,946円</td></tr>
  <tr><th>厚生年金保険料</th><td class="price">30,179円</td></tr>
  <tr><th>雇用保険料</th><td class="price">1,750円</td></tr>
  <tr><th colspan="2">源泉所得税</th><td class="price">▲8,420円</td></tr>
  <tr><th colspan="2">社宅家賃</th><td class="price">▲50,000円</td></tr>
  <tr><th colspan="2">差引支給額</th><td class="price">241,705円</td></tr>
</table>


2年間の家賃支払い後の手取り給与額は、以下の通り5,800,920円となります。

{% raw %}
```
241,705円 * 24 = 5,800,920円
```
{% endraw %}

#### 家賃支払い後の手取り給与額の比較

家賃支払い後の手取り給与額は、個人で契約した場合は5,322,208円ですが、社宅契約した場合は5,800,920円となり、478,712円も多くなります。つまり、社宅契約にすると、1ヶ月あたり2万円近く手取り給与額が増えます。これは不動産契約時の初期費用を会社が負担し、また給与を低くすることによって社会保険料や所得税[^2]が抑えられるためです。

続いて、会社の負担額について確認します。不動産契約に関して、支払った費用と人件費の合計額で比較します。

#### 個人で契約した場合の会社負担額

個人で契約した場合は、不動産に関する費用は発生しません。給与と法定福利費の合計を人件費として計算することにします。

<table>
  <tr><th colspan="2">給与</th><td class="price">400,000円</td></tr>
  <tr><th colspan="2">法定福利費</th><td class="price">60,024円</td></tr>
  <tr><th rowspan="5">[内訳]</th><th>健康保険料</th><td class="price">20,438円</td></tr>
  <tr><th>厚生年金保険料</th><td class="price">34,370円</td></tr>
  <tr><th>雇用保険料</th><td class="price">3,400円</td></tr>
  <tr><th>労災保険料</th><td class="price">1,200円</td></tr>
  <tr><th>児童手当拠出金</th><td class="price">616円</td></tr>
  <tr><th colspan="2">合計</th><td class="price">460,024円</td></tr>
</table>


　
2年間の合計額は以下の通り11,040,576円となります。

{% raw %}
```
460,024円 * 24 = 11,040,576円
```
{% endraw %}

#### 社宅契約にした場合の会社負担額

社宅契約にした場合、社宅関連の費用として契約時に230,000円を負担します。毎月の家賃として100,000円を負担します。2年間の合計は以下の通り2,630,000円となります。

{% raw %}
```
230,000円 + (100,000円 * 24) = 2,630,000円
```
{% endraw %}

人件費は、給与と法定福利費の合計から社宅家賃の受取額を差し引きます。

<table>
  <tr><th colspan="2">給与</th><td class="price">350,000円</td></tr>
  <tr><th colspan="2">法定福利費</th><td class="price">52,689円</td></tr>
  <tr><th rowspan="5">[内訳]</th><th>健康保険料</th><td class="price">17,946円</td></tr>
  <tr><th>厚生年金保険料</th><td class="price">30,179円</td></tr>
  <tr><th>雇用保険料</th><td class="price">2,975円</td></tr>
  <tr><th>労災保険</th><td class="price">1,050円</td></tr>
  <tr><th>児童手当拠出金</th><td class="price">539円</td></tr>
  <tr><th colspan="2">社宅家賃</th><td class="price">▲50,000円</td></tr>
  <tr><th colspan="2">合計</th><td class="price">352,689円</td></tr>
</table>


2年間の合計額は、以下の通り11,094,539円となります。

{% raw %}
```
2,630,000円 + (352,689円 * 24) = 11,094,539円
```
{% endraw %}

計算の結果、社宅契約にすると会社側の負担額が53,963円増えることがわかりました。これは1ヶ月に換算すると2,248円の負担増です。もし、基本給から減額する金額を2,300円増やして52,300円とすれば会社の負担はなくなります。このとき、社員はもともと2万円近く手取り給与が増える計算なので、基本給が2,300円減ってもなお大きなメリットがあります。こうすれば、会社も個人もメリットが得られます。

### その他のメリットデメリット

金銭的なメリットについては、紹介したとおりですが、デメリットがないわけではありません。

例えば社員にとっては、「会社をやめた時に個人契約に切り替える手続きが発生する」というデメリットがあります。また基本給が減額されますので、基本給から算出される残業代は少なくなります。社会保険料の負担額が少なくなっていますので、将来受け取る年金額や失業手当が減ります。

一方、会社にとっては、賃貸物件について何らかのトラブルが発生した場合の保証リスクが発生します。また不動産契約を管理するための事務コストが発生します。

### まとめ

社宅制度を使った社会保険料と税金の削減策を紹介しました。もうすぐ新入社員が入ってくる時期です。一人暮らしをはじめるにあたって家を借りるとなると金銭的な負担も小さくありません。これからの会社を担う新入社員のために、社宅制度の導入を検討してみてはいかがでしょうか。

[^0]: 社宅管理規程を定めて制度を導入するのが一般的です。

[^1]: 固定資産税の課税標準額から算出すれば、社員の支払うべき家賃を減らすことができ、より一層のコストメリットが得られます。しかし、会社側の事務コストも増加します。

[^2]: 給与が抑えられることによって住民税も減ります。ただし、住民税は前年の所得に対して課税されることから、効果があがるのが翌年となるため計算には含めていません。
