---
tags: []
title: github-post-receiverの複数サイト対応
---
### はじめに

クリアコードでは最近Gitリポジトリの管理ツールを[gitolite](http://gitolite.com/)から[GitLab](http://gitlab.org/)に移行[^0]しました。
<!--more-->


クリアコードでは[git-utils](https://github.com/clear-code/git-utils)を使用してdiff付きのコミットメールを配信しているのですが、GitLabに移行した当初はgithub-post-receiverがGitLab 6.0に対応しきれていませんでした。
github-post-receiverをGitLabで使用する場合は、以下の2つの制限がありました。

  * 同じリポジトリ名を異なるオーナー間で使用することができない
  * 複数サイト運用時にリポジトリ名を一意にしなければならない

この2つの制限の原因はgithub-post-receiverのGitLab対応が不十分であったことと複数サイトでの運用を考慮していなかったことでした。

そこで、この2つの制限を撤廃するための対応をしたのですが、一部互換性をなくす必要があったので、その説明と新しい設定ファイルの書き方の説明をします。

### 設定ファイルについて

github-post-receiverで複数サイトに対応するための設定ファイルの書き方を説明します。設定ファイルは後方互換性を維持しています。

github-post-receiverにはconfig.yamlという設定ファイルが必要です。
古い書式は以下の通りです。

{% raw %}
```
to: receiver@example.com
error_to: admin@example.com
exception_notifier:
  subject_label: "[git-utils]" 
sender: sender@example.com
add_html: true
owners:
  ranguba:
    to: groonga-commit@rubyforge.org
    repositories:
      examples:
        to: null@example.com
  groonga:
    to: groonga-commit@lists.sourceforge.jp
```
{% endraw %}

1つのサイトからのpost-receive-hooksしか受け付けないのであれば、古い書式のままでも問題ありません。
これは、古い書式の場合は全てのサイトで同一の設定を使用するようになっているためです。

複数のサイト[^1]を運用している場合は以下のようにdomainsキーを追加した設定ファイルを書けば、複数サイト運用時にgithub.comとGHEでリポジトリ名が重複していてもそれぞれのサイトごとにコミットメールの宛先などを設定することができます。

{% raw %}
```
to: global-to@example.com
error_to: error@example.com
exception_notifier:
  subject_label: "[git-utils]" 
sender: sender@example.com
add_html: false
domains:
  github.com:
    add_html: true
    owners:
      clear-code:
        to: commit@clear-code.com
      ranguba:
        to:
          - groonga-commit@rubyforge.org
          - commit@clear-code.com
        repositories:
          examples:
            to: null@example.com
      groonga:
        to: groonga-commit@lists.sourceforge.jp
  ghe.example.com:
    owners:
      clear-code:
        to:
          - commit@example.com
        from: null@example.com
        repositories:
          test-project1:
            to: commit+test-project1@example.com
          test-project2:
            to: commit+test-project2@example.com
      support:
        to: support@example.com
        from: null+support@example.com
  ghe.example.co.jp:
    add_html: true
    owners:
      clear-code:
        to:
          - commit@example.co.jp
        from: null@example.co.jp
        repositories:
          test-project1:
            to: commit+test-project1@example.co.jp
          test-project2:
            to: commit+test-project2@example.co.jp
      support:
        to: support@example.co.jp
        from: null+support@example.co.jp
```
{% endraw %}

設定は、一番狭い(深い)指定にマッチしたものを使用します。
いくつか具体的に説明します。

  * github.com/clear-code以下のリポジトリへのコミットはcommit@clear-code.comにコミットメールを送る
  * github.com/ranguba以下のリポジトリへのコミットはgroonga-commit@rubyforge.orgとcommit@clear-code.comにコミットメールを送る。ただしranguba/examplesリポジトリはnull@example.comにコミットメールを送る
  * github.com/milter-manager/milter-managerにpost-receive-hooksを設定した場合はglobal-to@example.comにコミットメールを送る
  * ghe.example.com/clear-code以下のリポジトリはtest-project1とtest-project2を除いてcommit@example.comにコミットメールを送る
  * ghe.example.co.jp/clear-code以下のリポジトリはtest-project1とtest-project2を除いてcommit@example.co.jpにコミットメールを送る

詳しくは[テストコード](https://github.com/clear-code/git-utils/blob/master/github-post-receiver/test/multi-site-receiver-test.rb)を参照してください。

### 非互換について

以前のバージョンとの非互換を説明します。

github-post-receiverは、通知が来るとリポジトリをミラーリングします。このときにgithub-post-receiverが動作しているホスト上のディレクトリ構成を変更しました。
以下のようにリポジトリのURIからドメイン名も抽出してパスの一部として使うようになっています。
また、GitLabではowner_nameが"$gitlab"固定だったのをやめ、リポジトリのURIからowner_nameを抽出するようにしました。

{% raw %}
```
Before: mirrors/#{owner_name}/#{repository_name}
After : mirrors/#{domain}/#{owner_name}/#{repository_name}
```
{% endraw %}

以前からgithub-post-receiverを使用している場合は、事前にミラーしたリポジトリを移動しておくと無駄なミラーリングを行いません。
パスが変わるだけなので、事前にミラーしたリポジトリを移動しなかった場合でもディスクを余分に使用すること以外に害はありません。

### まとめ

git-utilsがGitLabのpost-receive-hooksと複数サイトの運用に対応したのでその説明をしました。
今後もより便利に使えるように、改善していきたいと考えているのでgit-utilsの[issue](https://github.com/clear-code/git-utils/issues)に今後やりたいことをあげておきました。

お時間のある方は、取り組んでみるとよいかもしれません。

[^0]: GitLab 6.0のリリース後に移行しました

[^1]: 例えばgithub.comと社内のGHE
