---
tags: []
title: Debianでパッケージをリリースできるようにしたい - WNPPへのバグ登録
---
### はじめに

オープンソースのカラムストア機能付き全文検索エンジンといえば、[Groonga](http://groonga.org/ja)があります。Groongaを使うと全文検索機能付き高性能アプリケーションを開発することができます。
<!--more-->


Groongaプロジェクトでは各種ディストリビューション[^0]向けに独自にパッケージを提供しています。そのため、比較的簡単に[インストール](http://groonga.org/ja/docs/install.html)できるようになっています。

ただ、パッケージをインストールするのに、あらかじめ独自に提供しているリポジトリを登録しないといけないのがちょっと面倒です。できれば、それぞれのディストリビューションの公式リポジトリからインストールできるととても楽です。

昨年の[全文検索エンジンGroongaを囲む夕べ4](http://atnd.org/events/43461)の懇親会で、Debian開発者の[やまね](https://wiki.debian.org/HidekiYamane)さんとVine Linux開発メンバーの[岩井](http://vinelinux.org/projectvine.html)さんとお話する機会がありました。GroongaをDebianに入れたいのだけれどと相談したところ、やまねさんからわからないところはサポートしますよというありがたい一言をいただきました。そこで、現状のGroongaのパッケージについてコメントをもらうところからGroongaをDebianプロジェクトでリリースできるようにする作業ははじまりました。

今回は、現在進行中であるGroongaのDebianリポジトリ入りを目指す作業の中からバグ登録について紹介します。

### Debianでパッケージをリリースするには

そもそも、Debianプロジェクトでパッケージをリリースできるようにするのに必要な手順はどんなものがあるのでしょうか。

それを簡単に説明している文書の一つに、[Introduction for maintainers: How will my package get into Debian](http://mentors.debian.net/intro-maintainers)があります。

簡単に紹介すると以下の通りです。

  * パッケージを決める（今回はGroonga）
  * WNPPにバグ登録をする
  * パッケージを用意する
  * パッケージを公開する
  * スポンサーを見つけ、レビューを受ける
  * Debianにパッケージをアップロードする
  * パッケージを継続してメンテナンスする

今回はこのうち、WNPPにバグ登録するところを説明します。

### WNPPとは

WNPPとは、「作業が望まれるパッケージ（Work-Needing and Prospective Packages; WNPP）」を意味します。今回はGroongaが新たなパッケージ化が望まれているDebianパッケージということになります。

このWNPPに属するパッケージについては[Debian Bug report logs: Bugs in package wnpp in unstable](https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=wnpp;dist=unstable)で参照できます。

### バグ登録をするには

バグ登録する場合には、定型的なメールを送信します。[[groonga-dev,01967] Debianパッケージの登録作業(ITP）](http://sourceforge.jp/projects/groonga/lists/archive/dev/2013-December/001969.html)でやまねさんに教えてもらいました。

そこで、教えてもらった雛形を参考に以下のようなメールをsubmit@bugs.debian.org宛てに送りました。

{% raw %}
```
Subject: ITP: groonga -- Fulltext search engine
From: HAYASHI Kentaro <hayashi@clear-code.com>
To: submit@bugs.debian.org
Date: Fri, 13 Dec 2013 19:05:17 +0900
X-Mailer: Sylpheed 3.4.0beta7 (GTK+ 2.24.20; x86_64-unknown-linux-gnu)

Package: wnpp
Severity: wishlist
Owner: Groonga Project <packages@groonga.org>
X-Debbugs-CC: debian-devel@lists.debian.org, debian-devel@lists.debian.or.jp

   Package name: groonga
        Version: 3.1.0
Upstream Author: Daijiro MORI <morita at razil. jp>, Tasuku SUENAGA <a at razil. jp>, Yutaro Shimamura <yu at razil. jp>, Kouhei Sutou <kou at cozmixng. org>, Kazuho Oku <kazuhooku at gmail. com>, Moriyoshi Koizumi <moriyoshi at gmail. com>
       URL: https://github.com/groonga/groonga
        License: LGPL-2.1

Description: Groonga is an open-source fulltext search engine and column store.
 It lets you write high-performance applications that requires fulltext search.
```
{% endraw %}

件名に入れているITPというのはなんでしょうか。これは、Intent To Packageの略語で、新たなパッケージを作成したいという宣言を意味します。その際にはX-Debbugs-CC:にdebian-devel@lists.debian.orgを指定して、Debian開発者へと周知します。

メールを送信してしばらくすると、次のようにバグが採番され、BTSへと登録できます。Groongaの場合は#732055となりました。

  * [Debian Bug report logs - #732055 ITP: groonga -- Fulltext search engine](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=732055)

とても簡単ですね。

### まとめ

今回はDebianでパッケージをリリースするための最初の一歩であるバグ登録について紹介しました。バグ登録をしてからも、まだまだしなければいけない作業はたくさんあります。それらについては、またの機会に記事にしたいと思います。

### 参考リンク

  * [Debian JP Project 略語の解説](http://www.debian.or.jp/community/devel/abbreviation.html) Debianでよく使われる略語についてはこちらを参照してください。

[^0]: Debian/Ubuntu/CentOSでは独自のリポジトリからインストールできるようになっています。Fedoraの場合はFedoraプロジェクト公式リポジトリからインストールできます。Fedoraだけ公式リポジトリからインストールできるようになっているのは「[Fedoraプロジェクトで新規パッケージをリリースする方法]({% post_url 2013-04-10-index %})」や「[Fedoraプロジェクトでパッケージを更新するには]({% post_url 2013-07-17-index %})」の成果です。
