---
tags: []
title: GitHubのWikiが変更されたら差分付きで通知する方法
---
一人でWikiを使っている場合はそんなことはありませんが、複数人でWikiを使っている場合はだれかがWikiを変更したらそれを知りたいものです。複数人でWikiを使っている場合は、情報共有のために使うことが多いです。Wikiが変更されたことがわかると、最新の情報を入手することが容易になるため、情報共有という目的を達成しやすくなります。
<!--more-->


最新の情報の入手のしやすさという点では「どのように」変更がわかるかが重要です。例えば、次のような変更の知り方があります。下にいくほど最新の情報を入手するための手間が減るので最新の情報を入手しやすくなります。

  * 定期的にWikiの注目しているページをブラウザーで開き、変更がないか確認する。注目しているページが複数ある場合は繰り返し確認する。
  * 定期的にWikiをブラウザーで開き、「最近更新されたページリスト」を確認する。更新されたページのうち、前回確認したときより後に更新されたページを見て変更点を確認する。
  * Wikiに変更があったら通知がくるようにする。通知がきたら↑と同様に変更を確認する。
  * Wikiに変更があったら変更されたページの情報付きで通知がくるようにする。通知がきたら該当ページをブラウザーで開いて変更を確認する。
  * Wikiに変更があったら変更されたページの情報と変更内容付きで通知がくるようにする。通知を見れば変更点がわかるのでブラウザーで確認する必要はない。

なお、複数のWikiがある場合は通知先を一箇所に集約しているかどうかで最新の情報の入手しやすさに影響があります。一箇所に集約している方が最新の情報を入手しやすいです。理由は、集約しているところを確認するだけでよいので手間が減るからです。

ここでは、GitHubのWikiを使っている場合に最新の情報を入手しやすくする方法、つまり、GitHubのWikiに変更があったら変更されたページの情報と変更内容付きで通知がくるようにする方法を紹介します。

### 方法

最初に方法を説明します。仕組みはその後に説明します。

[clear-code/git-utils](https://github.com/clear-code/git-utils)[^0]というフリーソフトウェアを使うと、GitHubのWikiが更新されたらページのURLと差分（diff）が入ったメールを通知することができます。

git-utilsのインストール方法は[README](https://github.com/clear-code/git-utils/tree/master/github-post-receiver#readme)を参考にしてください。ここでは、git-utilsは http://git-utils.example.com/ にインストール済みという前提で説明します。

Wikiの変更を通知したいプロジェクトのWebhooksにgit-utilsを指定します。Webhooksを追加する設定画面のURLは`https://github.com/#{user}/#{project}/settings/hooks/new`です。例えば、git-utilsプロジェクトのWebhooksを設定するURLは https://github.com/clear-code/git-utils/settings/hooks/new です。

この設定画面には「Payload URL」を設定するテキストボックスがあるので、そこに`http://git-utils.example.com/post-receiver/`と指定します。

その後、「Which events would you like to trigger this webhook?」と説明がある選択肢の中から「Send me everything.」を選びます。デフォルトは「Just the push event.」となっているので変更しないといけません。

これで設定は完了です。GitHubのWikiを更新すると指定したメールアドレスにdiff入りで通知メールが届きます。

なお、diffではなく、変更後のページの内容そのものの方がよい場合はAtomを購読します。AtomのURLは`https://github.com/#{user}/#{project}/wiki.atom`です。

### 仕組み

GitHubのWebhooksを使うと[イベント](https://developer.github.com/webhooks/#events)が発生するごとにHTTPで通知を受け取ることができます。Wikiが変更されたときは[gollum](https://developer.github.com/v3/activity/events/types/#gollumevent)というイベントが発生します。git-utilsではこのイベントを契機に通知メールを作成します。

GitHubからの通知にはページ情報とコミットIDが含まれています。GitHubのWikiはGitで管理されているのでコミットIDとはGitのコミットIDそのものです。

WikiのGitリポジトリーは`https://github.com/#{user}/#{project}.wiki.git`になります。GitHubからの通知にはプロジェクトの情報も含まれているので、それらの情報からこのURLを作ることができます。

GitHubからの通知でGitリポジトリーとコミットIDを取得できるので、あとは通常のコミットメールの配送処理をするだけです。git-utilsにはコミットメールを配送するプログラムを含んでいるので、それを利用しています。

### まとめ

GitHubのWikiが更新されたらdiff入りで変更内容を通知する方法を紹介しました。

diff入りで通知することで最新の情報を共有しやすくなります。複数人で情報共有のためにGitHubのWikiを使っている場合は使用を検討してみてください。今回の記事では省略したgit-utilsのインストールが大変という方は、相談してください。クリアコードでもgit-utilsをインストールしたサーバーを持っているのでそれを使えるようにすることができます。例えば、[tdiary/tdiary](https://github.com/tdiary/tdiary-core/)はクリアコードがインストールしたgit-utilsを使っています。

なお、Wikiの内容だけではなく、コミットの内容も共有することもオススメです。他の人が書いたコードから学べることはたくさんありますよ。（参考：[コードリーダー育成支援](/services/code-reader/)）

### おまけ：RedmineのWikiが変更されたら差分付きで通知する方法

[Feature #11530: Support hooks in mailer - Redmine](http://www.redmine.org/issues/11530)のパッチと[redmine-plugin-wiki-change-notifierプラグイン](https://github.com/kou/redmine-plugin-wiki-change-notifier)を使うとRedmineのWikiが変更されたら差分付きで通知メールを届けることができます。インストール方法は次の通りです。

{% raw %}
```
% wget http://www.redmine.org/releases/redmine-2.4.5.tar.gz
% tar xvf redmine-2.4.5.tar.gz
% cd redmine-2.4.5
% curl http://www.redmine.org/attachments/download/8244/support-hooks-in-mailer-for-r10463.patch | patch -p0
% git clone https://github.com/kou/redmine-plugin-wiki-change-notifier plugins/wiki_change_notifier
（以降は通常のインストール方法）
```
{% endraw %}

[^0]: git-utilsという名前は実体と乖離しているので、git-commit-emailとかという名前にした方がよいプロジェクトです。
