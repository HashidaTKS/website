---
tags: []
title: Software Design 4月号の紹介
---
クリアコードの林です。
<!--more-->


3/18(水曜)に技術評論社から[「Software Design」4月号](http://gihyo.jp/magazine/SD/archive/2015/201504)が発売されました。
そのなかでDebian開発者である[やまねひでき](https://wiki.debian.org/HidekiYamane)さんが連載中の「Debian Hot Topics」に、[全文検索エンジンGroonga](http://groonga.org/ja)をDebian公式に入れるまでのあれこれを「Debian公式入りへの道」と題して書きました。

### 内容について

記事の内容は、過去にククログで公開した以下の記事のダイジェストです。

  * [Debianでパッケージをリリースできるようにしたい - WNPPへのバグ登録]({% post_url 2014-03-07-index %})
  * [Debianでパッケージをリリースできるようにしたい - よりDebianらしく]({% post_url 2014-04-03-index %})
  * [Debianでパッケージをリリースできるようにしたい - mentors.debian.netの使いかた]({% post_url 2014-07-02-index %})
  * [Debianでパッケージをリリースできるようにしたい - そしてDebianへ]({% post_url 2014-10-31-index %})

GroongaをDebian公式のパッケージにするにあたり、そのきっかけや、どんな作業をしたのか、苦労した点などをGroongaを題材にして書いています。

### まとめ

Debian公式に入れたいパッケージがあるんだけど、どうしたらいいかわからない人はぜひ読んでみてください。
紙面の都合で、詳細を割愛したところもあります。上記で紹介したククログの記事も合わせて参照してみてください。
