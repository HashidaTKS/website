---
tags: []
title: 動作確認用の画像をImageMagickで簡単に作るには
---
### はじめに

Webアプリケーションを開発していると、動作確認用の画像が欲しいことがしばしばあります。
ちょっとアップロードを試す程度なら、手元の適当な画像を探して使うということができます。
ただし、動作確認用の画像をリポジトリに入れたりとなると話は別で、そのままではまずいことも多々あります。
そういうときは癖のない無難な画像が欲しくなったりします。
<!--more-->


今回はそんなときに便利な[ImageMagick](http://www.imagemagick.org/script/index.php)を使って動作確認用の画像を簡単に作る方法を紹介します。

### ImageMagickのセットアップ

まずは、ImageMagickをインストールしましょう。

```text
% sudo apt-get install imagemagick
```


これで、今回利用する`convert`コマンドが使えるようになりました。きちんとインストールできていると`-version`オプションをつけて実行すればバージョンが表示されます。[^0]

```text
% convert -version
Version: ImageMagick 6.8.9-9 Q16 x86_64 2015-09-19 http://www.imagemagick.org
Copyright: Copyright (C) 1999-2014 ImageMagick Studio LLC
Features: DPC Modules OpenMP
Delegates: bzlib djvu fftw fontconfig freetype jbig jng jpeg lcms lqr ltdl lzma openexr pangocairo png tiff wmf x xml zlib
```


### 画像を作成してみよう

`convert`コマンドが使えるようになったところで、実際に動作確認用の画像を作成してみましょう。

例えば、次のような画像が欲しいとします。

  * 画像形式はBMP,JPEG,PNG,TIFFが必要（同一形式で拡張子名が異なるものもあり）

  * 画像形式の情報が描画されている（pngファイルなら「png」と描画されている）

  * サイズは任意

これは次のようなサンプルスクリプトで実現できます。

```sh
#!/bin/bash

function usage() {
    echo "Usage $0 width height"
    exit 1
}

if [ -z "$1" ]; then
    usage
fi
if [ -z "$2" ]; then
    usage
fi

width=$1
height=$2
point=`expr $width / 2`
for f in bmp gif jpeg jpg png tiff tif; do
    convert -size ${width}x${height} xc:#97c03e \
	    -pointsize $point \
	    -gravity center \
	    -fill black \
	    -stroke white \
	    -draw "text 0,0 $f" ${width}x${height}.$f
done
```


上記の内容を`create-image.sh`として保存し、実行可能にしておきます。
次のようにサンプルスクリプトを実行してみましょう。

```text
% ./create-image.sh 100 100
```


すると、画像をいくつか作成できます。

```text
% ls -la
合計 260
drwxr-xr-x  2 kenhys kenhys  4096 10月  7 10:11 .
drwx------ 25 kenhys kenhys  4096 10月  7 09:10 ..
-rw-r--r--  1 kenhys kenhys 40138 10月  7 09:51 100x100.bmp
-rw-r--r--  1 kenhys kenhys  1474 10月  7 09:51 100x100.gif
-rw-r--r--  1 kenhys kenhys  3406 10月  7 09:51 100x100.jpeg
-rw-r--r--  1 kenhys kenhys  2701 10月  7 09:51 100x100.jpg
-rw-r--r--  1 kenhys kenhys  8265 10月  7 09:51 100x100.png
-rw-r--r--  1 kenhys kenhys 80358 10月  7 09:51 100x100.tif
-rw-r--r--  1 kenhys kenhys 80358 10月  7 09:51 100x100.tiff
```


### スクリプトの説明

サンプルスクリプトでは`convert`コマンドを次のように使っていました。

```sh
convert -size ${width}x${height} xc:#97c03e \
	    -pointsize $point \
	    -gravity center \
	    -fill black \
	    -stroke white \
	    -draw "text 0,0 $f" ${width}x${height}.$f
```


先程の例だと、実際には次の内容で`convert`コマンドを実行していました。[^1]

```sh
convert -size 100x100 xc:#97c03e \
	    -pointsize 50 \
	    -gravity center \
	    -fill black \
	    -stroke white \
	    -draw "text 0,0 png" 100x100.png
```


これは次のように`convert`コマンドへと指示しています。

  * `-size 100x100 xc:#97c03e` 背景色が`#97c03e`の100x100サイズの画像を作成する

  * `-pointsize 50` 文字サイズは50ポイントにする

  * `-gravity center` 中央に配置する

  * `-fill black` 文字色は黒にする

  * `-stroke white` 文字の縁取りは白にする [^2]

  * `-draw "text 0,0 png"` 「png」と描画する

  * `100x100.png` 出力ファイル名は`100x100.png`にする

このように`convert`コマンドのオプションを組み合わせることで、以下のようなサンプル画像をまとめて作成できました。

![gif画像]({{ "/images/blog/20151007_0.gif" | relative_url }} "gif画像")
![jpeg画像]({{ "/images/blog/20151007_1.jpg" | relative_url }} "jpeg画像")
![jpg画像]({{ "/images/blog/20151007_2.jpg" | relative_url }} "jpg画像")
![png画像]({{ "/images/blog/20151007_3.png" | relative_url }} "png画像")

### まとめ

動作確認用の画像をImageMagickを使って簡単に作成する方法を紹介しました。
ここで紹介した`convert`コマンドにはほかにもたくさんの[オプション](http://www.imagemagick.org/script/convert.php)があります。
興味のある人はいろいろと調べてみると良いのではないでしょうか。

[^0]: 実際のバージョンはお使いのディストリビューションでインストールできるImageMagickのバージョンによって異なります。

[^1]: 拡張子がpngの場合。

[^2]: 小さいサイズでは縁取りしないほうが視認性が良いかもしれません。
