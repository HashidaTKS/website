---
tags:
- mozilla
title: Firefoxを意図的にクラッシュさせる方法
---
Firefoxの導入時の要件として、クラッシュ時のレポートを送信しないようにするという設定を行う事があります。
この設定が意図通りに反映されているかどうかを確認するために、Firefoxが実際にクラッシュした時の様子を観察したい場合があります。
<!--more-->


Firefoxには既知で且つ未修正のクラッシュバグがいくつかあるため、それを突くようなコードを実行すればFirefoxをクラッシュさせる事ができます。
しかし、クラッシュバグは再現性が低い物もありますし、そもそも新しいバージョンのFirefoxではクラッシュしないように修正されていることも多いです。
安定してFirefoxをクラッシュさせる方法を把握しておけば、Firefoxをクラッシュさせるための方法をその都度あれこれ調べなくても済みます。

Firefoxにはjs-ctypesという、JavaScriptからC言語製の共有ライブラリの機能を直接呼び出す機能が含まれており、これを使えば、比較的簡単にFirefoxをクラッシュさせられます。
例えば以下のようなコードを実行すれば、使用中のメモリ領域を強制的に解放してメモリ破壊を引き起こし、Firefoxをクラッシュさせる事ができます。

```js
(function(){
Components.utils.import('resource://gre/modules/ctypes.jsm');
// Firefoxの構成ファイルの1つであるnss3.dll（nss3.so）をオープンする。
var library = ctypes.open(ctypes.libraryName('nss3'));
// 指定したアドレスのメモリを解放する関数を取得する。
var PR_Free = library.declare(
  'PR_Free',
  ctypes.default_abi,
  ctypes.void_t,
  ctypes.voidptr_t
);
// 適当なアドレスを指定してメモリを解放する。
var ptr = new ctypes.voidptr_t(1);
PR_Free(ptr);
})();
```


上記のスクリプトを実行するには、js-ctypesを利用する権限（chrome権限）が必要です。
「スクラッチパッド」を使って実行する場合は以下の手順となります。

  1. Firefoxを起動する。

  1. `about:config`を開き、`devtools.chrome.enabled`の値を`true`に設定する。

  1. Shift-F4を押すか、「開発ツール」メニューから「スクラッチパッド」を選択してスクラッチパッドを起動する。

  1. 「実行環境」メニューから「ブラウザ」を選択する。

  1. 上記のスクリプトを貼り付けて、ツールバー上の「実行」をクリックする。

なお、メモリ破壊を引き起こしてFirefoxをクラッシュさせると履歴や設定などのデータが破損する場合があります。
この方法を実際に試す際は、データが壊れてもよいテスト用のプロファイルを使う事を強くお勧めします。
