---
tags:
- groonga
- presentation
title: "「nginx実践入門」出版記念！執筆者らが語る nginx Tech Talks：ngx_http_groonga - 全文検索nginx"
---
2月8日に開催された[「nginx実践入門」出版記念！執筆者らが語る nginx Tech Talks](http://eventdots.jp/event/578421)のLT枠が1つ余っていたので「ngx_http_groonga - 全文検索nginx」と題してGroongaでのnginx利用事例を紹介しました。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/nginx-tech-talks/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/nginx-tech-talks/" title="ngx_http_groonga - 全文検索nginx">ngx_http_groonga - 全文検索nginx</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show](https://slide.rabbit-shocker.org/authors/kou/nginx-tech-talks/)

  * [スライド（SlideShare）](http://www.slideshare.net/kou/nginx-tech-talks)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-nginx-tech-talks)

### 内容

nginxはHTTPサーバー・HTTPリバースプロキシサーバーだけでなく、メールプロキシサーバー・TCPプロキシサーバーにもなれます。いろいろなサーバーになれるので、全文検索エンジンにもなれるよ！全文検索nginxだよ！という話をしました。

nginxのモジュール機能を利用した例としてモジュールを開発したい人は参考にしてください。ソースは[Groongaのリポジトリーのsrc/httpd/nginx-module/](https://github.com/groonga/groonga/tree/master/src/httpd/nginx-module)にあります。

### まとめ

nginxは高速なHTTPサーバーで、Groongaを組み込むことで高速な全文検索エンジンとしても使うことができます。Groongaが組み込まれていなくても有用なので、「nginx実践入門」を参考に使ってみてはいかがでしょうか。

[https://amazon.co.jp/dp/9784774178660](https://amazon.co.jp/dp/9784774178660)
