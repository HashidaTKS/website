---
tags:
- mozilla
title: FirefoxアドオンのWebExtensions移行についてMozilla Blogに寄稿しました
---
弊社メンバーが執筆した、従来からあるFirefox用アドオンを「WebExtensions」ベースに移行させた際の知見を解説した記事がMozilla Add-ons Blogに掲載されました。
<!--more-->


  * [Mozilla Add-ons Blogに掲載された縮約版](https://blog.mozilla.org/addons/2016/04/26/migrating-popup-alt-attribute-from-xulxpcom-to-webextensions/)

  * [編集・縮約前の全文](http://piro.sakura.ne.jp/latest/blosxom/mozilla/xul/2016-04-19_webextensions.htm)

これは、筆者がアドオンのWebExtensions移行のための調査を進める中で、アドオンを1つ移行できた事についてMozillaのアドオン開発者向けメーリングリスト上に投稿した所、他のアドオン開発者にその時の知見を共有するゲスト記事の執筆を勧められたために書いた物です。
草稿段階では「WebExtensionsへの移行」に直接は関係していない前段階の準備にあたる話が多く含まれていたため、全文は筆者個人のブログで公開することにして、Mozilla Add-ons Blogには前段階の話を省いた内容が抜粋して掲載されています。

Mozilla Add-ons Blogは英語のブログである上に、記事自体もいくつかの前提が端折られています。
ここでは、WebExtensionsそのものの背景事情も含めて上記の記事の内容を紹介します。

### WebExtensionsとは

WebExtensionsは、Firefox用アドオンを開発するためのAPIであると同時に、アドオンの新しい形式そのものの名前でもあります。
現在はGoogle Chromeの拡張機能用のAPI群をFirefox上に再現する形で開発が進められており、未実装の機能の充実を急いでいる段階です。
また、Google ChromeのAPI群には含まれていない・Firefoxで独自に拡張したAPI群も追加される見込みで、既にそのようになっている部分もあります。

Firefoxアドオン向けのAPIを整備する試みは「FUEL」や「Add-on SDK（旧Jetpack）」など過去にもありました。
それらとWebExtensionsとの最大の違いは、*「WebExtensionsの導入」と「既存のアドオンの仕組みの廃止」が強く紐付いている*という点です。

Mozilla Firefoxはアドオンによって様々なカスタマイズができることが特徴です。
しかし、アドオンを開発するための基盤や互換性維持のための仕組みの整備が遅れていた事から、

  * Firefoxの更新に追従できず、動かなくなるアドオンが出てくる。

  * お気に入りのアドオンが動かなくなるので、ユーザーはFirefoxを更新したくなくなる。あるいは、お気に入りのアドオンが動かないということでFirefoxを使う動機が薄れ、Firefoxを使わなくなる。

  * ユーザー離れを防ぐために、アドオンに大きな影響を与える変更をFirefoxに対して入れにくくなる。Firefoxの根本的な設計を改良できない状態が続いてしまう。

といった具合に、Firefox自体の進歩にとってもユーザーにとっても望ましいとは言えない状態が発生してしまっています。

Add-on SDKはこのような状況の改善を目標の1つとしていましたが、現在は「アドオンを開発するための新しい推奨される方法」という位置付けに留まっており、すべてのアドオンをAdd-on SDKに移行させるには至りませんでした。
WebExtensionsではその反省もあってか、*当初から「すべてのアドオンのWebExtensionsへの移行」と「現在のアドオンの仕組みの将来的な廃止」を前面に打ち出しています*。
またそれと同時に、*どのようなAPIが必要なのかをアドオン作者にヒアリングする*という事も進められています。
これらの事から、過去の類似の取り組みとは腰の据え方が違うという印象を受けます。

### 従来型のFirefoxアドオンをWebExtensionsに移行する際のポイント

冒頭に挙げた記事では、[Popup ALT Attribute](https://addons.mozilla.org/firefox/addon/popup-alt-attribute/)というアドオンのWebExtensions移行の顛末を解説しています。

従来型のアドオンをWebExtensionsに移行する時の最も重要な点は、*「今までのアドオン開発のノウハウは一旦すべて忘れて、ゼロベースで考えること」*の一言に尽きます。

従来型のFirefox用アドオンは、本質的には「Firefox本体に動的に反映されるパッチ」と言うことができます。
そのため、機能性やメンテナンス性を強く意識して開発していくと、「Firefox本体の処理の中で期待通りの結果になっていない部分をピンポイントで書き換える」という、その時点でのFirefox本体の設計に強く依存した設計になってしまうことがあります。

Add-on SDKではいくつかの「抜け道」があり、それを使って「SDK以前の、今までのアドオン開発のノウハウ」を一部取り入れた開発」が可能となっていました。
それに対して、WebExtensionsにはそのような抜け道は一切用意されず、厳密にすべての要素がWebExtensionsの枠組みの中で実装される必要があります。
移行元のアドオンがFirefox本体の設計に強く依存した設計である場合、WebExtensionsへの移行は「同じ結果を得るアドオンを新規に再開発する」のに近くなります。
Popup ALT Attributesは、初出が2002年（※Firefox 1.0は2004年にリリースされたので、Firefoxより前からある事になる）と非常に歴史の古いアドオンなので、その典型的な例です。

実際の所、Popup ALT AttributeのWebExtensions移行は以下の3段階を経て実現されました。

  1. 再起動が必要なアドオンから、再起動不要なアドオン（Bootstrapped Extensions）への移行（2011年）

  1. シングルプロセス前提の設計から、マルチプロセス（e10s）対応の設計への移行（2016年2月）

  1. 従来のアドオンの枠組みから、WebExtensionsへの移行（2016年4月）

Bootstrapped Extensionsへの移行では、「動的な関数の書き換え」や「XULオーバーレイ」を廃止しました。
マルチプロセス（e10s）前提の設計への移行では、「Firefoxの内部的な処理に割り込んで任意の処理を行う設計」から、「Firefox内部の処理とは別の所で完結する設計」へ変更しました。
冒頭に挙げたMozilla Add-ons Blogに掲載された縮約版は、その後の最後のステップである、「アドオンを従来の形式からWebExtensionsの形式にパッケージングし直す」という工程にフォーカスした内容になっています。

ここでは3つの段階として書いていますが、実際には、1から3までが必ずこの順で行われる必要はありません。
例えば筆者が開発した別のアドオンでは、「1（再起動不要なアドオンへの移行）はできないが2（マルチプロセス対応）はできている」という物がいくつかあります。
既存のアドオンをWebExtensionsに移行できるかどうかは、*1と2を両方とも実現できていて、且つ、必要なAPIがWebExtensionsで既に提供されていること*が鍵となります。
Popup ALT Attributesはそのすべての条件を満たしていたため、つつがなくWebExtensionsに移行することができました。

### まとめ

筆者が行ったFirefox用アドオンのWebExtensionsへの移行の顛末を記したブログ記事が、Mozilla Add-ons Blogに掲載されました。

いくつかの条件を満たしているアドオンであれば、WebExtensionsへの移行はそう困難ではありません。
しかし、現状のWebExtensionsにAPIが不足している事は事実です。

Mozilla自身がレガシーなアドオンの廃止とWebExtensionsへの移行の推進に本気で取り組もうとしている今のタイミングであれば、必要なAPIのWebExtensionsへの追加提案も不可能ではないと考えられます。
アドオン作者の人はぜひ自作のアドオンのWebExtensions移行を試みて、躓きやすい箇所・APIが不足している箇所を早めに明らかにし、[dev-addons ML](https://mail.mozilla.org/listinfo/dev-addons)や[Bugzilla上でコンポーネントが「WebExtensions」と明示されているBug](https://bugzilla.mozilla.org/buglist.cgi?component=WebExtensions&product=Toolkit&bug_status=__open__)などを通じてMozillaにフィードバックしてみて下さい。
