---
tags:
- groonga
title: MariaDB コミュニティイベント in Tokyo開催のおしらせ
---
2016年7月21日（木）の午後に「MariaDB コミュニティイベント in Tokyo」というイベントが開催されます。MariaDBの開発に関わっている人（エヴァンジェリストの人）が来日しており、その人からMraiaDBの最新情報を説明してもらったり、その人に質問したりできます。他にも[All About](http://allabout.co.jp/)がどのようにMariaDBを使っているかという話（たぶん）や、MariaDBにバンドルされている[Mroonga](http://mroonga.org/ja/)（全文検索用プラグイン）・[Spider](http://spiderformysql.com/)（シャーディング用プラグイン）の最新情報の話もあります。MroongaとSpiderはそれぞれ開発している人が話をするので突っ込んだ質問をすることもできます。この機会をぜひ活かしてください。
<!--more-->


イベントに参加するには事前に登録する必要があります。以下のどのイベントページからでも登録できるので自分が使いやすいイベントページを選んでください。

  * Doorkeeper: [MariaDB Community Event in TOKYO](https://mariadb-community0721.doorkeeper.jp/events/48617)

  * connpass: [MariaDB コミュニティイベント in Tokyo](http://connpass.com/event/35245/)

  * Meetup: [MariaDB Community Event in TOKYO](http://www.meetup.com/ja-JP/Tokyo-MariaDB-Community-Event/events/232384187/)

無料のイベントなので、MariaDBの最新情報を知りたい、MariaDBがどのように使われているか知りたい、MariaDBでの全文検索・シャーディングについて知りたい、という方はぜひお気軽にお越しください。
