---
tags:
  - apache-arrow
title: Apache Arrow 0.3.0リリース（翻訳）
---
Apache Arrowの開発に参加している須藤です。Apache Arrowのコミッターにならない？と打診がきたので、そろそろコミッターになるかもしれません。最近、私以外の人がここに書くことが増えたので、わかりやすいように（？）私が書く時は記名して書くことにします。今回から。
<!--more-->


さて、2017年5月8日にApache Arrow 0.3.0がリリースされたので[リリースアナウンス](https://arrow.apache.org/blog/2017/05/08/0.3-release/)を翻訳します。翻訳記事なので、この記事のライセンスは[他の記事のライセンス](/license/#blog)と違い[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)です。元の記事の著作権者はApache Software Foundation（ASF）です。リリースアナウンスを書いたのは[Wes McKinney](http://wesmckinney.com/)さんです。[pandas](http://pandas.pydata.org/)の作者ですね。

---

Apache Arrowチームは0.3.0のリリースをアナウンスできてうれしいです。2月にリリースした0.2.0から10週間の活発な開発の結果が今回のリリースです。[**23人のコントリビューター**](https://github.com/apache/arrow/graphs/contributors)が[**306個のJIRAのissueを解決**](https://issues.apache.org/jira/issues/?jql=project%20%3D%20ARROW%20AND%20status%20in%20(Resolved%2C%20Closed)%20AND%20fixVersion%20%3D%200.3.0)しました。

複数のArrowの実装にたくさんの新しい機能を追加しています。2017年、特に注力して開発するのは、インメモリー用のフォーマット、型のメタデータ、メッセージング用のプロトコルです。これは、ビッグデータアプリケーションに**安定していてプロダクションで使える基盤**を提供するためです。高性能IOとインメモリーデータ処理にArrowを活用するために、[Apache Spark](http://spark.apache.org)・[GeoMesa](http://www.geomesa.org/)コミュニティーと協力していてとてもエキサイティングです。

それぞれのプラットフォームでArrowを使う方法は[インストールページ](http://arrow.apache.org/install)を見てください。（訳注：ここには載っていませんが、私が用意した[非公式のAPT/YUMリポジトリー](https://github.com/red-data-tools/arrow-packages)もあります。Apache ArrowだけでなくApache Parquetのパッケージもあります。リリース版ではなく未リリースの最新版のパッケージです。）

Arrowでビッグデータシステムを高速化するケースを増やすために、近いうちにApache Arrowのロードマップを公開する予定です。

Arrowの開発に参加するコントリビューターを募集中です！すでにArrowの開発に参加しているコミュニティーからのコントリビューターもそうですし、まだ参加していないGo、R、Juliaといったコミュニティーからのコントリビューターも募集中です！

#### ファイルフォーマットとストリーミングフォーマットの強化

0.2.0では**ランダムアクセス**用と**ストリーミング**用のArrowのワイヤーフォーマット（訳注：データ交換用のフォーマット？）を導入しました。実装の詳細は[IPC仕様](http://arrow.apache.org/docs/ipc.html)を見てください。ユースケースは[使用例を紹介したブログ](http://wesmckinney.com/blog/arrow-streaming-columnar/)を見てください。これらのフォーマットを使うと低オーバーヘッド・コピーなしでArrowのレコードバッチのペイロードにアクセスできます。

0.3.0ではこのバイナリーフォマットの細かい詳細をたくさん固めました。Java、C++、Python間の連携のテストおよびそれぞれ言語での単体テストの整備も進めました。[Google Flatbuffers](http://github.com/google/flatbuffers)は、前方互換性を壊さずにメタデータに新しい機能を追加するのに非常に助かりました。

まだバイナリーフォーマットの前方互換性を必ず壊さないと約束できる状態ではありませんが（もしかしたら変更する必要があるなにかが見つかるかもしれない）、メジャーリリース間では不必要に互換性を壊さないように努力するつもりです。Apache ArrowのWebサイト、各コンポーネントのユーザー向けのドキュメントおよびAPIドキュメントへのコントリビューションを非常に歓迎します。

#### 辞書エンコーディングのサポート

[GeoMesa](http://www.geomesa.org/)プロジェクトの[Emilio Lahr-Vivaz](https://github.com/elahrvivaz)はJavaのArrow実装に辞書エンコード対応ベクターをコントリビュートしました。これを受けて、C++とPythonでもサポートしました。（`pandas.Categorical`とも連携できます。）辞書エンコーディング用のインテグレーションテスト（C++とJava間でこのデータを送受信するテスト）はまだ完成していませんが、0.4.0までには完成させたいです。

これはカテゴリーデータ用の一般的なデータ表現テクニックです。これを使うと、複数のレコードバッチで共通の「辞書」を共有し、各レコードバッチの値はこの辞書を参照する整数になります。このデータは統計的言語（statistical language）の分野では「カテゴリー（categorical）」や「因子（factor）」と呼ばれています。（訳注：日本語での呼び方はあっています？）Apache Parquetのようなファイルフォーマットの分野ではデータ圧縮のためだけに使われています。

#### 日付、時刻、固定長型の拡張

0.2.0では現実に使われている日付・時刻型をインテグレーションテスト付きで完全にサポートすることを諦めました。これらは[Apache Parquet](http://parquet.apache.org)とApache Sparkとの連携に必要な機能です。

  * **日付**: 32-bit（日単位）と64-bit（ミリ秒単位）

  * **時刻**: 単位付き64-bit整数（単位：秒、ミリ秒、マイクロ秒、ナノ秒）

  * **タイムスタンプ（UNIXエポックからの経過時間）**: 単位付き64-bit整数のタイムゾーン付きとタイムゾーンなし

  * **固定長バイナリー**: 決まったバイト数のプリミティブな値

  * **固定長リスト**: 各要素が同じサイズのリスト（要素のベクターとは別にオフセットのベクターを持つ必要がない）

C++のArrow実装では、[Boost.Multiprecision](https://github.com/boostorg/multiprecision)を使ったexactな小数のサポートを実験的に追加しました。ただし、Java実装とC++実装間での小数のメモリーフォーマットはまだ固まっていません。

#### C++とPythonのWindowsサポート

一般的なC++とPythonでの開発用に、パッケージ周りの改良も多数入っています。0.3.0はVisual Studio（MSVC）2015と2017を使ってWindowsを完全にサポートした最初のバージョンです。AppveyorでMSVC用のCIを実行しています。Windows上でソースからビルドするためのガイドも書きました。[C++](https://github.com/apache/arrow/blob/master/cpp/doc/Windows.md)用と[Python](https://github.com/apache/arrow/blob/master/python/doc/source/development.rst)用。

[conda-forge](https://conda-forge.github.io)からWindows用のArrowのPythonライブラリーをインストールできます。

```shell
conda install pyarrow -c conda-forge
```


#### C（GLib）バインディングとRuby・Lua・他のサポート

[Kouhei Sutou](http://github.com/kou)（訳注：私です）は新しいApache Arrowのコントリビューターです。Linux用の（ArrowのC++実装の）GLibを使ったCバインディングをコントリビュートしました。[GObject Introspection](https://wiki.gnome.org/Projects/GObjectIntrospection)というCのミドルウェアを使うことでRuby、Lua、Goや[他にも様々なプログラミング言語](https://wiki.gnome.org/Projects/GObjectIntrospection/Users)でシームレスにバインディングを使うことができます。これらのバインディングがどのように動いているか、これらのバインディングをどのように使うかを説明するブログ記事が別途必要な気がします。

#### PySparkを使ったApache Sparkとの連携

[SPARK-13534](https://issues.apache.org/jira/browse/SPARK-13534)でApache Sparkコミュニティーと協力しています。PySparkでの`DataFrame.toPandas`をArrowを使って高速化しようとしています。効率的なデータのシリアライズにより[**40倍以上高速化**](https://github.com/apache/spark/pull/15821#issuecomment-282175163)できるケースがあります。

PySparkでArrowを使うことでこれまでできなかったパフォーマンス最適化の道が開けました。特に、UDFの評価まわりでいろいろやれることがあるでしょう。（たとえば、Pythonのラムダ関数を使って`map`・`filter`を実行するケース。）

#### Python実装での新しい機能：メモリービュー、Feather、Apache Parquetのサポート

ArrowのPythonライブラリーである`pyarrow`は`libarrow`と`libarrow_python`というC++ライブラリーのCythonバインディングです。`pyarrow`はNumPyと[pandas](http://pandas.pydata.org)とPythonの標準ライブラリー間のシームレスな連携を実現します。

ArrowのC++ライブラリーで最も重要なものは`arrow::Buffer`オブジェクトです。これはメモリービューを管理します。コピーなしの読み込みとスライスをサポートしている点が重要です。[Jeff Knupp](https://github.com/JeffKnupp)はArrowのバッファーとPythonのバッファープロトコルとmemoryviewの連携処理をコントリビュートしました。これにより次のようなことができるようになりました。

```python
In [6]: import pyarrow as pa

In [7]: buf = pa.frombuffer(b'foobarbaz')

In [8]: buf
Out[8]: <pyarrow._io.Buffer at 0x7f6c0a84b538>

In [9]: memoryview(buf)
Out[9]: <memory at 0x7f6c0a8c5e88>

In [10]: buf.to_pybytes()
Out[10]: b'foobarbaz'
```


C++でのParquet実装である[parquet-cpp](https://github.com/apache/parquet-cpp)を使うことで大幅に[**Apache Parquet**](http://parquet.apache.org)サポートを改良しました。たとえば、ディスク上にあるかHDFS上にあるか関係なく、パーティションされたデータセットをサポートしました。[Daskプロジェクト](https://github.com/dask/dask/commit/68f9e417924a985c1f2e2a587126833c70a2e9f4)はArrowを使ったParquetサポートを実装した最初のプロジェクトです。Dask開発者とはpandsデータを分散処理する文脈でさらに協力できることを楽しみにしています。

pandasを成熟させるためにArrowを改良することもあり、[**Featherフォーマット**](https://github.com/wesm/feather)の実装をマージしたのもその1つです。Featherフォーマットは本質的にはArrowのランダムアクセスフォーマットの特別なケースの1つです。ArrowのコードベースでFeatherの開発を続けます。たとえば、今のFeatherはArrowのPythonバインディングのレイヤーを使うことでPythonのファイルオブジェクトを読み書きできるようになっています。

`DatetimeTZ`や`Categorical`といったpandas固有のデータ型のちゃんとした（robust）サポートも実装しました。

#### C++ライブラリーでのテンソルサポート

Apache Arrowはコピーなしで共有メモリーを管理するツールという側面があります。機械学習アプリケーションの文脈でこの機能への関心が増えています。UCバークレー校の[RISELab](https://rise.cs.berkeley.edu/)の[Rayプロジェクト](https://github.com/ray-project/ray)が最初の例です。

機械学習ではは「テンソル」とも呼ばれる多次元配列というデータ構造を扱います。このようなデータ構造はArrowのカラムフォーマットがサポートしているデータ構造の範囲を超えています。今回のケースでは、[`arrow::Tensor`](http://arrow.apache.org/docs/cpp/classarrow_1_1_tensor.html)というC++の型を追加で実装しました。これはArrowのコピーなしの共有メモリー機能を活用して実装しました。（メモリーの生存期間の管理に`arrow::Buffer`を使いました。）C++実装では、これからも、共通のIO・メモリー管理ツールとしてArrowを活用できるようにするため、追加のデータ構造を提供するつもりです。

#### JavaScript（TypeScript）実装の開始

[Brian Hulette](https://github.com/TheNeuralBit)はNodeJSとWebブラウザー上で動くアプリケーションで使うために[TypeScript](https://github.com/apache/arrow/tree/master/js)でのArrowの実装を始めました。FlatBuffersがJavaScriptをファーストクラスでサポートしているので実装が捗ります。

#### Webサイトと開発者用ドキュメントの改良

0.2.0をリリースしてからドキュメントとブログを公開するためにWebサイトのシステムを[Jekyll](https://jekyllrb.com)ベースで作りました。Kouhei Sutou（訳注：私です）は[Jekyll Jupyter Notebookプラグイン](https://github.com/red-data-tools/jekyll-jupyter-notebook)を作りました。これによりArrowのWebサイトのコンテンツを作るためにJupyterを使うことができます。

WebサイトにはC、C++、Java、PythonのAPIドキュメントを公開しました。これらの中にArrowを使い始めるための有益な情報を見つけられるでしょう。

#### コントリビューター

このリリースにパッチをコントリビュートしたみなさんに感謝します。（訳注：上から2番目が私です。）

```
$ git shortlog -sn apache-arrow-0.2.0..apache-arrow-0.3.0
    119 Wes McKinney
     55 Kouhei Sutou
     18 Uwe L. Korn
     17 Julien Le Dem
      9 Phillip Cloud
      6 Bryan Cutler
      5 Philipp Moritz
      5 Emilio Lahr-Vivaz
      4 Max Risuhin
      4 Johan Mabille
      4 Jeff Knupp
      3 Steven Phillips
      3 Miki Tebeka
      2 Leif Walsh
      2 Jeff Reback
      2 Brian Hulette
      1 Tsuyoshi Ozawa
      1 rvernica
      1 Nong Li
      1 Julien Lafaye
      1 Itai Incze
      1 Holden Karau
      1 Deepak Majeti
```


---

以上が0.3.0の[リリースアナウンス](https://arrow.apache.org/blog/2017/05/08/0.3-release/)です。Apache Arrowを使いたくなってきましたか？

Apache Arrowに興味が出てきた人は今月（2017年5月）次のイベントでApache Arrow関連の話題があるのでぜひ参加してください。

  * [DataScience.rb ワークショップ 〜ここまでできる Rubyでデータサイエンス〜 - Rubyアソシエーション | Doorkeeper](https://rubyassociation.doorkeeper.jp/events/59965)

  * [【5/28大阪】データ分析用次世代データフォーマットApache Arrow勉強会 - connpass](https://classmethod.connpass.com/event/56478/)

Apache Arrow関連のイベントを開催したい！という方は私（[@ktou](https://twitter.com/ktou)）に相談してください。Apache Arrowがデファクトスタンダードになって開発に参加する人がもっと増えるといいなぁと思っているので一緒に協力してやっていきたいです。
