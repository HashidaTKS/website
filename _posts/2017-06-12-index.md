---
tags:
  - apache-arrow
  - presentation
title: 'データ分析用次世代データフォーマットApache Arrow勉強会（大阪） #osaka_arrow'
---
須藤です。このあいだ試したらApache Arrowのリポジトリーにpushできたので私のコミット権関連の設定は完了しているようです。
<!--more-->


東京近郊に住んでいる人向け情報：この大阪で開催したイベントと同じような内容のイベントを6月13日（明日！）に東京でも開催します！大阪だったので参加できなかったという方は、ぜひ東京でのイベントに参加してください。

  * [データ分析用次世代データフォーマットApache Arrow勉強会](https://speee.connpass.com/event/58765/)

2週間ほど前になりますが、2017年5月28日に[大阪でApache Arrowの勉強会](https://classmethod.connpass.com/event/56478/)を開催しました。前日に[関西Ruby会議2017で発表]({% post_url 2017-05-29-index %})していたのですが、せっかく大阪に来たのでApache Arrowの普及活動をしようと思い、開催しました。会場は[クラスメソッドさん](https://classmethod.jp/)に貸してもらいました。日曜日なのに会社を開けてもらってありがとうございます！

なお、クラスメソッドさんに会場提供をお願いしたのは[OSS Gate大阪ワークショップ](http://dev.classmethod.jp/study_meeting/event-report-oss-gate-osaka/)でつながりがあったからです。思わぬところで意外なつながりが活きていて、ありがたいことです。

当日使った資料は次の通りです。中盤までは既存の情報を紹介したので、最初の方のページはそれらの情報へのリンク集になっています。終盤は今回用にまとめた情報を紹介したので、それらの情報がこのスライドのメインの内容になります。イベントの内容は[クラスメソッドさんがまとめてくれたイベントレポート](http://dev.classmethod.jp/server-side/apache-arrow-workshop-in-osaka/)を参照してください。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/apache-arrow-osaka/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/apache-arrow-osaka/" title="Apache Arrow">Apache Arrow</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/apache-arrow-osaka/)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-apache-arrow-osaka)

大阪で開催したApache Arrow勉強会を紹介しました。なんと、6月13日（明日！）に同様のイベントが東京でも開催されます！

  * [データ分析用次世代データフォーマットApache Arrow勉強会](https://speee.connpass.com/event/58765/)

会場提供は[Speeeさん](http://speee.jp/)です。Speeeさんとは[OSS開発支援]({% post_url 2017-05-17-index %})や[DataScience.rb]({% post_url 2017-05-22-index %})などでつながりがあり、会場を提供してもらえました。ありがとうございます！
