---
tags:
- groonga
- presentation
title: '第115回 PHP勉強会＠東京：PHPでPostgreSQLとPGroongaを使って高速日本語全文検索！ #phpstudy'
---
はじめてまともにPHPを書いた須藤です。
<!--more-->


[第115回 PHP勉強会＠東京](https://phpstudy.doorkeeper.jp/events/61730)で「PHPでPostgreSQLとPGroongaを使って高速全文検索！」という話をしました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/php-study-115/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/php-study-115/" title="PHPでPostgreSQLとPGroongaを使って高速日本語全文検索！">PHPでPostgreSQLとPGroongaを使って高速日本語全文検索！</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/php-study-115/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/phpstudy115)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-php-study-115)

### 内容

[PGroonga](https://pgroonga.github.io/ja/)を使えばPHPで簡単にリッチで高速な全文検索システムを作れるよ！ということを伝えたかったので、実際にPHPで「PHPのドキュメントを全文検索するシステム」を作ってそれを紹介しました。詳細はスライドで説明していますが、次のような機能があります。

  * 探したいものが見つかる高速全文検索機能（基本機能）

    * [公式ドキュメント](http://php.net/manual/ja/index.php)の検索は関数やクラス名の検索のみで説明文では検索できない

    * Google custom searchを使ったサイト内検索機能は一般的な文書の検索に特化しているため、PHP固有の[「@」で「エラー制御演算子」を検索](http://php.net/results.php?q=%40&l=ja&p=all)することができない（[英語として検索](http://php.net/results.php?q=%40&l=en&p=all)すると「at sign」でヒットする）

  * キーワード周辺のテキストをハイライト（基本機能）

  * 入力補完（基本機能）

    * ローマ字でも入力補完可能（リッチな機能）

      * 例：「seiki」→「正規表現」

サービスとしては動かしていないのですぐに試せませんが、[ソースコード](https://github.com/kou/php-document-search)はフリーソフトウェアとして公開しているのでローカルで動かすことができます。動かし方のドキュメントは書いていませんが、標準的なLarabelの使い方だと思っているので、Larabelを使ったことがある人なら動かせるのではないかと思います。

私はこれまでまともにPHPを書いたことがありませんでしたが、3日で実装できました。簡単にリッチで高速な全文検索システムを作れることが示せたのではないかと思います。

### おねがい

このイベントのために作った[PHP document search](https://github.com/kou/php-document-search)ですが、PHPユーザーにとって有用なサービスになるのではないかと思うので、だれかサービスとして運用したり、メンテナンスしたりしませんか！？私は日常的にPHPを書くことはないのでサービスとして運用する予定はないのですが、技術的なサポートはするつもりです。

興味がある方は[issueで連絡](https://github.com/kou/php-document-search/issues/new)してください。

### まとめ

PHPユーザーにPGroongaを紹介する機会があったのでPHPのドキュメントを検索する[PHP document search](https://github.com/kou/php-document-search)を作ってそれを元にPGroongaを紹介しました。PHP document searchの運営者・メンテナーを募集しているので、ぜひ[ご連絡](https://github.com/kou/php-document-search/issues/new)ください！

### おしらせ

8月1日（火）14:00-16:00に[MySQL・PostgreSQL上で動かす全文検索エンジン「Groonga」セミナー](https://groonga.doorkeeper.jp/events/62741)を開催します。PGroongaを使いたくなった人はぜひこの機会をご活用ください！
