---
tags:
- groonga
- presentation
title: 'db tech showcase Tokyo 2017 - MySQL・PostgreSQLだけで作る高速でリッチな全文検索システム #dbts2017'
---
この発表のために[`mroonga_query_expand()`を実装した](https://github.com/mroonga/mroonga/commit/15911341d4e826c54d5d088f30a0eef6a7259e0d)須藤です。db tech showcase Tokyo 2017で「MySQL・PostgreSQLだけで作る高速でリッチな全文検索システム」という話をしました。一昔前の全文検索システムはそこそこ速く全文検索してキーワードをハイライト表示できれば普通でしたが、最近の全文検索システムはそれだけだと機能不足で、オートコンプリートやクエリー展開や関連エントリー表示機能などがあって普通というくらいにユーザーの要求レベルがあがっています。これは、GoogleやAmazonなど便利な検索サービスに慣れたユーザーが増えたためです。そんな時代の変化に対応できる全文検索エンジンが[Groonga](http://groonga.org/ja/)です。GroongaはMySQL・MariaDB・PostgreSQLから使えるためSQLを使って簡単にイマドキの全文検索システムを実装できます。しかも、運用も楽です。そんな話です。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-tokyo-2017/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-tokyo-2017/" title="MySQL・PostgreSQLだけで作る高速でリッチな全文検索システム">MySQL・PostgreSQLだけで作る高速でリッチな全文検索システム</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-tokyo-2017/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/dbtechshowcasetokyo2017)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-db-tech-showcase-tokyo-2017)

### 内容

まず、どういうときにMroonga・PGroongaを使うアプローチを選べばよいかという指針を示しました。

  * 全文検索の知識ナシ

    * 全文検索だけできれば十分

      * データが少ない（数十万件とか）：RDBMSで`LIKE`

      * データが多い：Mroonga・PGroonga

    * イマドキの機能が必要

      * Mroonga・PGroonga

  * 全文検索の知識アリ

    * カリカリにチューニングしたい

      * RDBMS + 全文検索サーバー

    * それ以外

      * Mroonga・PGroonga

次に、以下の機能をMroonga・PGroongaで実現するにはどういうSQLを使えばよいか説明しました。

  * 全文検索

  * キーワードハイライト

  * 周辺テキスト表示

  * 入力補完

  * 同義語展開

  * 関連文書の表示

最後に、次のステップとして構造化データ（オフィス文書・HTML・PDFなど）の対応方法について少し言及しました。Groongaプロジェクトは構造化データからテキスト・メタデータ・スクリーンショットを取得するツールとして[ChupaText](https://github.com/ranguba/chupa-text)を開発しています。コマンドラインでもHTTP経由でもライブラリーとしても使えます。HTTP経由で使う場合はDocker・Vagrantを使うのが便利です。依存ライブラリーを揃える手間がないからです。

  * [ChupaText on Vagrant](https://github.com/ranguba/chupa-text-vagrant)

  * [ChupaText on Dokcer](https://github.com/ranguba/chupa-text-docker)

### まとめ

Mroonga・PGroongaを使ってイマドキの全文検索システムを実装する方法を紹介しました。コンサルティングやチューニングや開発支援などを提供するサポートサービスがあります。社外秘のデータでお困りの場合は[お問い合わせ](/contact/?type=groonga)ください。NDAを結んだ上で対応できます。

Mroongaはインサイト・テクノロジーさんが進めているPinkshiftでも活用されています。MySQL・MariaDB・PGroongaで高速全文検索が必要ならMroonga・PGroongaを試してみてください。
