---
tags: []
title: Lua用のCSSセレクターパーサーライブラリー LuaCS
---
Lua用の使いやすいCSSセレクターパーサーライブラリーを開発しました。これは、クリアコードが[株式会社セナネットワークス様](https://www.sena-networks.co.jp/)からの発注を受けて開発したライブラリーです。
<!--more-->


LuaCS(るあっくす)といいます。MITライセンスで公開しています。

LuaCSは、CSSセレクターをXPathに変換する機能を提供しています。
また、[XMLua](https://clear-code.github.io/xmlua/ja/)と連携して、XML/HTMLのDOM内のノードをCSSセレクターで検索できます。

### インストール方法

ライブラリーは、[LuaRocks](https://luarocks.org/)で公開しており、`luarocks`コマンドで簡単にインストールできます。

例えば、Debian GNU/Linuxでは以下のようにインストールします。

```shell
% sudo luarocks install luacs
```


Debian GNU/Linux以外のOSでのインストールは、[LuaCS - インストール](https://clear-code.github.io/luacs/ja/install/)を参照してください。

LuaCSは、Debian GNU/Linuxの他に、Ubuntu、CentOS、macOSに対応しています。

### 主な機能

LuaCSの主な機能を紹介します。

LuaCSを使うとLuaで以下のことができます。

#### CSSセレクターをXPathへ変換する

LuaCSを使って、簡単にCSSセレクターをXPathへ変換できます。

具体的には、`to_xpaths`という関数を使って以下のように変換します。

```lua
local luacs = require("luacs")

-- CSSセレクターをXPathに変換
local xpaths = luacs.to_xpaths("*, .class, #id")

for _, xpath in ipairs(xpaths) do
  print(xpath)
-- /descendant::*
-- /descendant::*[@class][contains(concat(' ', normalize-space(@class), ' '), ' class ')]
-- /descendant::*[@id='id' or @name='id']
end
```


上記のように、`to_xpath`の引数には、`,`区切りで複数のCSSセレクターを指定できます。

#### XMLuaと連携して、XML/HTMLのDOM内のノードをCSSセレクターを使って検索する。

[Lua用HTML・XML処理ライブラリー XMLua]({% post_url 2017-12-25-index %}) は、version 1.0.4 からCSSセレクターを用いたノードの検索をサポートしています。
XMLuaのCSSセレクターを用いた検索機能は、LuaCSを使って実現しています。

CSSセレクターを用いた検索は、以下のように行います。

検索条件にマッチしたノードは、Luaのテーブルとして取得できますので、`[]`を用いたアクセスや`#`を用いて、検索条件にマッチしたノード数を取得できます。

```lua
local xmlua = require("xmlua")

local xml = [[
<root>
  <sub1 class="A" id="CB"/>
  <sub1 class="B"/>
  <sub1 class="C">
    <sub1 class="B" id="CB"/>
  </sub1>
  <sub2 class="B"/>
  <sub1 class="D"/>
</root>
]]

local document = xmlua.XML.parse(xml)

-- class="B" または、id="CB"の要素をすべて検索します。
local all_classes = document:css_select(".B, #CB")

-- "#"を使ってマッチしたノードの数を出力できます。
print(#all_classes) -- -> 4

-- "[]"を使って、N番目のノードにアクセスできます。
print(all_classes[1]:to_xml()) -- -> <sub1 class="B"/>
print(all_classes[2]:to_xml()) -- -> <sub1 class="B" id="CB"/>
print(all_classes[3]:to_xml()) -- -> <sub2 class="B"/>
print(all_classes[4]:to_xml()) -- -> <sub1 class="A" id="CB"/>
```


### おわりに

LuaCSの主な機能を紹介しました。より詳しい内容については、以下のドキュメントも参照してください。

[LuaCS - リファレンス](https://clear-code.github.io/luacs/ja/reference/)

[LuaCS - チュートリアル](https://clear-code.github.io/luacs/ja/tutorial/)

このライブラリーは、[XMLua](https://clear-code.github.io/xmlua/ja/)と同様[株式会社セナネットワークス様](https://www.sena-networks.co.jp/)からの依頼を受けて開発した受託開発の成果物です。

成果物をフリーソフトウェアとして公開すると、様々なユーザーがライブラリーを使うことによって、いままで気が付かなかったバグを発見できたり、ユーザーからの要望によって、当初想定していなかった、便利な新機能を実装するきっかけとなったり、様々なメリットがあます。

このように成果物の公開によって、ライブラリーの品質も高まるので、お客さんにとっても成果物を公開するメリットがあります。

ご依頼主の[株式会社セナネットワークス様](https://www.sena-networks.co.jp/)には、上記のようなメリットにご理解をいただき、成果を公開できました。ありがとうございます！
