---
tags:
  - apache-arrow
title: Apache Arrow 0.11.0リリース
---
Apache Arrow 0.11.0のリリースマネージャーをした須藤です。
<!--more-->


2018年10月8日に[Apache Arrow 0.11.0をリリース](https://arrow.apache.org/blog/2018/10/09/0.11.0-release/)しました。

### 0.11.0の新機能

0.10.0のリリースから2ヶ月くらいしか経っていないのですが、今回のリリースはすごくアグレッシブです。まだ荒削りのものが多いのですが、新しく次の機能が入りました。

  * RPC機能

    * Apache Arrow Flightという名前がついている

  * Apache Parquet C++

    * 別リポジトリーで開発していたが密に連携しているのでApache Arrowのパッケージ含めることになった

  * Apache Parquet GLib

    * 別リポジトリーで開発していたがApache Parquet C++がApache Arrowに含まれることになったのでこれも含めることになった

  * LLVMを使った実行エンジン

    * Gandivaという名前がついている

  * CSVパーサー

    * CSVを高速に読み込んでApache Arrowのデータとして処理できるようになる

  * R実装

    * C++のバインディングとして実装

  * MATLAB実装

    * C++のバインディングとして実装

[Apache Arrowの最新情報（2018年9月版）]({% post_url 2018-09-05-index %})でまとめた内容のいくつかはすでに古いものになってしまいました。開発が活発ですね！リリース後に[.NET実装](https://github.com/feyenzylstra/apache-arrow)も現れています。

### 開発者を増やしたい！

活発な開発をさらに活発にするために、12月8日（土）に[Apache Arrow東京ミートアップ2018](https://speee.connpass.com/event/103514/)という「開発者を増やすこと」が目的のイベントを開催します！

開発者を増やしたいプロジェクトはApache Arrowはもちろんですが、Apache Arrow以外のデータ処理関連ソフトウェアもです。たとえば、Apache SparkやApache Hadoop関連の開発者も増やしたいですし、R関連の開発者も増やしたいですし、Ruby用のデータ処理ツールの開発者も増やしたいです。

開発に参加したいけど踏み出せていなかったという人はこの機会をぜひ活用してください！開発に参加する人を増やすためのいろいろな仕掛け（？フォロー？）を準備しています。

「Apache Arrow東京ミートアップ2018」は今のところ以下のプロジェクトの開発者・関係者が協力しています。

  * Apache Arrow

  * Apache Spark

  * R

  * Ruby（Red Data Tools）

他のプロジェクトのみなさんにも協力して欲しいので、開発者が増えることに興味がある人はぜひ[@ktou](https://twitter.com/ktou)に連絡してください。たとえば、次のような界隈のプロジェクトです。

  * Python（pandasとか）

  * JavaScript

  * Julia

  * Go

  * Rust

  * MATLAB

  * GPU

Ruby用のデータ処理ツールを開発する[Red Data Toolsプロジェクト](https://red-data-tools.github.io/ja/)もこのイベントに協力しています。

そんなRed Data Toolsは毎月開発イベントを開催しています。今月は[来週の火曜日（2018年10月16日）開催](https://speee.connpass.com/event/102968/)です。「Apache Arrow東京ミートアップ2018」を待たずにApache Arrow本体やRuby用のデータ処理ツールの開発に参加したい人はぜひどうぞ。「Apache Arrow東京ミートアップ2018」後には翌週の火曜日（2018年12月11日）に開催します。

### まとめ

Apache Arrow 0.11.0をリリースしたので自慢しました。

Apache Arrowを含むデータ処理関連プロジェクトの開発者を増やすためのイベントをすることを宣伝しました。
