---
tags:
- feedback
title: 片手でのキーボード入力を支援するソフトウェアについて
---
### はじめに

怪我などで手首などを負傷してしまうと、満足に両手でのタイピングが行えない事態に陥ることがあります。
今回は、そんなときでも怪我した側の手にあまり負担をかけずに入力するための方法を紹介します。[^0]
<!--more-->


### Half KeyboardもしくはHalf QWERTY

片手だけで入力できるようにしようというアイデアはそう目新しいものではなく、昔からあるようです。

  * [Matias, E., MacKenzie, I. S., & Buxton, W. (1993). Half-QWERTY: A one-handed keyboard facilitating skill transfer from QWERTY](https://www.billbuxton.com/matias93.html)

Half KeyboardもしくはHalf QWERTYなどで検索するといろいろでてくるので興味があれば調べてみるのもよいでしょう。
専用のキーボードが製品化されていたり、ソフトウェアで特定のキーが押されたらレイアウトを変更するというものもあります。

  * [Mirrorboard: A one-handed keyboard layout for the lazy](https://blog.xkcd.com/2007/08/14/mirrorboard-a-one-handed-keyboard-layout-for-the-lazy/)

Mirrorboardの場合は、キー配列の定義を差し替えることでCapsLockを押したらキーレイアウトが左右反転するようになっています。

### xhkによる片手キーボード入力

専用のキーボードの購入は敷居が高いので、ソフトウェアによる片手入力を試してみましょう。
今回そのうちの一つであるxhkを使ってみます。

  * [XLib HalfKeyboard implementation](https://github.com/kbingham/xhk)

xhkは次の手順でインストールできます。

```console
% git clone https://github.com/kbingham/xhk.git
% cd xhk
% sudo apt install build-essential autoconf automake pkg-config
% sudo apt install libx11-dev libxi-dev libxtst-dev
% ./autogen.sh
% ./configure
% make
% sudo make install
```


使用するには、xhkコマンドを実行するだけです。（最初から反転した状態にするには `-m` オプションを指定します。）

```console
% xhk
```


xhkを起動後、SPACEキーを入力するとレイアウトが左右反転するようになっています。

![通常のキーレイアウト]({{ "/images/blog/20181121_0.png" | relative_url }} "通常のキーレイアウト")

![SPACEキーを押したときのキーレイアウト]({{ "/images/blog/20181121_1.png" | relative_url }} "SPACEキーを押したときのキーレイアウト")

上が通常のキーレイアウトで、下がSPACEキーを押したときのキーレイアウトです。
SPACEキーを押すと、QのキーはPの位置にというように左右反転した位置に配置されます。そのため本来左手で押していたキーも右手で打てるようになっています。
（このキー配列の画像は、http://www.keyboard-layout-editor.com/ で作成しました。あらかじめ選択できる候補にHalf Keyboardがなかったので、[フィードバック](https://github.com/ijprest/keyboard-layout-editor/pull/249) しておきました。）

例えば、「わたしの」（WATASHINO）と右手だけでローマ字入力したい場合には次のようにキーを押すと入力できます。

キー入力 | 印字内容
--------|----------
SPACE+O| W
SPACE+;| A
SPACE+Y| T
SPACE+;| A
SPACE+L| S
H| H
I| I
N| N
O| O

入力したい文章によってはキーレイアウトを左右反転させるためにSPACEキーを頻繁に打つことになるので、右手をそれなりに酷使することになります。[^1]

なお、xhkがパッケージとしてインストールできると導入しやすくなって嬉しいだろうということで、パッケージ化の作業を進めやすくするために[フィードバック](https://github.com/kbingham/xhk/issues/7)しておきました。またドキュメントの記述がやや古かったのでそちらも[報告](https://github.com/kbingham/xhk/issues/8)しておきました。

### まとめ

今回は、片手でのキーボード入力を支援するソフトウェアのひとつであるxhkを紹介しました。
もし、片手でなんとかキーボード入力しないといけない事態になったら、選択肢の一つとして試してみるのもよいかもしれません。

[^0]: 怪我をしたほうの手に負担をかけないことが主目的なので、怪我をしていないほうの手にはどうしても負荷がかかります。

[^1]: 慣れないうちはキーレイアウトの変更に頭がついていかずにかなり打ち間違えたりします。
