---
tags:
  - apache-arrow
  - presentation
title: 日本OSS推進フォーラム アプリケーション部会 第10回勉強会 - Apache Arrow - データ処理ツールの次世代プラットフォーム
---
日本OSS推進フォーラム一般会員の1企業クリアコードの須藤です。
<!--more-->


[日本OSS推進フォーラム アプリケーション部会 第10回勉強会](https://ossforum.connpass.com/event/109902/)でApache Arrowを紹介しました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/oss-forum-apache-arrow/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/oss-forum-apache-arrow/" title="Apache Arrow - データ処理ツールの次世代プラットフォーム">Apache Arrow - データ処理ツールの次世代プラットフォーム</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/oss-forum-apache-arrow/)

  * [スライド（SlideShare）](https://slideshare.net/kou/ossforumapachearrow)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-oss-forum-apache-arrow)

### 内容

どういう人たちが来そうか予想できなかったので、あまりデータ処理まわりを知らない人にも雰囲気が伝わるといいなぁくらいのレベル感の内容にしました。技術的に突っ込んだ内容は少なめにしてこんなことができるよというのを網羅的に紹介したつもりです。

数年後、Apache Arrowがもっと普及したときに、この勉強会に参加した人たちが「あぁ、それ数年前から知っていたよー」と言えるようになるといいなぁというのを狙いました。

が、「アプリケーションのユーザーから見てApache Arrowで具体的にどううれしいの？」という問の答えとしては微妙だったなぁという感触でした。データ処理ツールを開発している人には「ここが速くなるのはこのケースでうれしい」とか「この機能はここでうれしい」とか伝わるのですが、データ処理ツールを使っている人にはイメージしづらいということがわかりました。ユーザーにとっても「速くなる」はうれしいはずですが、挙動は変わらないし、まだ動くものが少ないので体感できないしでピンとこないようです。

1,2年もすればApache Arrowを活用したプロダクトがいろいろでてくるはずなので、このプロダクトではこううれしくなった、あのプロダクトでは…と紹介できるようになるだろうなぁと思います。そうすればアプリケーションのユーザーからもイメージしやすくなりそうです。早くそんな状況になるためにも開発を進めたりデータ処理ツールを開発している人たちに紹介できるといいんじゃないかと思いました。

なお、もっと突っ込んだ話は[Apache Arrow東京ミートアップ2018]({% post_url 2018-12-10-index %})で紹介します。この集まりにはデータ処理ツールを開発するような人たちが来る集まりです。

### まとめ

日本OSS推進フォーラム アプリケーション部会 第10回勉強会でApache Arrowを紹介しました。

アプリケーションのユーザーにApache Arrowのよさを説明することが難しいという知見を得ました。

クリアコードはApache Arrowを活用して[データ処理ツールの開発をしたい]({% post_url 2018-07-11-index %})のでデータ処理ツールを開発していてApache Arrowを活用したい方は[お問い合わせ](/contact/?type=data-processing-tool)ください。
