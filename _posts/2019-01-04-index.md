---
tags: []
title: Linuxでキーボード入力の補助手段としてフットスイッチを活用するには
---
### はじめに

以前[片手でのキーボード入力を支援するソフトウェアについて](2018/11/21)というタイトルで、怪我などで手首などを負傷してしまったときに使える入力方法の一つとしてHalf QWERTY（とそれをソフトウェアとして実装した`xhk`）を紹介しました。
ある程度は`xhk`を使うことでなんとかなるのですが、それでも打ちにくいキーというのは存在します。例えばキーを複数組み合わせて押さないといけない場合です。
[^0]
<!--more-->


### キーボード以外の選択肢としてのフットスイッチ

片手にこだわらずに使えるものをという観点で探したところ、プログラム可能なフットスイッチというものがあるのをみつけました。

  * [メカニカルスイッチ搭載 USBフットペダルスイッチ 3ペダル グレー RI-FP3MG](http://route-r.co.jp/?p=3121)

![RI-FP3MG]({{ "/images/blog/20190104_0.jpg" | relative_url }} "RI-FP3MG")

ベースになっているのはPCsensorのモデルのようです。

  * [USB Foot Control Keyboard,Three Switch Pedal](http://www.pcsensor.com/usb-foot-control-keyboard-mouse-button-three-switch-pedal-fs3_p-p180.html)

複数のモデルがあり、別のモデルであるRI-FP3BKはLinuxでの[実績](https://qh73xebitbucketorg.readthedocs.io/ja/latest/0.OS/linux/DIST/fedora/footswich/#rgerganov-footswitch)がすでにあるようでした。

RI-FP3MGそのものの言及はありませんが、フットスイッチそのものに設定が保持されるタイプなので、設定自体はLinuxからできなくてもいいかということで試してみました。

### フットスイッチを認識させる

フットスイッチをPCに接続して`lsusb`の結果を確認すると、次のIDで認識されていました。

```
% lsusb
（省略）
Bus 002 Device 002: ID 0c45:7404 Microdia
```


### フットスイッチをLinuxから設定するには

標準ではWindowsの設定アプリが付属しているのでそちらを使えばいいのですが、認識されたIDをもとに検索すると、次のリポジトリを見つけました。

  * https://github.com/rgerganov/footswitch

対応しているIDのリストに`0c45:7404`があったので、Linuxでも使えそうです。

`footswitch`はREADME.mdに記載の次の手順でビルドできました。

```
% sudo apt install libhidapi-dev
% git clone https://github.com/rgerganov/footswitch.git
% cd footswitch
% make
```


ビルドが完了すると、`footswitch`と`scythe`の2つのバイナリができます。`0c45:7404`に対応しているのは`footswitch`なのでそちらを使います。

### フットスイッチの設定方法

`-r`オプションを指定するとフットスイッチの現在の設定値を確認できます。

```
% sudo ./footwswitch -r
[switch 1]: a
[switch 2]: b
[switch 3]: c
```


出荷直後は、`a`、`b`、`c`がそれぞれのスイッチに割りあてられていました。

スイッチ自体は3つありますが、次の2つの用途に使ってみることにしました。

  * 日本語入力のON/OFFをShift+Spaceにしているので、スイッチ1に割りあてる

  * tmuxのプレフィクスをCtrl+Tにしているので、スイッチ3に割りあてる

上記の2つの設定を行うには、次のコマンドを実行します。

```
% sudo ./footswitch -1 -m shift -k space -3 -m ctrl -k t
```


`-1 -m shift -k space`の部分が「スイッチ1が押されたら、モディファイアキーとしてShiftキーを、通常のキーとしてスペースを押した状態にする」ための指定です。

`-3 -m ctrl -k t`の部分が「スイッチ3が押されたら、モディファイアキーとしてCtrlキーを、通常のキーとしてtを押した状態にする」ための指定です。

なお、上記のようにスイッチ2の設定を一緒に指定しない場合には、スイッチ2の設定がクリアされるので注意が必要です。（特定のスイッチのみ指定して設定を上書きみたいなことはできない）

### フットスイッチの設定がうまくいかない場合

footswitchはキー入力だけでなく、マウスカーソルの移動もエミュレートできるようです。（そちらは試していない）
README.mdを参照する限り、かなり細かな設定ができそうに思いますが、試した限りでは次の場合には期待通りには動作しません。

  * CtrlとCapsLockキーを入れ換えている場合

CtrlとCapsLockキーを入れ換えている場合には、`-m ctrl -k t`が機能せず、tだけ打鍵したかのように振る舞います。

  * [How to use footswitch with ctrl:swapcaps?](https://github.com/rgerganov/footswitch/issues/47)

この問題は解決していませんが、キー割り当てをCtrlにこだわらなければ回避策があります。単純なやりかたですが、Ctrlの代わりに別のキーとの組み合わせにするなどです。

幸い、Ctrl+Tを使いたかったのはtmuxのプレフィクスとしてだったので、次のようにしてAlt+Tをfootswitchで指定することにしました。（ターミナルで作業する範囲においては、他のキー割り当てと衝突しにくい）

```
% sudo ./footswitch -1 -m shift -k space -3 -m alt -k t
```


### まとめ

今回は、キーボード入力の補助手段としてのフットスイッチを紹介しました。
もし、`xhk`などを駆使していても追いつかない状況になったら、選択肢の一つとして試してみるのもよいかもしれません。

[^0]: 組み合わせても問題ないキーの割り当てを工夫するのも一つの手ですが、どう割りあてるかが悩ましいです。既定の割り当てと衝突したりするためです。
