---
tags:
- javascript
title: Webアプリや拡張機能（アドオン）で、Web Crypto APIを使ってローカルに保存されるデータを暗号化する
author: piro_or
---
<ins>※注記：本文末尾の「公開鍵暗号ではなく共通鍵暗号を使う理由」の説明について、2019年1月30日午前0時から21時までの間の初出時に内容の誤りがありました。また、2019年1月30日午前0時から2月5日20時頃までの間において、本文中での `AES-CTR` による暗号化処理が、 `nonce` を適切に指定していないために脆弱な状態となっていました。お詫びして訂正致します。初出時の内容のみをご覧になっていた方は、お手数ですが訂正後の説明を改めてご参照下さい。</ins>
<!--more-->


クリアコードで主にMozilla製品のサポート業務に従事している、結城です。

FirefoxやThunderbirdがSSL/TLSで通信する際は、通信内容は自動的に暗号化されます。その一方で、Cookieやローカルストレージ、IndexedDBなどに保存されるデータは、平文のままでユーザーの環境に保存される事が多く、必要な場合はアプリ側で内容を暗号化しなくてはなりません。こういった場面でJavaScriptから使える暗号化・復号のための汎用APIが、[Web Crypto API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API)です。

ただ、Web Crypto APIでデータを暗号化するにはある程度の知識が必要になります。「`encrypt()` という関数に文字列と鍵を渡したらいい感じに暗号化されて、`decrypt()` という関数に暗号と鍵を渡したらいい感じに復号される」というような単純な話ではないため、全く前提知識がないと、そう気軽には使えません。

この記事では、[Web Crypto API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API)を用いたローカルデータの暗号化の基本的な手順をご紹介します。

### Web Crypto APIでの暗号化の基本

Web Crypto APIでは[様々な暗号方式を使えます](https://github.com/diafygi/webcrypto-examples)が、それぞれ適切な用途が決まっています。一般的な「データの暗号化と復号」という場面では、以下のいずれかの暗号方式を使うことになります。

  * 共通鍵暗号

    * `AES-CTR`

    * `AES-CBC`

    * `AES-GCM`

  * 公開鍵暗号

    * `RSA-OAEP`

AES や RSA というのが具体的な暗号アルゴリズムの名前で、CTR、CBC、GCM というのは暗号モードの名前です。諸々の理由から、*アプリ内部でローカルに保存するデータを暗号化するという場面では、<del>`AES-CTR`</del> <ins>`AES-GCM`</ins> が最適*と言えるでしょう（詳細は後で述べます）。

JavaScriptで書かれた実装において、暗号化したいデータは普通は文字列や配列、オブジェクトといった形式を取っていることでしょう。しかしながら、Web Crypto APIでこれらのプリミティブな値を直接暗号化する事はできません。Web Crypto APIでは基本的に、データを `ArrayBuffer` やTyped Arrayと呼ばれる、よりバイナリ形式に近いデータで取り扱います。そのため、これらのデータ型の概念をきちんと理解しておく必要があります。[Blob, ArrayBuffer, Uint8Array, DataURI の変換](http://var.blog.jp/archives/62330155.html)という記事では、これらのデータ型の関係を図を交えて分かりやすく説明してくれていますので、Web Crypto APIを触る前にぜひ読んでおきましょう。

また、Web Crypto APIの多くの機能は非同期処理になっており、処理結果がPromiseとして得られる場合が多いです。Promiseとは、簡単に言えば以下のような物です。

  * Promiseは値の入れ物と言える。

    * 中の値を取り出すには、`promise.then(function(value) { ... })` のように、`then` メソッドにコールバック関数を渡してその中で受け取る必要がある。

    * 中の値をすぐに取り出せるとは限らない。通信先のサーバーから結果が返ってきて初めて `then` メソッドのコールバック関数が呼ばれる、というような事もある。

  * `async function() { ... }` という風に `async` キーワードを付けて定義された関数は、非同期の関数になる。

    * 非同期の関数を呼び出すと、関数の中で `return` した値は、呼び出した側からは常にPromiseとして受け取る事になる（関数の戻り値が常にPromiseになる）。

    * 非同期の関数の中では、`let value = await promise;` のように書くと、その場でPromiseの中の値を取り出せる（`then` メソッドを使わなくてもよくなる）。

以上の事を踏まえ、実際にデータを暗号化してみることにしましょう。

なお、Web Crypto APIの入出力はPromiseになっている事が多いので、以降のサンプルはすべて以下のような非同期関数の中で実行するものとします。

```javascript
(async function() {

  // ここで実行

})();
```


#### 鍵の生成

Web Crypto APIでの暗号化・復号では、*Web Crypto APIで生成された `CryptoKey` クラスのインスタンスを鍵に使います*。ここでは以下の2パターンの鍵の作り方を紹介します。

  * ユーザーが入力したパスワードを鍵に変換する方法

  * 完全に自動で鍵を生成する方法

##### パスワードを鍵に変換する

パスワードで保護されたOffice文書やPDFを取り扱う時のように、*暗号化や復号の時にユーザーによるパスワードの入力を求める*形にしたい場合には、*入力されたパスワードを `CryptoKey` クラスのインスタンスに変換する*操作を行います。Webブラウザで開発者用のコンソールを開いて、以下のスクリプトを実行してみると、実際に得られた `AES-GCM` 用の鍵がコンソールに出力されます。

```javascript
// パスワードとして使う文字列。（ユーザーの入力を受け付ける）
let password = prompt('パスワードを入力して下さい'); // 例：'開けゴマ'

// 文字列をTyped Arrayに変換する。
let passwordUint8Array = (new TextEncoder()).encode(password);

// パスワードのハッシュ値を計算する。
let digest = await crypto.subtle.digest(
  // ハッシュ値の計算に用いるアルゴリズム。
  { name: 'SHA-256' },
  passwordUint8Array
);

// 生パスワードからのハッシュ値から、salt付きでハッシュ化するための素材を得る
let keyMaterial = await crypto.subtle.importKey(
  'raw',
  digest,
  { name: 'PBKDF2' },
  // 鍵のエクスポートを許可するかどうかの指定。falseでエクスポートを禁止する。
  false,
  // 鍵の用途。ここでは、「鍵の変換に使う」と指定している。
  ['deriveKey']
);

// 乱数でsaltを作成する。
let salt = crypto.getRandomValues(new Uint8Array(16));

// 素材にsaltを付与して、最終的なWeb Crypto API用の鍵に変換する。
let secretKey = await  crypto.subtle.deriveKey(
  {
    name: 'PBKDF2',
    salt,
    iterations: 100000, // ストレッチングの回数。
    hash: 'SHA-256'
  },
  keyMaterial,
  // アルゴリズム。
  { name: 'AES-GCM', length: 256 },
  // 鍵のエクスポートを禁止する。
  false,
  // 鍵の用途は、「データの暗号化と復号に使う」と指定。
  ['encrypt', 'decrypt']
);

console.log(secretKey);
// => CryptoKey { type: "secret", extractable: false, algorithm: {…}, usages: (2) […] }
```


<ins>
パスワードから鍵への変換に際して、より安全にするためにsaltとストレッチングを使っている事に注意して下さい[^0]。
</ins>


この使い方においては、`CryptoKey` クラスのインスタンスとして得られた鍵はメモリ上に一時的に保持しておくだけでよく、ログアウト時やアプリの終了時にはそのまま破棄する事になります。次回訪問時・ログイン時・アプリ起動時には、再びパスワードの入力を求め、その都度この方法で鍵へと変換します。

<ins>
なお、saltは次回以降のパスワード→鍵の変換時にも必要になります。保存と復元は以下の要領で行えます。


```javascript
// saltの保存。
localStorage.setItem('passwordSalt',
  JSON.stringify(Array.from(salt)));

// saltの復元。
let salt = localStorage.getItem('passwordSalt');
salt = Uint8Array.from(JSON.parse(salt));
```


</ins>


<del>


ところで、上記の例では「平文のパスワード→SHA-256でハッシュ化→AES-GCMの鍵としてインポート」という経路で変換を行っていますが、「パスワードのハッシュ化にsaltは必要じゃないの？」と疑問に思う方もいらっしゃるかと思います。saltが必要かどうかは、*ハッシュ化したパスワードを外部や第三者に読み取られる危険性を考慮しないといけないかどうか*に依存します。ハッシュ化したパスワードや鍵を一旦ストレージに保存するのであればハッシュ化は必要ですが、この例のようにメモリ上でごく短時間保持されるだけの場合は必要ない、というのが筆者の考えです。ハッシュ化時にsaltを使うという事自体は以下のようにして実現できますが、ストレッチングにそれなりの時間がかかりますので、本当に必要という事が言える場合にのみ使う事をお薦めします。

</del>


##### 鍵の自動生成

「何らかの方法で鍵を安全な場所（暗号化したデータとは別の保存先）に保管できる」という前提がある場合には、`crypto.subtle.generateKey()` を使って鍵を生成し、ユーザーにパスワードの入力を求めずに暗黙的に暗号化・復号を行うという事もできます。この場合、鍵は以下のように生成します。

```javascript
let secretKey = await crypto.subtle.generateKey(
  // アルゴリズムと鍵長。ここでは最長の256bitを指定している。
  { name: 'AES-GCM', length: 256 },
  // 鍵のエクスポートを許可するかどうか。trueでエクスポートを許可する。
  true,
  // 鍵の用途。
  ['encrypt', 'decrypt']
);
console.log(secretKey);
// => CryptoKey { type: 'secret', extractable: true, algorithm: Object, usages: Array[2] }
```


生成された鍵は `CryptoKey` クラスのインスタンスとなっており、そのままでは永続化（保存）できませんが、以下のようにすると、JSON Web Key形式（通常のオブジェクト）として鍵をエクスポートできます。

```javascript
// JSON Web Key（JWK）形式でのエクスポート。
let exportedSecretKey = await crypto.subtle.exportKey('jwk', secretKey);
console.log(exportedSecretKey);
// => Object { alg: "A256GCM", ext: true, k: "DAvha1Scb8jTqk1KUTQlMRdffegdam0AylWRbQTOOfc", key_ops: (2) […], kty: "oct" }
```


このようにして得られたJWK形式の鍵は、以下の手順で再び `CryptoKey` のインスタンスに戻す事ができます。

```javascript
// JWK形式からのインポート。
let importedSecretKey = await crypto.subtle.importKey(
  'jwk',
  exportedSecretKey,
  // 鍵を生成した際の暗号アルゴリズム。
  { name: 'AES-GCM' },
  // 再エクスポートを許可するかどうかの指定。falseでエクスポートを禁止する。
  false,
  // 鍵の用途。
  ['encrypt', 'decrypt']
);
console.log(importedSecretKey);
// => CryptoKey { type: 'secret', extractable: true, algorithm: Object, usages: Array[2] }
```


実際には、初回訪問時・初回起動時などのタイミングで生成した鍵をエクスポートして安全な場所に補完しておき、次回訪問時・次回起動時などのタイミングでそれを読み取ってインポートし、メモリ上に鍵を復元する、といった要領で使う事になります。

#### データの暗号化

鍵の準備ができたらいよいよ本題の暗号化ですが、その前に、*「暗号化する対象のデータ」を「暗号化できるデータ形式」に変換する*という操作が必要です。

Web Crypto APIの暗号化処理は、データの入出力を `ArrayBuffer` やTyped Arrayの形式のみで行う仕様になっているため、どんなデータを暗号化するにせよ、何らかの方法でデータをこれらの形式に変換しなくてはなりません。文字列は 前出の例の中で行っていたように、`TextEncoder` を使って以下のようにTyped Arrayに変換できます。

```javascript
// データをTyped Arrayに変換。
let inputData = (new TextEncoder()).encode('暗号化したい文字列');
console.log(inputData);
// => Uint8Array(27) [ 230, 154, 151, 229, 143, 183, 229, 140, 150, 227, … ]
```


オブジェクト形式のデータであれば、この直前に「`JSON.stringify()` で文字列に変換する」という操作を加えればよいでしょう。

入力データが用意できたら、<ins>次は*初期ベクトル*の準備です。</ins>

<ins>初期ベクトルとは、`AES-CBC` および `AES-GCM` においてデータを同じ鍵で暗号化する際に、暗号文から内容の推測を困難にするために付与する値です[^1]。パスワードのハッシュ化に用いるsaltのようなもの、と言えば分かりやすいでしょうか。本来は一意な値である必要がありますが、ここでは話を単純にするために、とりあえず乱数を使う事にします。実際に使う時は、[きちんとした初期ベクトルを生成する手順の解説]({% post_url 2019-02-08-index %})も併せて参照して下さい。</ins>

```javascript
// 初期ベクトルとして、8bit * 16桁 = 128bit分の領域を確保し、乱数で埋める。
let iv = crypto.getRandomValues(new Uint8Array(16));
```


入力データ<ins>と初期ベクトル</ins>が用意できたら、ようやく暗号化です。これは以下の要領で行います。

```javascript
let encryptedArrayBuffer = await crypto.subtle.encrypt(
  // 暗号アルゴリズムの指定とパラメータ。
  { name: 'AES-GCM',
    iv },
  // 事前に用意しておいた鍵。
  secretKey,
  // ArrayBufferまたはTyped Arrayに変換した入力データ。
  inputData
);
console.log(encryptedArrayBuffer);
// => ArrayBuffer { byteLength: 27 }
```


暗号化後のデータは、非同期に `ArrayBuffer` 形式で返されます。ここでは27バイトの長さがあるという事だけが分かっているため、このような表示になっています。

データを暗号化できたら、今度はこれをIndexedDBや `localStorage` などに格納可能な形式に変換します。例えば文字列への変換であれば、以下の手順で行えます。

```javascript
let encryptedBytes = Array.from(new Uint8Array(encryptedArrayBuffer), char => String.fromCharCode(char)).join('');
console.log(encryptedBytes);
// => �����`Ù�¥ë�`û-Þm#þ'�¾��[�·�
```


ここでは `ArrayBuffer` 形式で27バイトの長さのデータとして得られた物を、8bitずつに切り分けて `Unit8Array` に変換し、さらにその1バイトずつを文字コードと見なして文字に変換しています。

このような文字列はBinary Stringと呼ばれ、コンソールなどに出力しても文字化けした結果になる事がほとんどのため、データを持ち回る過程で破損してしまわないよう、取り扱いには注意が必要です。安全のためには、以下のようにしてBase64エンコード済みの文字列に変換して、文字化けなどが起こりにくい安全な状態で取り回すのがお薦めです。

```javascript
let encryptedBase64String = btoa(encryptedBytes);
console.log(encryptedBase64String);
// => YPgdHZgguUeHpt9FcYy2IaZTfbTNswbfn93e
```


<ins>`AES-GCM`で暗号化したデータ[^2]の復号には、暗号化時に使用した初期ベクトルが必要となります[^3]。以下のようにして、暗号化したデータとセットで保存しておきます。</ins>

```javascript
localStorage.setItem('encryptedData',
  JSON.stringify({
    data: encryptedBase64String,
    iv:   Array.from(iv)
  }));
```


#### データの復号

次は、暗号化済みのデータから元のデータを取り出してみましょう。

Web Crypto APIの復号処理も、暗号化処理と同様、データの入出力を `ArrayBuffer` やTyped Arrayの形式のみで行う仕様になっています。先の例と逆の操作を行い、まずは暗号化されたデータを `ArrayBuffer` またはTyped Array形式に変換します。

```javascript
let encryptedData = JSON.parse(localStorage.getItem('encryptedData'));
let encryptedBase64String = encryptedData.data;
// 通常のArrayとして保存しておいた初期ベクトルをUint8Arrayに戻す
let iv = Uint8Array.from(encryptedData.iv);

// Base64エンコードされた文字列→Binary String
let encryptedBytes = atob(encryptedBase64String);

// Binary String→Typed Array
let encryptedData = Uint8Array.from(encryptedBytes.split(''), char => char.charCodeAt(0));
```


データの準備ができたら、いよいよ復号です。これは以下の手順で行います。

```javascript
let decryptedArrayBuffer = await crypto.subtle.decrypt(
  // 暗号アルゴリズムの指定とパラメータ。暗号化時と同じ内容を指定する。
  { name: 'AES-GCM',
    iv },
  // 暗号化の際に使用した物と同じ鍵。
  secretKey,
  // ArrayBufferまたはTyped Arrayに変換した暗号化済みデータ。
  encryptedData
);
console.log(decryptedArrayBuffer);
// => ArrayBuffer { byteLength: 27 }
```


復号の時には、暗号化時と同じパラメータ、同じ鍵が必要である点に注意して下さい。*何らかのトラブルで鍵<ins>や初期ベクトル</ins>を喪失してしまうと、元のデータは永遠に取り出せなくなってしまいます*。

復号されたデータは `ArrayBuffer` 形式になっています。これを通常の文字列へ変換するには以下のように操作します。

```javascript
let decryptedString = (new TextDecoder()).decode(new Uint8Array(decryptedArrayBuffer));
console.log(decryptedString);
// => '暗号化したい文字列'
```


無事に、暗号化する前の平文であった「暗号化したい文字列」という文字列を取り出す事ができました。

### 疑問

#### なぜ暗号アルゴリズムに <del>`AES-CTR`</del> <ins>`AES-GCM`</ins> を使うのか？

冒頭で、Web Crypto APIでデータの暗号化と復号に使えるアルゴリズムは4つあると述べましたが、本記事ではその中で何故 <del>`AES-CTR`</del> <ins>`AES-GCM`</ins> を選択しているのかについて、疑問に思う人もいるでしょう。

暗号アルゴリズムには、大別して「共通鍵暗号」と「公開鍵暗号」の2種類があります。共通鍵暗号は暗号化と復号に同じ鍵を使う方式、公開鍵暗号は暗号化と復号で異なる鍵を使う方式です。公開鍵暗号は<del>「暗号化したデータと、それを復号する鍵の両方を、信頼できない通信経路で送り、受信側で復号する」</del><ins>「暗号化と復号を別々の所で（別々の人が）行う」</ins>という場面に適しています。逆に言うと、「アプリ内部でローカルに保存するデータを暗号化する」という場面のように、*<del>データや鍵が信頼できない通信経路を通る事がないのであれば</del>暗号化も復号も同じ所で行うのであれば、公開鍵暗号（`RSA-OAEP`）ではなく共通鍵暗号（`AES-*` 系）で充分に安全*だと言えます。

<del>また、`AES-*` 系の中で `AES-CTR` を選択する理由は、他の2つが「初期ベクトル」という、鍵とは別の使い捨ての情報を組み合わせて使う（「鍵」と「初期ベクトル」の両方が揃って初めて暗号を復号できる）方式だからです。</del>

<del>`AES-CBC` と `AES-GCM` は、1回1回の暗号化・復号ごとに初期ベクトルを変えることで安全性を高める前提の方式です。ネットワークを通じてサーバーとクライアントの間でデータをやりとりするという場面では、暗号化して送られたデータはすぐに受け手の側で復号されます。このようなケースでは、*同じ暗号データを何度も復号するという事は起こり得ないため、初期ベクトルは使い捨てにできます*。</del>

<del>一方、アプリケーションのローカルデータを暗号化するという場面では、*暗号化して保存されたデータを何度も復号する必要があります*。データの復号には暗号化した時と同じ初期ベクトルが必要になるため、必然的に、初期ベクトルは使い捨てにできず、何度も使い回す事になります。[本当は怖いAES-GCMの話 - ぼちぼち日記](https://jovi0608.hatenablog.com/entry/20160524/1464054882)などの解説記事で詳しく述べられていますが、*初期ベクトルを使い回した場合これらの暗号アルゴリズムは非常に脆弱な物となり、「情報の漏洩を防ぐ」という目的を達成できなくなってしまいます*。</del>

<del>以上の理由から、このような場面では初期ベクトルを用いない方式である `AES-CTR` が最適だと言える訳です。</del>

<ins>`AES-*` 系の中で `AES-GCM` を選択した理由は、他の2つには以下のようなデメリットがあるからです。</del>

  * <ins>`AES-CTR`: カウンタの取り扱いが初期ベクトルに比べて面倒。</ins>

  * <ins>`AES-CBC`: 暗号化の並列処理ができない＝暗号化処理に時間がかかる可能性がある。</ins>

<ins>Webアプリなどでパスワード認証を実装する際には、パスワードをそのまま保存するのはなく、非可逆的に変換した結果であるハッシュ値を保存する事が一般に推奨されています。この時には、ハッシュ値から元のパスワードを推測しにくくするために、パスワードに余計なデータを加えてからハッシュ化するのが一般的です。この追加するデータを「塩をまぶす」ことに例えてsaltと呼びます。</ins>

<ins>これと同様にAESにおいても、データには必ずsaltのような*一意な*データを付与してから暗号化するようになっています。これは仕様上秘密である必要はなく、一意でさえあればいいという事になっています。`AES-CBC` および `AES-GCM` では、そのようなsalt相当のデータを「初期ベクトル」と呼んでいます。</ins>

<ins>一方、`AES-CTR` ではsaltにあたるデータを「カウンタ」の一部として与えます。カウンタは最大で16byte＝128bitの長さを指定できますが、そのうち一部をnonce（一意な数字）、残りを0からカウントアップする文字通りのカウンタとして使う事になっています。例えば128bit中の64bitをnonce、残り64bitをカウントアップ部にする、といった要領です。……という具合に長々説明している事自体に顕れていますが、「一意な値を1つ用意するだけでいい」という初期ベクトルに比べて、カウンタは取り扱いが若干ややこしいという事自体が `AES-CTR` のデメリットと言えます。</ins>

<ins>では、`AES-CTR` 以外なら `AES-CBC` でも `AES-GCM` でもどちらでも良いのでは？ という話になりそうですが、`AES-CBC` には*処理を並列化できないという原理上の制限があります*。`AES-CTR` や `AES-GCM` にはそのような制限が無く、上手く実装されたソフトウェアであれば、マルチスレッドを活用した並列処理で高速ができます[^4]。</ins>

<ins>以上の理由から、この3つの選択肢の中では総合的に見て `AES-GCM` が最もメリットが大きいと筆者は考えています。</ins>

<ins>なお、本文中の例のように乱数を使って初期ベクトルを生成するのは、本来は望ましい事ではありません。実際の実装にそのまま組み込む前には、[よりセキュアなきちんとした初期ベクトルを生成する手順の解説]({% post_url 2019-02-08-index %})も併せてご参照下さい。</ins>

#### 自動生成した鍵はどこに保存すればよいのか？

鍵を自動生成して暗黙的に暗号化・復号を行うという動作をさせたい場合、*鍵は暗号化されたデータとは別の安全な場所に置く必要があります*。生の鍵を暗号化されたデータと同じ場所に保存してしまっては、暗号化の意味がありません

鍵の置き場所としては、例えば以下のような例が考えられます。

  * ファイルとして保存した物をUSBメモリに書き込み、そのUSBメモリを物理的な鍵として使う。

  * QRコードに変換して表示した物をスマートフォンで撮影し、そのスマートフォンを物理的な鍵として使う。

  * 鍵をICカードに保存し、その都度鍵をICカードリーダー経由で読み取る事として、ICカードそのものを物理的な鍵として使う。

  * パスワードマネージャの中にパスワードの一種として保管し、パスワードマネージャのマスターパスワードで保護する。

こういった方法で鍵を保存しておくやり方と、都度パスワードの入力を求めるやり方のどちらが安全かについては、専門家の間でも意見が分かれているようです。鍵をデバイス上に保存しておくやり方には、当然ながら、デバイスそのものが盗難された場合にデータを保護できなくなるというリスクがあります。一方で、パスワード入力を頻繁に求める方法は、パスワードの入力の機会が増えるため、キーロガーやショルダーハッキングといった攻撃に遭うリスクが増大します。パスワードマネージャとマスターパスワードを使う方法は、両者の弱点を補うものと言えます。

### まとめ

以上、Web Crypto APIを使ってJavaScriptでローカルデータを暗号化する手順をご紹介しました。個人情報のようにクリティカルな情報を取り扱うWebアプリや拡張機能を開発する場合に、参考にしてみて下さい。

[^0]: saltというとハッシュ化した後のパスワードの流出に備えての物のように思えますが、ここでは暗号化されたデータが流出した時の保護を目的としています。ハッシュ化した後のパスワードを保存していない場合、ハッシュ化されたパスワードの流出については考えなくても良い（JavaScriptのコードから機械語への変換のされ方を制御したり、メモリ上の情報を再配置したり、といった低レベルの対策を取れないJavaScriptという言語の特性上、サイドチャネル攻撃で平文のパスワードやハッシュ化したパスワードを読み取られる事を警戒する事はアプリケーション実装者の側では不可能で、JavaScriptの実行エンジンの実装側が責任を持つことになります。また、平文のパスワードが流出しないようXSSの対策は充分に取られていることを前提とします）のですが、saltを使用していないと、攻撃者は生パスワードからのハッシュ値の計算を省略できるため、暗号化済みデータに対する攻撃がより成功しやすくなります。saltを付与すると、攻撃者はそのような最適化を行えなくなり、結果的に、暗号化されたデータのみが流出した時の攻撃に対する耐性が高まります。

[^1]: `AES-CBC`、`AES-GCM`だけでなく、`AES-CTR` においても暗号化時のパラメータの1つである「カウンタ」の一部としてこれに相当する情報を指定する必要があります。

[^2]: `AES-CBC` も同様。

[^3]: よって、実質的には初期ベクトルも「暗号を解く鍵」の一部と言えるかもしれません。

[^4]: 実際にブラウザのWeb Crypto APIの実装がそのように最適化されているかどうかは、また別の話になります。
