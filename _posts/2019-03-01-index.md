---
tags:
- mozilla
- feedback
title: Firefox 67 以降でPolicy Engineによるポリシー設定でPOSTメソッドの検索エンジンを追加できるようになります
---
[Firefoxのポリシー設定]({% post_url 2018-05-12-best-practice-about-firefox-esr60-policy-engine %})では、以下の要領でロケーションバーやWeb検索バー用の検索エンジンを登録することができます。
<!--more-->


```json
{
  "policies": {
    "SearchEngines": {
      "Add": [
        {
          "Name":               "検索エンジンの表示名",
          "IconURL":            "https://example.com/favicon.ico",
          "Alias":              "ex",
          "Description":        "検索エンジンの説明文",
          "Method":             "GET",
          "URLTemplate":        "https://example.com/search?q={searchTerms}",
          "SuggestURLTemplate": "https://example.com/suggest?q={searchTerms}"
        }
      ]
    }
  }
}
```


Firefox 66以前のバージョンではこの時、HTTPのメソッドとして`GET`のみ使用可能で、`POST`メソッドを要求する検索エンジンは登録できないという制限事項がありました。OpenSearchの検索プラグインでは`POST`の検索エンジンも登録できたので、これはPolicy Engineだけの制限ということになります。

[REST](https://ja.wikipedia.org/wiki/Representational_State_Transfer)の原則からいうと「何回検索しても同じ結果が返る事が期待される検索結果一覧ページを表示するためのリクエストは`GET`が当然」という事になるのですが、実際にはFirefoxのロケーションバーやWeb検索バーは「検索」に限らず、*入力された語句を含む任意のHTTPリクエストを送信する汎用のフォームとして使えます*。よって、適切な内容の「検索エンジン」を登録しておきさえすれば、FirefoxのUI上から直接「Issueの登録」や「チャットへの発言」を行うといった応用すらも可能となります。しかし、そういった「新規投稿」にあたるリクエストは`POST`メソッドを要求する場合が多いため、それらは残念ながらPolicy Engine経由では登録できませんでした。

この件について [1463680 - Policy: SearchEngines.Add cannot add effective search engine with POST method](https://bugzilla.mozilla.org/show_bug.cgi?id=1463680) として報告していたのですが、最近になって「[バックエンドとなる検索エンジン管理の仕組みの改良](https://bugzilla.mozilla.org/show_bug.cgi?id=1477076)によって、仕組み的には`POST`メソッドも受け取るようになったので、パッチを書いてみては？」と促されました。そこでさっそく実装してみた所、変更は無事マージされ、Firefox 67以降では以下の書式で`POST`メソッドとそのパラメータを指定できるようになりました。

```json
{
  "policies": {
    "SearchEngines": {
      "Add": [
        {
          "Name":        "検索エンジンの表示名",
          "IconURL":     "https://example.com/favicon.ico",
          "Alias":       "ex",
          "Description": "検索エンジンの説明文",
          "Method":      "POST",
          "URLTemplate": "https://example.com/search",
          "PostData":    "q={searchTerms}"
        }
      ]
    }
  }
}
```


`"Method"`の指定を`"POST"`にする事と、`"URLTemplate"`ではなく`"PostData"`の方に`{searchTerms}`というパラメータを指定する事がポイントです。

今回の変更自体は[別のBugでの改良](https://bugzilla.mozilla.org/show_bug.cgi?id=1477076)に依存しているため、ESR60に今回の変更がupliftされるためには、依存する変更も併せてupliftされる必要があります。ESR版自体のメジャーアップデートも視野に入りつつあるこの時期ですので、そこまでの手間をかけてこの変更がESR60に反映されるかどうかについては、現状では何とも言えません。とはいえ、次のESRのメジャーアップデート以降で使用可能になる事は確実です。もしこの機能をお使いになりたいというESR版ユーザーの方がいらっしゃいましたら、期待してお待ちいただければと思います。
