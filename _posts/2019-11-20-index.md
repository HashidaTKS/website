---
tags:
- fluentd
title: "「Fluentd」のサポートサービスを開始しました。"
---
クリアコードでは2015年からFluentdの開発コミュニティに参加し、Fluentd本体とプラグインの開発、サポート、各種ドキュメントの整備を行っています。
2016年以降、既存の取引先様に対してFluentdのサポートを提供していましが、この度満を持してサポートサービスの開始を宣言しました。
サービスの概要は[こちら](/services/fluentd.html)です。
先日[プレスリリース](/press-releases/20191029-fluentd-support.html)も出しましたので、併せてご参照ください
<!--more-->


Fluentdに関するお問い合わせは[こちら](/contact/)まで。
