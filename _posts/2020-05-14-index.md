---
tags:
- notable-code
title: ノータブルコード6 - ポインターのサイズで32bit環境を見分ける
---
[Groonga](https://groonga.org/ja/)という全文検索エンジンの開発に参加している堀本です。
今回は、開発中に[Groonga](https://groonga.org/ja/)のソースコードから「おぉー」と思ったコードを見つけたので紹介します。
<!--more-->


[Groonga](https://groonga.org/ja/)は32bit用パッケージと64bit用パッケージを配布していますが、32bit環境のときだけコンパイルオプション等を設定したいケースがあります。

例えば、32bitのWindows向けパッケージはCMakeを使用してビルドしていますが、CMakeには32bitのWindowsかどうかを判定できる定義済みの変数はありません。
`WIN32`という定義済みの変数がありますが、これはコンパイルターゲットが(64bitを含む)Windowsの場合に`True`になるので、32bitかどうかの判定には使えません。

そこで[Groonga](https://groonga.org/ja/)では、以下のようにポインターのサイズを使って32bitかどうかを判定しています。
(`CMAKE_SIZEOF_VOID_P`は`void`ポインタのサイズを計算してくれるCMakeの定義済みの変数です。)

```
  if(CMAKE_SIZEOF_VOID_P EQUAL 4)
    # 32bit
    list(APPEND MRUBY_DEFINITIONS "MRB_METHOD_T_STRUCT")
  endif()
```


データ型のサイズは、データ型モデルという定義にしたがって決まります。データ型モデルとはデータ型の大きさを定義したもので、OSによって異なりますが、概ね以下のどれかになります。

<div style="text-align: center;">
32bit用データ型モデル
</div>


|データモデル名|short|int|long|long long|pointer|
|---|---|---|---|---|---|
|ILP32|16|32|32|64|32|
|LP32|16|16|32|64|32|

<br>
<div style="text-align: center;">
64bit用データ型モデル
</div>


|データモデル名|short|int|long|long long|pointer|
|---|---|---|---|---|---|
|LLP64|16|32|32|64|64|
|LP64|16|32|64|64|64|
|IP64|16|64|32|64|64|
|ILP64|16|64|64|64|64|
|SILP64|64|64|64|64|64|

上記を見てわかるとおり、どのモデルであっても32bitの場合、ポインターのサイズは、32bit(4byte)になっています。
また、同様に64bitの場合も、ポインターのサイズはどのモデルも64bit(8byte)になっています。

現状出回っているほとんどの環境では、上記のように32bitと64bitでポインターのサイズは決まっているので、これを利用して32bit環境かどうかを判定できるのです。

様々なOSの32bit版のソフトウェアをメンテナンスしている方は、上記のような判定方法も利用してみてはいかがでしょうか？
