---
tags: []
title: Serverspec(Specinfra)でWindows向けに不足している機能を追加するには
---
### はじめに

クリアコードでは、[Fluentd](https://www.fluentd.org/)の開発に参加しています。Fluentdにはtd-agentと呼ばれるディストリビューションがあり、各種プラグインをまとめてパッケージやインストーラーが提供されています。
<!--more-->


今回は、td-agentのテストに使っている[Serverspec(Specinfra)](https://serverspec.org/)で、Windows向けのテストに不足している機能を一部追加する機会があったのでその紹介をします。

### 機能不足に気づいたきっかけ

td-agentでは、ビルドしたパッケージの動作を確認するために、Serverspec(Specinfra)によるテストを実行するようにしています。
Windowsのインストーラーにバンドルしているgemが期待通りインストールされているかをテストしようとしたところ、以下のエラーが発生することに気づきました。[^0]

```
check_is_installed_by_gem is not implemented in Specinfra::Command::Windows::Base::Package
```


これは、以下のようなテストコードで発生していました。特定のgemの指定したバージョンのものがインストールされていることを確認するテストです。

```ruby
describe package("#{spec.name}") do
  it { should be_installed.by("gem").with_version(spec.version) }
end
```


エラーメッセージから明らかなように、特定のgemがインストールされているかどうかをチェックする機能がまだWindowsではサポートされていないことがわかりました。

### Specinfraにcheck_is_installed_by_gemを追加する

Windowsでも同じコードでバンドルしているgemのチェックができると嬉しいので、不足している機能を追加することにしました。

必要なのは、前述のエラーメッセージが示すように、`Specinfra::Command::Windows::Base::Package` に `check_is_installed_by_gem` を実装してあげることです。

```ruby
def check_is_installed_by_gem(name, version=nil, gem_binary="gem")
  version_selection = version.nil? ? "" : "-gemVersion '#{version}'"
  Backend::PowerShell::Command.new do
    using 'find_installed_gem.ps1'
    exec "(FindInstalledGem -gemName '#{name}' #{version_selection}) -eq $true"
  end
end
```


gemの名前とバージョンを受け取り、比較して一致するか否かを返す `FindInstalledGem` を`find_installed_gem.ps1`に実装し、`lib/specinfra/backend/powershell/support/find_installed_gem.ps1` として配置することで実現しました。

実際のPull Requestは[Windows: support check_is_installed_by_gem](https://github.com/mizzy/specinfra/pull/720)です。

### まとめ

今回は、Serverspec(Specinfra)の未実装の機能を追加して、Windowsでも他の環境同様に `it { should be_installed.by("gem") }` という記述で指定したgemがインストールされていることを確認できるようにフィードバックした話を紹介しました。

当社では、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Fluentdサポートサービス](/services/fluentd.html)を提供しています。Fluentd/Fluent Bitをエンタープライズ環境において導入/運用されるSIer様、サービス提供事業者様は、[お問い合わせフォーム](/contact/)よりお問い合わせください。

[^0]: td-agentはWindowsのインストーラに環境に合わせてWindows向けのgemをバンドルするようにしています。
