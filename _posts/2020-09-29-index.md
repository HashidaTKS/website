---
tags:
- groonga
title: Groongaのダンプから不具合の再現に必要なカラムのデータのみを抽出するには
---
Groongaのサポートをしていると、Groongaのデータベースのダンプを提供いただいて、不具合を再現させることが多いです。
その際、特定のカラムに問題があることがわかっている場合、問題のカラムだけをダンプから抽出できたほうが効率が良いです。
<!--more-->


例えば、データのロード中に問題が発生するなら、問題が発生するカラムのデータだけを抜き出して、再現しながらデバッグを進められれば、余計な処理が走らないのでデバッグしやすくなります。
ただ、データベースのダンプは巨大なことが多いです。巨大なファイルから該当のカラムのデータだけを見つけ出して抽出するのは困難です。

このような時に、`groonga-command-filter`というツールが使えます。
このツールはオプションに指定したカラムのデータだけをデータベースのダンプから抽出するツールです。

このツールの引数に問題のカラムを指定して実行すれば、簡単に問題のカラムのデータを抽出できます。
具体的には以下のように使います。

#### インストール方法

`groonga-command-filter`は以下のように`groonga-command-parser`をインストールすると使えるようになります。

```console
$ gem install groonga-command-parser
```


#### 使い方

例えば、以下のようなテーブルをダンプしたファイルがあるとします。(`groonga-command-filter`の動作をわかりやすくするために小さいデータを使います。)

![table]({{ "/images/blog/20200929_0.png" | relative_url }} "table")

ダンプファイルの中身は以下のようになっています。

```
table_create Blog TABLE_HASH_KEY ShortText
column_create Blog message COLUMN_SCALAR ShortText
column_create Blog title COLUMN_SCALAR ShortText

table_create IndexBlog TABLE_PAT_KEY ShortText --default_tokenizer TokenBigram --normalizer NormalizerAuto

load --table Blog
[
["_key","message","title"],
["grn1","Groonga message","Groonga test"],
["grn2","rakutan eggs 4 - 4 Groonga moritars","baseball result"],
["grn3","none","Groonga message"]
]

column_create IndexBlog index_message COLUMN_INDEX|WITH_POSITION Blog message
column_create IndexBlog index_title COLUMN_INDEX|WITH_POSITION Blog title
```


この中で、`title`カラムのデータのみを抽出したい場合は、以下のように`groonga-command-filter`を実行します。

```console
$ groonga-command-filter --include-column Blog.title ダンプファイル名
```


実行結果は以下のようになります。

```
table_create --flags "TABLE_HASH_KEY" --key_type "ShortText" --name "Blog"
column_create --flags "COLUMN_SCALAR" --name "title" --table "Blog" --type "ShortText"
load --table "Blog"
[
["_key","title"],
["grn1","Groonga test"],
["grn2","baseball result"],
["grn3","Groonga message"]]
```


下図の枠線内のデータ(`title`カラムのデータ)が抽出できてきることが確認できます。
(このテーブルは、`TABLE_HASH_KEY`なので、`_key`カラムの値は自動的に抽出されます。)

![select-column]({{ "/images/blog/20200929_2.png" | relative_url }} "select-column")

また、複数のカラムのデータを抽出するには、以下のように`--include-column`オプションを複数回使用して実現します。

```console
$ groonga-command-filter --include-column Blog.title --include-column Blog.message ダンプファイル名
```


実行結果は以下のようになります。

```
table_create --flags "TABLE_HASH_KEY" --key_type "ShortText" --name "Blog"
column_create --flags "COLUMN_SCALAR" --name "message" --table "Blog" --type "ShortText"
column_create --flags "COLUMN_SCALAR" --name "title" --table "Blog" --type "ShortText"
load --table "Blog"
[
["_key","message","title"],
["grn1","Groonga message","Groonga test"],
["grn2","rakutan eggs 4 - 4 Groonga moritars","baseball result"],
["grn3","none","Groonga message"]]
```


下図の枠線内のデータ(`title`カラムと`message`カラムのデータ)が抽出できてきることが確認できます。
(このテーブルは、`TABLE_HASH_KEY`なので、`_key`カラムの値は自動的に抽出されます。)

![select-columns]({{ "/images/blog/20200929_1.png" | relative_url }} "select-columns")

上記のように`groonga-command-filter`を使うと特定のカラムの値を簡単に抽出することができます。
Groongaのデータベースのダンプから特定のカラムのデータを抽出したいケースがあったら、ぜひこのツールをお使いください。
