---
tags:
  - apache-arrow
author: kou
title: Apache Arrow 3.0.0リリース
---
Apache Arrow 3.0.0でのコミット数2位、マージ数3位だった須藤です。（積極的に開発に参加しているほうの人であるというアピールです。）

2020年1月26日に[Apache Arrow 3.0.0がリリース](https://arrow.apache.org/blog/2021/01/25/3.0.0-release/)されました。

<!--more-->

### 3.0.0の概要

3.0.0での目玉はJuliaの公式サポートです。Julia実装は数年前からあり[Apache Arrowの最新情報（2018年9月版）]({% post_url 2018-09-05-index %})でも存在を匂わしていました。しばらくは[JuliaData/Arrow.jl](https://github.com/JuliaData/Arrow.jl)での開発も続けるそうですが、徐々に[apache/arrow](https://github.com/apache/arrow)での開発に軸足を移すそうです。

Julia実装はC++実装のバインディングではなく、Rust実装やGo実装と同じようにJuliaで一から実装されています。[Tables.jl interface](https://tables.juliadata.org/stable/)を実装しているため既存のもろもろのライブラリーといい感じに使えます。Juliaで使いやすいライブラリーに仕上がっていきそうですね！詳細は[Julia公式ブログでの紹介](https://julialang.org/blog/2021/01/arrow/)を参照してください。

開発が特にアクティブなのはRust実装です。Rust実装が特にアクティブになったのはApache Arrow 1.0.0がリリースされてからです。複数の企業がRust実装を使おうとしているようで、何人もアクティブに開発する人がいます。たとえば、InfluxDBの人たちは[InfluxDBの次世代コアとしてRust実装を使うことにした](https://www.influxdata.com/blog/announcing-influxdb-iox/)とアナウンスしています。やっぱり、フルタイムで開発に参加できると実装が進みますね。これまでは、フルタイムで開発しているのは[Ursa Labs](https://ursalabs.org/)（[Ursa Computing](https://ursacomputing.com/)）の人たちくらいでしたが、それ以外の企業も増えてきていい感じですね！（私ももう少し仕事の時間でApache Arrowの開発に参加したいな。私にお金を払ってApache Arrowに機能追加したい方は[お問い合わせ](/contact/)ください。）

今後、Rust実装をどうやって進めていくかを[Google Docs](https://docs.google.com/document/d/1qspsOM_dknOxJKdGvKbC1aoVoO0M3i6x1CIo58mmN2Y/edit#heading=h.m9f3h07axpxm)で相談しているので、Rust実装の今後に興味がある人はこちらも参照してください。

PythonユーザーにとってうれしいのはPython 3.9サポートでしょう。Apache Arrow 2.0.0リリースと3.0.0リリースの間にPython 3.9リリースがあったのですが、「Python 3.9のサポートはないの！？」という問い合わせがすごく多かったです。（10件くらいあった気がする。）Python 3.9に対応するために実装を変更する必要はなかったのですが、wheelがなくてインストールに困っている人が多かったです。みんな、wheelを使っているんですね。Python 3.9の少しあとにNumPy 1.20がリリースされたのですが、NumPy関連の互換性の問題がありました。pyarrow 3.0.0はそのあたりの問題も解決しています。最新のPython・NumPyを使うにはpyarrow 3.0.0が必要です。

ただし、1つ注意する点があります。pyarrow 3.0.0からmanylinux1のサポートをやめました。manylinux2010かmanylinux2014を使う必要があります。古いpipはmanylinux2010をサポートしていないので、Linux環境だとpyarrow 3.0.0のwheelを見つけてくれない！というときはpipをアップグレードしてから試してください。

### 3.0.0のビッグエンディアン対応

現在、多くのプラットフォームはリトルエンディアンを採用していますが、ビッグエンディアンを採用しているプラットフォームもあります。たとえばIBM Zです。Apache Arrowは[フォーマットレベルではリトルエンディアンもビッグエンディアンもサポート](https://arrow.apache.org/docs/format/Columnar.html#byte-order-endianness)しています。しかし、各言語でのApache Arrow実装におけるビッグエンディアンのサポートはリトルエンディアンに比べるとまだまだです。ビッグエンディアン対応は主にIBM関係の人たちが進めています。たとえば、C++実装・Java実装はApache Sparkのコミッターでもある[@kiszk](https://twitter.com/kiszk)さんが進めています。

3.0.0では次のようにビッグエンディアン対応が進みました。

  * C++実装・Java実装間でどちらもビッグエンディアンを使っている場合は動くようになった
    * どちらかがリトルエンディアンの場合はまだ動きません。エンディアンの変換は@kiszkさんが[#7507](https://github.com/apache/arrow/pull/7507)で進めています。
  * Go実装：単体テストが通るようになった

### 3.0.0の個人的なオススメ

私のコミットの多くはビルドシステムの改良だったりパッケージングまわり（Python 3.9のwheel対応もちょっとやった）だったりリリースまわりだったりCIを直したりだったり地味なものが多いです。（やる人があまりいないのでしょうがなく私がやっています。大事な作業だと思うんだけどなぁ。）そんな私ですが久しぶりにC++実装にまとまった機能を実装しました！それがテーブルのソート機能です。

Apache Arrowでの「テーブル」は複数の「カラム」がまとまったデータです。これまではカラム単位でソートできましたが、テーブルをソートすることができませんでした。単一のカラムでソートする場合はカラム単位のソートでも十分でしたが、複数カラムでソートする場合は十分ではありません。最初のソートキーが同じ値なら次のソートキーでソートしないといけません。C++実装でデータを扱うためには重要な機能の1つだと私は思いますが、だれも実装していなかったため私が実装しました！現在のC++実装の主な使い方は「高速なデータの読み書き」（参考：[Apache Arrowフォーマットはなぜ速いのか]({% link downloads/apache-arrow-fast-format.md %})）ですが、今後は「Apache Arrowフォーマットのデータを高速に処理するエンジン」としての使い方が増えてくるはずです。そのときにこのソート機能も活躍するはずです。なお、私の実装はそこそこの速さだったのですが、その後、C++実装をもりもり開発している人がさらに高速にしてくれました。（私の実装の方が速いケースもまだそれなりにあるので、私の実装も残っています。）

なお、この機能は[網屋](https://www.amiya.co.jp/)さんが開発している[ALog](https://www.amiya.co.jp/solutions/alog/)というログ収集製品で必要だったので網屋さんの支援のもと私が仕事として実装しました。（仕事の時間でApache Arrowの開発をしました。）私にお金を払ってApache Arrowに機能追加したい方は[お問い合わせ](/contact/)ください。

### まとめ

Apache Arrow 3.0.0がリリースされたので紹介しました。

今のApache Arrowの開発体制の問題の1つは「リリースが大変」です。どうしたらいいかメーリングリストで[議論している](https://lists.apache.org/thread.html/rf43d270b4dde2dce601c69fdbb0ab9e741232149e6c8a24caa6ac0c8%40%3Cdev.arrow.apache.org%3E)ので、Apache Arrow関連でなにかやれることがないかなと思っている人はこのあたりから参加してみるのはどうでしょうか？今は3ヶ月単位くらいでリリースしていますが、リリースが楽になると毎月リリースもできるかもしれません。個人的には「リリース関連作業に協力する人が少ない」ことが原因の1つの気がしているので人手が増えるのは解決につながる気はします。
