---
title: 非推奨となったapt-keyの代わりにsigned-byとgnupgを使う方法
author: kenhys
---

`apt-key(8)`はapt 2.1.8より非推奨となりました。

Ubuntu 20.10 (Groovy)やそろそろリリースの時期が近づいてきたDebian 11(Bullseye)からは`apt-key`を利用すると警告されます。

```text
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
```

今回は`apt-key`を使うのをやめて、代替手段へと(パッケージ提供側が)移行する方法について紹介します。

<!--more-->

### apt-keyはどのような挙動をするのか

ソフトウェアを簡単にインストールできるように、開発元が独自にAPTリポジトリを提供してくれている場合があります。
その場合、パッケージの署名に使っている公開鍵をあらかじめインポートするように指示していることがあります。

`apt-key`はその鍵をインポートするのにこれまでよく使われていました。

ただし、`apt-key`は(対象となるキーリングを指定しない場合には)鍵を`/etc/apt/trusted.gpg`へと取り込みます。
`/etc/apt/trusted.gpg`はAPTが既定で信頼する鍵として扱うことを意味します。
単に特定のリポジトリにひもづいた鍵をそちらへ追加するのは、あまりよいやりかたではありません。

### apt-keyから移行する方法

`apt-key`を使ったやりかたから移行する方法はいくつかあります。

* `apt-key`を使ってキーリングに変換し、対象リポジトリとひもづける方法
* `gnupg`を使ってキーリングに変換し、対象リポジトリとひもづける方法(おすすめ)

`apt-key`はまだ使えるのでどちらを使っても同じです。ただ、今後削除されることを踏まえると、`gnupg`を使ったやりかたがおすすめです。


#### apt-keyを使ってキーリングに変換し、対象リポジトリとひもづける方法

インポート元の鍵が`sample.key`で移行先のキーリングを`archive-keyring.gpg`とした場合の変換は次のようにして行います。

```console
$ apt-key --keyring ./archive-keyring.gpg add sample.key
```

`apt-key`が削除され、使えなくなるまでは有効な方法です。`apt-key`が削除されたあとに移行する必要がでてきたときは、`gnupg`を使いましょう。


キーリングを対象リポジトリとひもづける方法は後述します。

### `gnupg`を使ってキーリングに変換し、対象リポジトリとひもづける方法(おすすめ)

インポート元の鍵が`sample.key`で移行先のキーリングを`archive-keyring.gpg`とした場合の変換は次のようにして行います。


```console
$ gpg --no-default-keyring --keyring ./archive-keyring.gpg --import archive.key
```

期待通りに変換できたかは、`file`コマンドを使うとわかります。

```console
$ file archive-keyring.gpg
archive-keyring.gpg: GPG keybox database version 1 (省略)
```

上記のように`GPG keybox database version 1`となっていれば期待通りに変換できています。

キーリングを対象リポジトリとひもづける方法は次で述べます。

### 変換したキーリングを対象リポジトリとひもづける方法

APTのデータ取得元設定リストでは、どの鍵で署名されたかを明示することができます。

そのため、サードパーティーの鍵は`/usr/share/keyrings/`配下において、
どの鍵で署名されているかを`signed-by`で明示してひもづけるのがおすすめです。

変換したキーリングファイルが`archive-keyring.gpg`であれば、次のようにして対象リポジトリとひもづけます。

`/etc/sources.list.d/`配下に置かれた取得元の設定リストは次のようになるはずです。

```
deb [signed-by=/usr/share/keyrings/archive-keyring.gpg] https://packages.example.com/debian stable main
```

このようにすることで、`/etc/apt/trusted.gpg.d`配下に変換後のキーリングを置かなくても、正規のリポジトリからパッケージをインストールできます。
キーリングが信用されるリポジトリの範囲を制限できるのでおすすめです。

### まとめ

今回は、非推奨となった`apt-key`から移行する方法について紹介しました。
Debian 11 (Bullseye)が`apt-key`が使える最後のリリースです。Debian 12 (Bookworm)で削除されますので移行はお早めに。
