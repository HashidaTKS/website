---
tags:
- ruby
- presentation
- apache-arrow
title: 'RubyKaigi 2022 - Fast data processing with Ruby and Apache Arrow #rubykaigi'
author: kou
---

[RubyKaigi 2022](https://rubykaigi.org/2022/)で[Fast data processing with Ruby and Apache Arrow](https://rubykaigi.org/2022/presentations/ktou.html)という[Apache Arrow](https://arrow.apache.org/)を使ってRubyで高速にデータ処理する話をした須藤です。

なお、[クリアコードはシルバースポンサーとしてRubyKaigi 2022を応援](https://rubykaigi.org/2022/sponsors/#sponsor-333)しました。

<!--more-->

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2022/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; box-sizing: content-box; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2022/" title="Fast data processing with Ruby and Apache Arrow">Fast data processing with Ruby and Apache Arrow</a>
  </div>
</div>

関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2022/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/rubykaigi-2022)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-rubykaigi-2022)

### 内容

[RubyKaigi Takeout 2021のRed Arrowのトーク]({% post_url 2021-08-23-rubykaigi-takeout-2021-announce %})ではRed Arrowを中心にできることをたくさん紹介しました。その発展形として今年は実際に使えそうな感じになっていることを紹介したかったので、高速データ処理機能にフォーカスすることにしました。が、採択されて資料を作り始めてみると「実際に使えそう」というには各機能の実装にもう少しブラッシュアップが必要なことがわかりました。なんと。。。

ということで、Apache Arrowを使って高速にデータ処理できる各種方法について次のようにまとめることにしました。

  * 高速にデータを処理したいケース
  * そのケースの実現を阻害する課題
  * その課題の解決方法の概要
  * 実際に動くRubyのコードをデモ

いろいろなケースがあった方が「このケースは自分も高速にしたい！」と感じる人が増えそうかと思い、できるだけ多くのケースを入れるようにまとめました。が、その代償として各ケースの詳細や速さがわかる数値を入れることは諦めました。そのため、「すごそうな気がする！」レベルの話になっています。

ただ、それでも興味を持ってくれた人は例年より多く、[Red Data Toolsのチャット](https://gitter.im/red-data-tools/ja)にはすでに数人新しい人が入ってきてくれています！話してよかった！

でも、いつか、詳細も説明する機会があるといいな。。。ということで、今回のトークについて深堀りするイベントを次の通り開催します！

  * イベント名：RubyKaigi 2022 after event - 「Fast data processing with Ruby and Apache Arrow」のスピーカーに質問しよう！
  * 目的：RubyKaigi 2022のトーク「Fast data processing with Ruby and Apache Arrow」で話されなかったことについてスピーカーに質問して理解を深めよう！
  * 日時：9月22日（木）10:00-11:00
  * 場所：オンライン（YouTube live: https://www.youtube.com/watch?v=bpuJWC9_USY ）
  * 参加者：
    * 須藤（スピーカー）
    * 藤本（質問者、[How fast really is Ruby 3.x?](https://rubykaigi.org/2022/presentations/fujimotos.html)のスピーカー）
    * 田辺（質問者、[@sunaot](https://twitter.com/sunaot)、[RubyKaigi 2022でシャトルバススポンサーをしていたエス・エム・エスさん](https://tech.bm-sms.co.jp/entry/2022/09/11/182627)の技術責任者）
  * 参加者以外からの質問も受け付けるので、聞きたいことがある人はイベント終了までの間に次のいずれかの手段で質問を共有してください。
    * [↑のYouTube liveページ](https://www.youtube.com/watch?v=bpuJWC9_USY)のコメント・チャット
    * [Red Data Toolsのチャット](https://gitter.im/red-data-tools/ja)
    * Twitterで[#reddatatools](https://twitter.com/hashtag/reddatatools)付きでツイート

### オフライン参加

久しぶりにオフライン参加できるようになったので今年はオフラインで参加しました。オンラインでの参加では発表関連のコミュニケーションしかできませんでしたが、オフラインでの参加ではちょっとした相談事ができるのがよいところです。

たとえば、Red Data Toolsに参加している[@ericgpks](https://github.com/ericgpks)に今取り組んでいる[strscanの改良方法](https://github.com/ruby/strscan/pull/44#discussion_r959309634)について[口頭で説明](https://note.com/sq_engch5/n/n9cd59c7bc729)したり、同じくRed Data Toolsに参加している[@otegami](https://github.com/otegami)に[activerecord-duckdb-adapter](https://github.com/red-data-tools/activerecord-duckdb-adapter)の実装方針を説明したりできました。

他にもtest-unitやRubyGemsやMySQLのFlight SQLインターフェイスなども相談できました。

自宅から参加できるのでオンラインでの参加の方が楽ですが、オフラインでの参加でしかできない（しにくい）こともあるので、今回はオフラインで参加してよかったです。

### まとめ

RubyKaigi 2022でRubyとApache Arrowで高速にデータ処理をする方法を紹介しました。ただ、どうして高速なのかといった詳細は紹介できなかったので9月22日（木）10:00から詳細を聞くイベントを開催します。RubyとApache Arrowが気になる人はぜひ質問をもって参加してください。

私と一緒に「Rubyでデータ処理をできるようにする！」を仕事として取り組みたい人は[クリアコードのApache Arrow関連業務の募集要項]({% link recruitment/apache-arrow.md %})を読んでください。
