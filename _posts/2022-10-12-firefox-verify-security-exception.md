---
title: "作業環境からアクセスできないホストのTLS証明書に関するブラウザーの挙動を検証する方法"
author: kenhys
tags:
- mozilla
---

### はじめに

クリアコードでは、[Firefoxサポートサービス]({% link services/mozilla/menu.html %})の一環として、お客さまからの要望に基づいてカスタマイズしたFirefoxの提供もしています。
要望のなかには、[特定のホストの証明書の例外を自動的に承認したい](https://github.com/clear-code/firefox-support-common/blob/master/esr102/Security#L1203-L1313)というものがあります。

例外として登録しておくサーバーがお客様の環境からしかアクセスできない事例もあり、要望の通りにカスタマイズできているかを事前に検証するためには、
テスト用の証明書を用意して確かめる必要がでてきます。

今回は、先述のFirefoxのカスタマイズの検証を例として、TLSで通信する特定のホストを対象としたブラウザーの動作をローカルで検証する方法を紹介します。

<!--more-->

### 検証するための準備

実際に検証できるようにするためには、次の手順が必要です。
なお、検証対象のサイトは https://myserver とし、Firefoxは[特定のホストの証明書の例外を自動的に承認する](https://github.com/clear-code/firefox-support-common/blob/master/esr102/Security#L1203-L1313)で https://myserver を承認するためのカスタマイズを適用済みであるものとします。

  * 対象のホストの名前解決ができるようにする
  * 対象のホストの証明書を用意する
  * 対象のホストの証明書を使用してHTTPSサーバーを起動する
  * カスタマイズ済みFirefoxで実際にアクセスして自動的に承認されるか確認する

#### 対象のホストの名前解決ができるようにする

簡単なやりかたは、`hosts`を使って名前解決できるようにすることです。

`c:\Windows\System32\drivers\etc\hosts`に`myserver`を解決できるようにエントリを追加します。

```text
127.0.0.1 myserver
```

#### 対象のホストの証明書を用意する

Let's Encryptで紹介されている、[自分の証明書を作成して信頼する](https://letsencrypt.org/ja/docs/certificates-for-localhost/#%E8%87%AA%E5%88%86%E3%81%AE%E8%A8%BC%E6%98%8E%E6%9B%B8%E3%82%92%E4%BD%9C%E6%88%90%E3%81%97%E3%81%A6%E4%BF%A1%E9%A0%BC%E3%81%99%E3%82%8B/)という項目を参考に、次のようにして対象ホストの証明書を作成します。

```bash
$ openssl req -x509 -out myserver.crt -keyout myserver.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=myserver' -extensions EXT -config <( \
  printf "[dn]\nCN=myserver\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:myserver\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```

Windows環境の場合は、上記のままだと動かないので、次のように設定ファイルを経由するように変更して対象のホストの証明書を作成します。

```cmd
openssl.exe req -x509 -out myserver.crt -keyout myserver.key -newkey rsa:2048 -nodes -sha256 -subj "//CN=myserver" -extensions EXT -config config.txt
```

`-config`の引数にあたえるファイル(config.txt)の内容は次のとおりとします。

```text
[dn]
CN=myserver
[req]
distinguished_name = dn
[EXT]
subjectAltName=DNS:myserver
keyUsage=digitalSignature
extendedKeyUsage=serverAuth
```

#### 対象のホストの証明書を使用してHTTPSサーバーを起動する


あらかじめ作成しておいた証明書と秘密鍵を使って https://myserver を起動します。

```cmd
openssl.exe s_server -cert myserver.crt -key myserver.key -accept 443 -WWW
```

実行するとWindows Defenderによりファイアウォールのブロックの確認がなされるのでアクセスを許可します。

![ファイアウォールのアクセス許可]({% link images/blog/firefox-verify-security-exception/accept-firewall.png %})


なお、あえてTLS1.0かつ3DESを有効にした古い環境で疎通できることを検証しないといけない場合には、次のように明示的にプロトコルバージョンや暗号スイートを指定して起動します。ただし、3DESはOpenSSL 1.1.0でサポートされなくなっているので、古め(1.0.2系など)のバージョンでないと試せません。[^old-openssl]

[^old-openssl]: https://indy.fulgan.com/SSL/Archive/ などで古めのWindows向けバイナリが公開されているようです。

```cmd
openssl.exe s_server -cert portal.crt -key portal.key -accept 443 -WWW -cipher DES-CBC3-SHA -tls1
```

#### カスタマイズ済みFirefoxで実際にアクセスして自動的に承認されるか確認する

サーバーの準備がととのったら、実際にFirefoxでアクセスしてみます。
openssl.exeと同階層にindex.htmlを用意しておけば、 https://myserver/index.html にアクセスするとセキュリティ例外が自動的に承認された状態で表示されるはずです。

![セキュリティ例外が自動承認された画面]({% link images/blog/firefox-verify-security-exception/allow-exception.png %})


### おわりに

今回は、Firefoxでの「TLS証明書の例外を自動承認する」カスタマイズの検証を例として、TLSで通信する特定のホストを対象としたブラウザーの動作をローカルで検証する方法を紹介しました。
そのような検証が必要になったときに参考にしてみてください。

クリアコードでは、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Firefoxサポートサービス]({% link services/mozilla/menu.html %})を提供しています。企業内でのFirefoxの運用でお困りの情シスご担当者さまやベンダーさまは、[お問い合わせフォーム]({% link contact/index.md %})よりお問い合わせください。


