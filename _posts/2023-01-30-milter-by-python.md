---
title: メールフィルタープラグインであるmilterをPythonで簡単実装！ - milterを動かしてみよう編
author: daipom
tags:
  - milter-manager
  - unix
---

前回と前々回の記事では、[milter manager](https://milter-manager.osdn.jp/index.html.ja)というメールフィルタを管理するための自由ソフトウェアを、[GObject Introspection](https://gi.readthedocs.io/en/latest/)に対応させてバインディングを生成することについて紹介しました。

* [Mesonを使ってGObject Introspection対応のビルドシステムを構築する方法]({% post_url 2022-08-17-meson-and-gobject-introspection %})
* [GObject Introspectionによるバインディングの（ほぼ）自動生成]({% post_url 2023-01-19-meson-and-gir %})

milter managerは従来からRubyでmilterを作るためのライブラリーを提供してきましたが、今回のGObject Introspection対応によって、Pythonでmilterを作るためのライブラリーも提供するようになりました。

今回は、milter managerの機能を使ってPythonで書いてみます。
また、書いたmilterを実際に動かす方法も紹介します。

<!--more-->

## milter

milterとは、SendmailやPostfixなどのMTA(Mail Transfer Agent)に組み込むメールフィルタープラグインのことです。
が、ThunderbirdやOutlookなら分かるけれど、SendmailとかPostfixとかMTAとか聞いたことがない！という方もいらっしゃるかもしれません。
まずは、メールが届く仕組みから簡単に解説します。

### メールが届く仕組み

ThunderbirdやOutlookなどのメールクライアントでメールを作成し、送信するケースを考えてみます。
このように送信されたメールは、送信先に直接届くのではなく、幾つかのメールサーバーを経由して届きます。

まずメールクライアントは近くの(メールクライアントで設定をした)メールサーバーにメールを送信します。
するとメールサーバーが、メールの宛先ドメインに応じた別のメールサーバーへとメールを転送します。
こうしてメールの宛先ドメインであるメールサーバーに届けられたメールは、そのサーバー内で保管されます。
最終的に、受信側のメールクライアントがそのメールサーバーに問い合わせることで、そのメールが届きます。

ユーザーが直接操作をする、メールの送信/受信を行うメールクライアント(メーラー)のことを、MUA(Mail User Agent)と呼びます。
代表的なMUAとして、ThunderbirdやOutlookなどがあります。

一方でメールサーバーにおいて、メールの転送を担う機能、もしくはそのソフトウェアのことをMTA(Mail Transfer Agent)と呼びます。
代表的なMTAとして、SendmailやPostfixなどがあります。
普段ユーザーが意識することはないかもしれませんが、MUAにSMTPサーバーとして登録するのがMTAです。
MTAは、MUAやMTAから受信したメールを、別のMTAに転送します。
自身が宛先である、すなわちゴールである場合は、転送せずに自身のサーバー内でメールを保管します[^mail-receiving]。

MTAは、宛先のドメインをDNSに問い合わせることで、転送先のMTAを特定します[^dns]。
最終的な宛先であるMTAに届くまでに、複数のMTAを経由することもあります。
最終的な宛先である「To」に対して、転送時など実際に送信する先のことを「Envelope To」と呼んで区別します[^envelope]。
「From」も同様に区別します。
例えば複数のMTAを経由する場合は、「To」と「Envelope To」が異なることがあります。

[^mail-receiving]: 受信したメールを各ユーザーのメールボックスに配信する機能はMTAとは別にMDA(Mail Delivery Agent)と呼ばれます。MUAからのリクエストに応じて受信したメールを返す機能はMRA(Mail Retrieval Agent)と呼ばれます。POPサーバーやIMAPサーバーと呼ばれるソフトウェアのことです。
[^dns]: MXレコードを引くことでMTAのホスト名を取得し、さらにそのAレコードまたはAAAAレコードを引くことでIPアドレスを取得します。MTAの候補が複数あることもあります。その場合は、MXレコードの優先度に応じて順に試行します。
[^envelope]: メールを配送するために封筒（envelope）に包んでいるイメージです。メール自体のFrom/Toと封筒のFrom/Toがある、と考えるとイメージしやすいかもしれません。

### milterとmilter manager

milterとは「*m*ail f*ilter*」の略で、SendmailやPostfixといったMTAのメールフィルタプラグイン、またはその仕組み、プロトコルのことです。
MTAにおいて迷惑メールフィルタやウィルスチェックをするためにmilterを利用できます。
元々はSendmailの仕組みでしたが、Postfixもサポートするようになった経緯があります。
SendmailやPostfixのように、milterをサポートしているMTAであれば、共通のmilterを利用できます。

前回と前々回の記事では、[milter manager](https://milter-manager.osdn.jp/index.html.ja)を[GObject Introspection](https://gi.readthedocs.io/en/latest/)に対応させた事例を紹介してきました。
milter managerとは、milterを効果的に利用するためのmilterであり、クリアコードが開発した自由ソフトウェアです。
milter managerは、MTAとmilterとの間のプロキシとして動作し、柔軟にmilterを適用できます。
またmilter managerは、milterをRuby/Pythonで簡単に作るためのライブラリーも提供しています（前回と前々回の記事で、Pythonをサポートしました）。

milter managerのwebページに、milterとmilter managerについて分かりやすい説明があるので、ぜひご覧ください。

* [はじめに — milter managerの概要](https://milter-manager.osdn.jp/reference/ja/introduction.html)

また、milterプロトコルについてはこちらの記事で説明しています。

* [milterプロトコル]({% post_url 2014-12-10-index %})

## Pythonで作ったmilterを動かす

実際にmilterを作って動かしてみましょう。

### Postfixの用意

milterは、milterプロトコルをサポートしているMTAのプラグインとして機能します。
まずは、MTAとしてPostfixを動かしてみましょう。
以下、Ubuntu 20.04環境の例になります。

Postfixのインストール

```console
$ sudo apt install postfix
```

インストール中に、`Postfix Configuration`の設定画面が出てくるので、以下のように設定します。

* `configuration type`は、`Local only`を選択します。
  * 今回はローカルで動かすだけだからです。
* `mail name`はデフォルトで問題ありません。

### メールを送信してみる

Postfixにメールを送信してみましょう。
メールを送る方法は色々ありますが、今回はSMTPプロトコルに従ったメッセージを直接送信することで、メールを送信してみます。

SMTPプロトコルは、メールを送信するのに用いるプロトコルです。
MUAからMTAへのメール送信や、MTAからMTAへのメール転送に使われます。
このプロトコルに従ってメッセージを送信することで、メールを送信したことになります。

クライアント側のSMTPプロトコルの簡単な使い方は、次のようになります。

1. `EHLO {hostname}`を送信して、自身のhostnameを伝える。
1. `MAIL FROM: {Envelope From}`を送信して、メールの`Envelope From`を伝える。
1. `RCPT TO: {Envelope To}`を送信して、メールの`Envelope To`を伝える。
1. `DATA`を送信して、実際のメール内容の送信を開始することを伝える。
1. タイトルやFrom/Toなど、メールのヘッダーを1行ずつ送信する。
1. ヘッダーを全て送信し終えたら、空行（改行）を送信し、ヘッダーの送信を完了したことを伝える。
1. メールの本文を送信する。
1. 本文を全て送信し終えたら、`.`のみの行を送信し、メールの送信を全て完了したことを伝える[^smtp-complete]。

例えば、タイトルも本文も`Hello, world!`であるメールを自身に送る場合、次のようなメッセージになります。
`$USER`は現在のユーザー名を表す環境変数です。

```
EHLO localhost
MAIL FROM: $USER@localhost
RCPT TO: $USER@localhost
DATA
Subject: Hello, world!
From: $USER@localhost
To: $USER@localhost

Hello, world!
.
```

実際にメールを送信してみましょう。
例えば`echo`と`nc`コマンドを使って次のように送信できます。

```
$ {
    echo "EHLO localhost"
    echo "MAIL FROM: $USER@localhost"
    echo "RCPT TO: $USER@localhost"
    echo "DATA"
    echo "Subject: Hello, world!"
    echo "From: $USER@localhost"
    echo "To: $USER@localhost"
    echo
    echo "Hello, world!"
    echo "."
} | nc -q 1 127.0.0.1 25
```

メールを確認してみましょう。
`/var/mail/${USER}`に記録されているはずです。

```console
$ cat /var/mail/${USER}
From root@localhost  Thu Jan 12 09:13:07 2023
Return-Path: <root@localhost>
X-Original-To: root@localhost
Delivered-To: root@localhost
Received: from localhost (localhost [127.0.0.1])
    by tmp2.lxd (Postfix) with ESMTP id E86341046D
    for <root@localhost>; Thu, 12 Jan 2023 09:13:06 +0000 (UTC)
Subject: Hello, world!
From: root@localhost
To: root@localhost
Message-Id: <20230112091306.E86341046D@tmp2.lxd>
Date: Thu, 12 Jan 2023 09:13:06 +0000 (UTC)

Hello, world!
```

[^smtp-complete]: この後に再び`MAIL FROM`から繰り返して、次のメールの送信処理を続けることもできます。

### milterを動かしてみる

さて、いよいよmilterを動かして、送信したメールにフィルタリング処理をさせてみましょう。

一度話を戻すと、クリアコードが開発した[milter manager](https://milter-manager.osdn.jp/index.html.ja)は、その1つの機能として、Rubyで簡単にmilterを作るためのライブラリーを提供していました。
今回それを[GObject Introspection](https://gi.readthedocs.io/en/latest/)に対応させたことで、Pythonでもmilterを作ることができるようになりました。

Rubyによるmilter開発のチュートリアルがあります。Pythonも基本的に同じですので、こちらもぜひご覧ください。

* https://milter-manager.osdn.jp/reference/ja/ruby-milter-tutorial.html

今回は、Pythonでmilterを作って動かしてみましょう。
2つの実装例がmilter manager v2.2.5に梱包されています。

* [外部コマンドを実行する例](https://github.com/milter-manager/milter-manager/blob/2.2.5/binding/python/sample/milter-external.py)
* [特定のワードを置換する例](https://github.com/milter-manager/milter-manager/blob/2.2.5/binding/python/sample/milter-replace.py)

まずは、[特定のワードを置換する例](https://github.com/milter-manager/milter-manager/blob/2.2.5/binding/python/sample/milter-replace.py)を参考にして、
メール中のワードを置換させてみましょう。
最後の`patterns`の定義部分を少し修正して、`world`を`ClearCode`に置換するようにしてみます。
また最初の`sys.path`の更新処理は、今回は必要ないので消してあります。

```py
#!/usr/bin/env python3
#
# Copyright (C) 2022  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re

import milter.client

class MilterReplace(milter.client.Session):
    def __init__(self, context, patterns):
        super().__init__(context)
        self._patterns = patterns

    def header(self, name, value):
        self._headers.append([name, value])

    def body(self, chunk):
        self._body += chunk

    def end_of_message(self):
        header_indexes = {}
        for name, value in self._headers:
            if name not in header_indexes:
                header_indexes[name] = 0
            header_indexes[name] += 1
            for pattern, replaced in self._patterns.items():
                replaced_value, _ = pattern.subn(replaced, value)
                if value != replaced_value:
                    self._change_header(name,
                                        header_indexes[name],
                                        replaced_value)
                    break

        for pattern, replaced in self._patterns.items():
            body = self._body.decode("utf-8")
            replaced_body, _ = pattern.subn(replaced, body)
            if body != replaced_body:
                self._replace_body(replaced_body)

    def reset(self):
        self._headers = []
        self._body = b""

command_line = milter.client.CommandLine()
with command_line.run() as (client, options):
    # "world" を "ClearCode" に置換する！
    patterns = {
        re.compile("world", re.IGNORECASE): "ClearCode",
    }
    client.register(MilterReplace, patterns)
```

このPythonコードを、`milter_replace.py`ファイルとして保存します。

さて、このmilterを実行するには、milter managerが提供しているライブラリーが必要です。
以下のようにインストールします。

```console
$ sudo add-apt-repository ppa:milter-manager/ppa
$ sudo apt install python3-milter-core python3-milter-client
```

以上で準備はできました。milterを実行してみます。
`--verbose`オプションを付けることで、詳細なログを出力してくれます。

```console
$ python3 milter_replace.py --verbose
```

すると、milterが動作している状態になります。
デフォルトでは、20025番ポートで処理を待ち受けています。
Postfixにこのmilterを登録します。

`smtpd_milters = inet:127.0.0.1:20025`の設定を追加します。

```console
$ echo "smtpd_milters = inet:127.0.0.1:20025" | sudo tee -a /etc/postfix/main.cf
$ sudo systemctl restart postfix
```

以上でmilterをPostfixに登録できました。
先ほどと同様にメールを送信して動作を確認してみましょう。
milterが多くのログを出力し、何やら動作したのが分かるはずです。
メールを確認してみましょう。

```console
$ cat /var/mail/${USER}
From root@localhost  Thu Jan 12 09:13:07 2023
Return-Path: <root@localhost>
X-Original-To: root@localhost
Delivered-To: root@localhost
Received: from localhost (localhost [127.0.0.1])
    by tmp2.lxd (Postfix) with ESMTP id E86341046D
    for <root@localhost>; Thu, 12 Jan 2023 09:13:06 +0000 (UTC)
Subject: Hello, world!
From: root@localhost
To: root@localhost
Message-Id: <20230112091306.E86341046D@tmp2.lxd>
Date: Thu, 12 Jan 2023 09:13:06 +0000 (UTC)

Hello, world!

From root@localhost  Thu Jan 12 15:29:10 2023
Return-Path: <root@localhost>
X-Original-To: root@localhost
Delivered-To: root@localhost
Received: from localhost (localhost [127.0.0.1])
    by tmp2.lxd (Postfix) with ESMTP id 11E2010D21
    for <root@localhost>; Thu, 12 Jan 2023 15:29:10 +0000 (UTC)
Subject: Hello, ClearCode!
From: root@localhost
To: root@localhost
Message-Id: <20230112152910.11E2010D21@tmp2.lxd>
Date: Thu, 12 Jan 2023 15:29:10 +0000 (UTC)

Hello, ClearCode!
```

前回のメールに続き、新しくメールが記録されています。
新しいメールでは、タイトル（`Subject`）と本文の双方で、`Hello, world!`が`Hello, ClearCode!`に置換されています！

以上で、Pythonで作ったmilterを動かすことができました。
milterを使うことで、MTAにおいてメールの内容を加工したり、条件によってメールをブロックしたりすることができます。
そして、milter managerが提供するライブラリーを使うことで、PythonやRubyで簡単にmilterを開発することができるのです。

## まとめ

前回と前々回の記事では、[milter manager](https://milter-manager.osdn.jp/index.html.ja)というメールフィルタを管理するための自由ソフトウェアを、[GObject Introspection](https://gi.readthedocs.io/en/latest/)に対応させてバインディングを生成することについて紹介しました。

* [Mesonを使ってGObject Introspection対応のビルドシステムを構築する方法]({% post_url 2022-08-17-meson-and-gobject-introspection %})
* [GObject Introspectionによるバインディングの（ほぼ）自動生成]({% post_url 2023-01-19-meson-and-gir %})

本記事ではこれらに続いて、実際にPythonで書かれたmilterを動かしてみることについて紹介しました。
milter managerを使えば、PostfixやSendmailなどのMTAにおけるメールのフィルタリングをこんなに便利にできるんだ、と実感していただけたら幸いです。

次回は、milterの実装の仕方についてより詳しく紹介をする予定です。

クリアコードでは[milter manager](https://milter-manager.osdn.jp/index.html.ja)を始め、様々な自由ソフトウェアの開発・サポートを行っております。
詳しくは次をご覧いただき、こちらの[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。

* [メールシステム(milter manager)サービス]({% link services/milter-manager.html %})
* [クリアコードが提供する主なサービス一覧]({% link services/index.md %})

また、クリアコードではこのように業務の成果を公開することを重視しています。
業務の成果を公開する職場で働きたい人は[クリアコードの採用情報]({% link recruitment/index.md %})をぜひご覧ください。
