---
title: "Firefoxでウェブからダウンロードしたファイルの扱いを指定する方法"
author: kenhys
tags:
  - mozilla
---

### はじめに

Firefoxにはダウンロードしたファイルをどのアプリケーションで扱うか指定するためのポリシーがあらかじめ用意されています。
そのため、特定の拡張子のファイルを任意のアプリケーションで開くようにあらかじめカスタマイズする、というようなことができます。

今回は、Firefoxでウェブからダウンロードしたファイルの扱いをポリシーを使ってカスタマイズする方法を紹介します。

<!--more-->

ウェブからダウンロードしたファイルをFirefoxがどう扱うかの設定は 「一般」> 「プログラム」の項目から行えます。


既定では次のスクリーンショットのような設定が有効です。[^general-programs]

[^general-programs]: Firefox 102.8.0esrの場合

![Firefoxの設定画面におけるファイルとプログラムの関連付け設定一覧のスクリーンショット。ファイルの種類ごとに「Firefoxで開く」「ファイルを保存」などの取り扱い方法が選択されている。]({% link images/blog/firefox-specify-handlers-policy/firefox-programs.png %})

この設定は `%APPDATA%\Roadming\Mozilla\Firefox\Profiles\(プロファイルフォルダ)\handlers.json`に保存されています。

このファイルはユーザーごとに保存されますが、ポリシーファイルを作成し、あらかじめ配置することでユーザーによらず設定を統一することが可能です。

ポリシーでどのような定義が行えるかは、[Handlersポリシーの定義](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#Handlers)
を参照するとわかります。

Handlersポリシーでは次のカスタマイズを行うことができます。

* 処理のトリガーはなにか(MIMEタイプ、拡張子、カスタムURLスキーム)
* どのようなアクションを実行するか (ディスクに保存する、指定したアプリを起動する、既定のアプリにまかせる)
* 実行時にユーザーに確認を求めるか

それぞれの場合でどのように指定するかを説明します。

### MIMEタイプをもとにどう扱うか指定する

もし、`text/csv`であれば`notepad.exe`で開きたい場合、次のような設定を `C:\Program Files\Mozilla Firefox\distribution\policies.json` に行います。

```json
{
  "policies": {
    "Handlers": {
      "mimeTypes": {
        "text/csv": {
          "action": "useHelperApp",
          "ask": false,
          "handlers": [
            {
              "name": "notepad.exe",
              "path": "C:\\Windows\\System32\\notepad.exe"
            }
          ]
        }
      }
    }
  }
}
```

これで`text/csv`なファイルをダウンロードしたときに、`notepad.exe`を利用してユーザーに問い合わせすることなくファイルを開けます。
`path` はFirefoxがアプリケーションを見つけられるように、フルパスを指定する必要があります。

### 拡張子をもとにどう扱うか指定する

もし、拡張子が`csv`であれば`notepad.exe`で開きたい場合、次のような設定を `C:\Program Files\Mozilla Firefox\distribution\policies.json` に行います。

```json
{
  "policies": {
    "Handlers": {
      "extensions": {
        "csv": {
          "action": "useHelperApp",
          "ask": false,
          "handlers": [{
            "name": "notepad.exe",
            "path": "C:\\Windows\\System32\\notepad.exe"
          }]
        }
      }
    }
  }
}
```

これで拡張子が`csv`なファイルを、ユーザーに問い合わせすることなく`notepad.exe`で開けます。

### カスタムURLスキームをもとにどう扱うか指定する

もし、任意のURLスキーム(例: `mycustomscheme`)をあらかじめ登録済みで、そのURLスキームに応じてアプリを起動するなら、次のような設定を
`C:\Program Files\Mozilla Firefox\distribution\policies.json` に行います。

```json
{
  "policies": {
    "Handlers": {
      "schemes": {
        "mycustomscheme": {
          "action": "useSystemDefault",
          "ask": false
        }
      }
    }
  }
}
```

これで`mycustomscheme`を登録したときに指定した既定のアプリを利用して、ユーザーに問い合わせすることなくファイルを開けます。

### MIMEタイプが異なる拡張子がcsvなファイルを扱う場合

例えばCSVの場合、MIMEタイプは`text/csv`だけとは限りません。
そのような場合には、MIMEタイプの指定と、`extensions`の指定を併用するのがおすすめです。

```json
{
  "policies": {
    "Handlers": {
      "mimeTypes": {
        "text/csv": {
          "action": "useHelperApp",
          "ask": false,
          "handlers": [{
              "name": "notepad.exe",
              "path": "C:\\Windows\\System32\\notepad.exe"
          }],
          "extensions": [
            "csv"
          ]
        },
        "text/x-comma-separated-values": {
          "action": "useHelperApp",
          "ask": false,
          "handlers": [{
              "name": "notepad.exe",
              "path": "C:\\Windows\\System32\\notepad.exe"
          }],
          "extensions": [
            "csv"
          ]
        }
      }
    }
  }
}
```


### おわりに

今回は、Firefoxでウェブからダウンロードしたファイルの扱いをポリシーを使ってカスタマイズする方法を紹介しました。
カスタムURLスキーム、MIMEタイプ、拡張子による指定など、ダウンロードされる状況等に応じて、柔軟にカスタマイズできることがわかります。

クリアコードでは、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Firefoxサポートサービス]({% link services/mozilla/menu.html %})を提供しています。企業内でのFirefoxの運用でお困りの情シスご担当者さまやベンダーさまは、[お問い合わせフォーム]({% link contact/index.md %})よりお問い合わせください。
  
