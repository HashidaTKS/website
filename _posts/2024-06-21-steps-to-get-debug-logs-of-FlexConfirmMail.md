---
title: FlexConfirmMailのデバッグログ採取手順
author: yashirot
tags:
- free-software
- windows
---

屋代です。

サポートサービスご契約のお客様より問い合わせをいただいた際、現象の詳細を把握するためにデバッグログの採取をお願いすることがあります。
[FlexConfirmMail](https://github.com/FlexConfirmMail)は自由にお使いいただけるソフトウェアですので、デバッグログ採取の手順を公開しておきます。

<!--more-->

## Thunderbird版での手順
次の手順で採取します。

1. メイン画面のメニューバーから「アドオンとテーマ」を選択し、「アドオンマネージャー」タブを開く。
2. アドオンマネージャー上でFlexConfirmMail右側の工具アイコンをクリックし、「アドオンのオプション」を開く。
3. 「FlexConfirmMailの設定」画面を最下部までスクロールし、「デバッグモード」のチェックをONにする。
4. メイン画面のメニューバーから「アドオンとテーマ」を選択し、「アドオンマネージャー」タブを開く。
5. 「拡張機能の管理」右側の歯車アイコンをクリックし、開かれたメニューから「アドオンをデバッグ」を選択する。

![Thunderbird版・デバッガー画面]({% link /images/blog/steps-to-get-debug-logs-of-FlexConfirmMail/Thunderbird-debugger.png %} "Thunderbird版・デバッガー画面")

6. 開かれたタブの左のリストから「このThunderbird」を選択する。
7. 右画面の「拡張機能」配下の「FlexConfirmMail」を探し、[調査] ボタンを押す。
8. 「開発ツール - FlexConfirmMail」というタイトルのウィンドウまたはタブが開かれるので、「コンソール」を選択する。
9. この状態で現象を再現させる。
10. 8のコンソールに出力された内容を採取する。

デバッグログの採取状態においては、FlexConfirmMailの画面上に細かい枠線が表示される状態となります。

## Outlook版での手順
Outlook版について現在採取できるログ情報は比較的簡易なものです。次の手順で採取します。

1. メイン画面の「ホーム」から、送信チェック「FlexConfirmMail設定」を開く。

![Outlook版・リボン抜粋]({% link /images/blog/steps-to-get-debug-logs-of-FlexConfirmMail/Outlook-ribbon-bassui.png %} "Outlook版・リボン抜粋")

2. 「このアドオンについて」タブを選択する。

![Outlook版・FlexConfirmMail設定]({% link /images/blog/steps-to-get-debug-logs-of-FlexConfirmMail/Outlook-settei.png %} "Outlook版・FlexConfirmMail設定")

3. 「動作ログ」内の全文をコピー＆ペーストして、テキストエディタ等に貼り付ける


FlexConfirmMailを使っていて「あれ？」と思う現象に遭遇したら、ご協力いただけますと幸いです。
