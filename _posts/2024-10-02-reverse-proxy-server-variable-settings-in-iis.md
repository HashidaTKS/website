---
title: 'IISでリバースプロキシ（転送するヘッダの設定）'
author: abetomo
tags:
- windows
---

IISでリバースプロキシの設定をしている阿部です。

先日、[IISでリバースプロキシの設定をする方法]({% post_url 2024-09-25-reverse-proxy-with-iis %})を説明しました。

今回は少し進んだ設定方法としてヘッダ情報を転送する設定（nginxの`proxy_set_header`相当の設定）について説明します。
IISでリバースプロキシ設定で、バックエンドにヘッダ情報を転送したい方の参考になると思います。

<!--more-->

### 前提

[Grafana](https://grafana.com/ja/)を題材に設定例を説明します。

Grafanaでリバースプロキシの設定をするときは`Host`をバックエンドに転送する必要があります。
（CSRF対策のチェックの一環として必要です。）

参考: https://github.com/grafana/grafana/issues/45117#issuecomment-1033842787

この設定をしないとWeb画面でエラー表示が出ます。

![スクリーンショット：origin not allowed alert]({% link /images/blog/reverse-proxy-server-variable-settings-in-iis/origin-not-allowed-alert.png %})

ということで、このGrafanaでのヘッダ転送を例にIISでヘッダ情報を転送する方法を説明します。

### 準備: IIS

* [IISでリバースプロキシの設定をする方法]({% post_url 2024-09-25-reverse-proxy-with-iis %})は理解している
* **IISでWebSocket Protocolを有効にしている**
  * 設定手順: https://learn.microsoft.com/ja-jp/iis/get-started/whats-new-in-iis-8/iis-80-websocket-protocol-support#step-by-step-instructions
    * ドキュメントの通りポチポチ押してインストールして有効にします
  * 本題のヘッダ情報の転送には無関係です。設定例に使うGrafanaが利用するので有効にします
  * WebSocket Protocolを有効にさえしていれば、リバースプロキシの設定時に追加の設定などは必要ありません

### 準備: Grafana

Grafanaのインストールや設定についての詳細は割愛します。関連する部分のみ説明します。

例として使うGrafanaはv11.2.0です。

#### Winodwsにインストール

参考: https://grafana.com/docs/grafana/latest/setup-grafana/installation/windows/

書いてある通りにZIPファイルをダウンロードして展開すれば完了です。

#### リバースプロキシに関連する設定: ドメイン・URL

ZIPファイルを展開すると`conf/sample.ini`があるはずなので、`conf/custom.ini`というファイル名でコピーし、それを編集します。
変更点は次のの2つです。

```diff
@@ -41,7 +41,7 @@
 ;http_port = 3000

 # The public facing domain name used to access grafana from a browser
-;domain = localhost
+domain = "iis.example.clear-code.com"

 # Redirect to correct domain if host header does not match domain
 # Prevents DNS rebinding attacks
@@ -49,7 +49,7 @@

 # The full public facing url you use in browser, used for redirects and emails
 # If you use reverse proxy and sub path specify full url (with sub path)
-;root_url = %(protocol)s://%(domain)s:%(http_port)s/
+root_url = %(protocol)s://%(domain)s/grafana

 # Serve Grafana from subpath specified in `root_url` setting. By default it is set to `false` for compatibility reasons.
 ;serve_from_sub_path = false
```

* domain
  * 例で `iis.example.clear-code.com` を設定しています
  * 環境に合わせて適宜設定してください
* root_url
  * `/grafana` というパスで運用する設定です

#### リバースプロキシに関連する設定: CSRF

**詳細は後述しますが、大事な設定です！**

```diff
@@ -403,7 +403,7 @@
 ;csrf_trusted_origins = example.com

 # List of allowed headers to be set by the user, separated by spaces. Suggested to use for if authentication lives behind reverse proxies.
-;csrf_additional_headers =
+csrf_additional_headers = X-Forwarded-Host

 # The CSRF check will be executed even if the request has no login cookie.
 ;csrf_always_check = false
```

`csrf_additional_headers` で指定したヘッダでチェックするようになります。この例では`X-Forwarded-Host`です。

### IISの「URL 書き換え」でヘッダ情報の転送設定

#### リバースプロキシの基本設定

[リバースプロキシの設定]({% post_url 2024-09-25-reverse-proxy-with-iis %}#2.-%E5%9F%BA%E6%9C%AC%E7%9A%84%E3%81%AA%E3%83%AA%E3%83%90%E3%83%BC%E3%82%B9%E3%83%97%E3%83%AD%E3%82%AD%E3%82%B7%E3%81%AE%E8%A8%AD%E5%AE%9A)の基本的な設定は済んでいるものとして、追加で必要な設定について記載します。

このあと登場する設定例では次の設定をした例です。

* `grafana` というアプリケーションを追加
* URLの書き換え（L）: `http://localhost:3000`

#### 「許可されたサーバー変数」設定

散々「ヘッダ情報の転送」と書いてきましたが、IISの「URL 書き換え」では「サーバー変数」を設定するかたちで実現します。
ですので、ヘッダの設定ではなくサーバー変数を設定します。

設定するにあたり、まずは設定を許可するサーバー変数を登録する必要があります。

まずは`grafana`アプリーケーションにある「URL 書き換え」の設定画面を開きます。右側にある「操作」の「サーバー変数の表示」をクリックします。

![スクリーンショット：サーバー変数の表示]({% link /images/blog/reverse-proxy-server-variable-settings-in-iis/view-server-variables.png %})

「許可されたサーバー変数」の一覧が表示されます。右側にある「操作」の「追加」から`HTTP_X_FORWARDED_HOST` を追加します。

![スクリーンショット：許可されたサーバー変数]({% link /images/blog/reverse-proxy-server-variable-settings-in-iis/allowed-server-variables.png %})

* キャプチャは`HTTP_X_FORWARDED_HOST`を追加した後のものです
* 「サーバー変数の追加」画面で「サーバー変数名」を入力すると候補が表示されます。そこに`HTTP_X_FORWARDED_HOST`は表示されませんが追加して問題ありません
* 上述の通りヘッダではなくサーバー変数を設定するので、`X-Forwarded-Host`ではなく`HTTP_X_FORWARDED_HOST`を設定します
  * Grafanaの設定で設定した `csrf_additional_headers` と対応している設定です

#### 「サーバー変数」設定

「許可されたサーバー変数」設定が済んだら「URL 書き換え」の設定画面から、基本的なリバースプロキシの設定をした「受信規則の編集」画面を開きます。

その画面で「サーバー変数」の設定をします。「サーバー変数」のメニューが閉じていたら開いて、「追加」から追加します。

「追加」をクリックすると「サーバー変数の設定」画面が開くので設定します。

![スクリーンショット：サーバー変数設定: 追加]({% link /images/blog/reverse-proxy-server-variable-settings-in-iis/add-server-variables.png %})

* サーバー変数名
  * `HTTP_X_FORWARDED_HOST`
  * 「許可されたサーバー変数」先ほど追加した変数名
* 値
  * `{HTTP_HOST}`
  * ユーザが送信した`Host`

追加すると次の画面になるので、右側の「操作」メニューの「適用」をクリックして変更内容を反映します。

![スクリーンショット：サーバー変数設定]({% link /images/blog/reverse-proxy-server-variable-settings-in-iis/set-server-variables.png %})

以上で「サーバー変数」（ヘッダ情報）を転送する設定は完了です。

#### 補足: `HTTP_HOST` を転送すれば良いのでは？

[上述した参考リンク](https://github.com/grafana/grafana/issues/45117#issuecomment-1033842787)の通り、`HTTP_HOST`が転送できればnginxやApacheと同様の設定で単純だったのですが、IISの「URL 書き換え」の「サーバー変数」設定では `HTTP_HOST` の上書きはできないようでした。

ということで、Grafana側で`X-Forwarded-Host`を利用する設定（`csrf_additional_headers`の設定）とIISの「サーバー変数」設定で `HTTP_X_FORWARDED_HOST` の設定をして、CSRF対策のチェックが成功するようにしました。

### まとめ

IISでGrafanaのリバースプロキシの設定をすることを例にIISで「サーバー変数」（ヘッダ情報）を転送する設定を紹介しました。
`HTTP_HOST`の他にも上書きできないサーバ変数があるようでしたので、設定の際は動作確認をしながら設定してください。
