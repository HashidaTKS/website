---
title: Thunderbird 128でIMAPアカウントのメールフォルダーが破損する・増殖する場合の回避方法
author: piro_or
tags:
  - mozilla
toc: true
--- 

当社の法人向けThunderbirdサポートサービスにおいて、それぞれ別々のお客さまから、以下のようなお問い合わせを頂きました

* Thundebrird 115からThunderbird 128へ更新して以後、IMAPで運用しているメールアカウントにおいて、*フォルダー内のメールを選択するとメールのソースらしき文字列が表示されたり、別のメールの内容が表示されたりする*現象が、度々唐突に起こるようになった。「フォルダーの修復」を行うと状態が回復するが、しばらくするとまた再発する。
* アーカイブの保存方法を年単位から単一のフォルダーにまとめるように変更して以後、Thunderbirdを終了する度に「archive-1」「archive-2」「archive-3」……と、*名前の末尾に数字が付いたメールフォルダーが増殖していくようになった*。

本記事では、この2件の事例を切り口として、この種の問題の共通の原因と疑われるThunderbirdの事情を解説した上で、これらの問題の回避に有効と考えられる対策をご紹介します。

<!--more-->

### 事例1：メールフォルダーが破損する

1つ目のお問い合わせの事例について、お客さまにご提供頂いた情報に基づいて現象を再現した状態のスクリーンショットを示します。  
![（現象発生時の様子を再現したThunderbirdのスクリーンショット。スレッドペイン上では「社外秘のメール」というSubjectのメールが選択されているが、メッセージペインにはSubjectも送信者も表示されていない。メールの本文としては、MIMEでの境界を示す文字列らしきものと、その後に続いて、Base64エンコードされたデータと思しき内容が表示されている。）]({% link /images/blog/thunderbird-mbox-corruption/corrupt.png %})  
この画像は、Thunderbirdのメイン画面でメールフォルダーを選択し、フォルダー内のメールのうち1つを選択した状態なのですが、本来であればスレッドペイン（中央）で選択されたメールに対応する内容がメッセージペイン（右）に表示されるところ、このスクリーンショットでは、

* メールのSubjectも送信者も表示されていない。
* 本文としては、MIMEでのboundary（データの境界を示す文字列）らしきものや、Base64エンコードされたデータと思しき文字列が表示されている。

と、明らかに異常な状態になっています。
お客さまによると、この状態は「フォルダーの修復」操作で解消されるものの、その後もまた何らかのきっかけで発生してしまうとのことでした。

当社のThundebirdサポートサービスにおいては類似のお問い合わせはありませんでしたが、[英語圏のユーザーコミュニティ「MozillaZine」のフォーラムには類似の現象の報告があります](https://forums.mozillazine.org/viewtopic.php?t=3124946)。
当該スレッドによるとBugzillaにも同様の報告が複数寄せられているようですが、[Thunderbird 122以降でのフォルダー破損問題のメタバグ](https://bugzilla.mozilla.org/show_bug.cgi?id=1890230)[^meta-bug]を見る限り、完全解決には至っていない様子です。

[^meta-bug]: [bugzilla.mozilla.org](https://bugzilla.mozilla.org/)ではすべての話題を「バグ」と呼んで管理していますが、ある1つの話題に関係する複数のバグがある場合に、それらの進捗をまとめて把握・管理する目的で登録されているバグを、特に「メタバグ」と呼びます。


### 事例2：メールフォルダーが増殖する

2つ目のお問い合わせの事例は、以下の状況で問題が起こるようになったというものです。

* 「アカウント設定」→特定のメールアカウント→「送信控えと特別なフォルダー」→「メッセージの保管」→「アーカイブオプション」で「単独のフォルダー」を選択して（初期状態は「年別のフォルダー」）、すべてのアーカイブ対象のメールを1つのフォルダー配下にまとめるようにした。それ以後、現象が起こるようになった。
* Thunderbirdを起動すると「archive-1」という名前のフォルダーができている。Thunderbirdを終了し、次に起動すると、「archive-2」という名前のフォルダーがさらにできている。Thunderbirdを起動する度に、末尾の数字が増えたフォルダーが増殖していく。

こちらのケースでは、「2021」「2022」などのように年別で複数のフォルダーに分かれていたアーカイブを単独のフォルダーにまとめた結果、アーカイブフォルダーが17GBほどの大きさになっているとのことでした。
Thunderbirdのトラブル事例として「メールフォルダーの最大サイズは4GBまでで、この上限を超えたために問題が起こった」事例がWeb上には多数報告されていたことから、お客さまはこの時点で原因を「『4GB制限』に抵触したせいで問題が起こっているのでは？」と推測されていました。
この「4GB制限」とは何かについては、後ほど詳しく述べます。


### 共通の原因「mbox形式」

一般論として、Thunderbirdにおいて1つ目の事例のような現象は、メールフォルダーの実体であるmbox形式のファイルと要約ファイルとの間で内容に不整合が生じたときに発生します。
この背景についてまず簡単に説明します。

#### mbox形式とは？

Thunderbirdの既定の状態では、受信したメールはローカル上で[*mbox形式*または*バークレー形式*とよばれる形式](https://www.loc.gov/preservation/digital/formats/fdd/fdd000383.shtml)で保存されています。
これは、個々のメールを「`From `で始まり、改行文字で終了する」テキストとして表現して、1つのメールフォルダーをそのようなテキストが連続するテキストファイルとして表現する[^mbox-format]ものです。
Thunderbirdのアカウント設定の画面で「サーバー設定」→「メッセージの保存」→「メッセージの保存先」にあるフォルダーを開くと、メールフォルダーと同じ名前の拡張子無しのファイルがあるのが見て取れますが、それがメールフォルダーの実体であるmboxファイルです。

[^mbox-format]: 実際のmboxファイルの内容は`From ...1通目のメール...（改行文字）From ...2通目のメール...（改行文字）From ...3通目のメール...（改行文字）`のような形になり、全体を「`（改行文字）From `で区切られたテキストファイル」と見なすこともできます。

mbox形式は情報の検索性が悪いため、Thunderbirdでは個々のメールの「送信者、宛先、Subjectの内容」「既読・未読の状態」「そのメールの位置情報（ファイルの先頭から何バイト目から始まっているか）」などを「要約（サマリー）」として収集・保存して、*メールの一覧を表示するような場面では要約の方を参照する*ように設計・実装されています。

この要約ファイルと、mboxファイルとの間に何らかの理由で不整合が生じると、「3054バイト目からメールが始まっているはずなのに、実際にその位置のデータを取得してみたら、添付ファイルの途中の位置だった」といったことが起こり、先のスクリーンショットのように、*想定と異なる内容が画面上に表示される*ことになります。
それを解消するのが「*フォルダーの修復*」操作で、修復が指示されると、Thunderbirdは今ある要約ファイルを破棄して、IMAPアカウントの場合はメールサーバーから取得し直したフォルダー内のメールの情報を、POP3アカウント等であれば今あるmbox形式の元データを情報ソースとし、要約情報を再構築します。

また、mbox形式においては個々のメールの削除は論理削除[^logical-delete]になるため、削除されたメールが増えれば増えるほどファイル内にゴミデータが蓄積されていくことになります。
そのためThunderbirdは、「ファイルを先頭からスキャンして、削除済みの印が付いていないメールのデータだけを取り出して、それらだけを新しいmboxファイルに保存する」ということを行う機能を持っています。
これが「*フォルダーの最適化*」という操作で、Thundebrirdはゴミデータの総量が一定水準を超えた際に自動的に最適化を行うようになっています。

[^logical-delete]: データを実際に消すのではなく、データのメタ情報定義領域にある「削除済み」かどうかを示す値を変更して、データが削除されたものとして扱う方式。

#### 事例1の現象発生時の状況の詳細

1つ目の事例について、現象発生時と「フォルダーの修復」実行後のそれぞれのmboxファイルと要約ファイルを採取して頂いたところ、やはりお客さまの環境では、現象発生時には要約との間での不整合が発生していることが分かりました。
ただ、その状況は「一部が破損している」というような生易しいものではなく、現象発生時のmboxファイルは完全に破損した状態になっていました。頂いた情報を元に再現した例示用のmboxファイルに基づいて破損の仕方を示したものが、以下の図です。  
![（現象発生時のmboxファイルの内容を示した図。MIME境界らしき文字列を挟んでAとBという特定のパターンのデータが繰り返し登場している。）]({% link /images/blog/thunderbird-mbox-corruption/mbox.png %})

先述した通り、IMAPアカウントで「フォルダーの修復」を実行すると、メールサーバーからフォルダー内のすべてのメールの情報が再取得されます。修復操作で現象が解消されるのは、この破損したmboxファイルの内容が完全に破棄されて、メールサーバー側にある正しいメールの情報で置き換わるためだと考えられます。

他方、「なぜmboxファイルがこのような破損の仕方をするのか」については、明確な原因はまだ不明ですが、以下の根拠から、前述の*最適化処理が原因である可能性が高い*と考えられます。

* 最適化処理では「元のmboxファイルの何バイト目から何バイト目を新しいファイルに出力する」ということが行われるが、この時の読み出し開始位置や終了位置が適切に更新されないままループ処理が進むと、このような処理結果になり得る。
  * MIMEにおいてデータの境界となる文字列は都度ランダムに決定されるが、破損後のmbox形式では、すべての部分のデータ境界が同じ文字列となっている。これは、すべての部分が単一の元データに基づいていること、意図しないループ処理が行われたことを示唆する。
* 破損後のmboxファイルに含まれる、元々は添付ファイルの一部だったと思われるデータが、修復後のmboxファイルには含まれていない。
  * 全く別のメールフォルダーにあるメールのデータが混入することは考えにくく、破損後のmboxファイルに含まれているのは、フォルダー内に元々あった論理削除されたメールの残骸であったと考えられる。
* Thunderbirdはフォルダーの最適化を自動実行するが、「最適化が終了するまでは元のmboxファイルを使うので問題無く動作し、最適化が完了した後で初めて破損状態のmboxファイルを使うようになって問題が表面化する」、というタイムラグがある。そのため、「この操作をしたらフォルダーが壊れた」といった因果関係がユーザーからは見えにくく、「突然壊れる、再現条件が分からない」という状況になりやすいと考えられる。
* [Thunderbird 128のリリースノート](https://www.thunderbird.net/en-US/thunderbird/128.0esr/releasenotes/)には記載が無いが、先のMozillaZineフォーラムの情報によると、[Thunderbird 122（開発版）の時点でmbox形式の処理部分に行われた変更](https://bugzilla.mozilla.org/show_bug.cgi?id=1719121)がきっかけとなり、mbox形式に関する処理で様々な後退バグが報告されている。
  * 最適化処理はmbox形式向けに固有の機能のため、この変更の影響を受ける可能性がある。

なお、お客さまからのお問い合わせはIMAPアカウントについてで、MozillaZineのフォーラムに記載がある事例もIMAPアカウントでの運用である模様ですが、mboxファイルの最適化処理に問題がある場合、ファイルの破損はPOP3アカウントやニュースグループアカウントなどでも発生する可能性は否定できません。

#### mbox形式の4GB制限

2つ目の事例では、お問い合わせの時点ですでに、お客さま側で「4GB制限」への抵触の可能性を疑っておられました。
この「4GB制限」とは、mbox形式の仕様と、32bitアプリケーションとファイルシステムの制約によって過去に発生していた問題です。

32bit整数で表せるデータ量の上限は、約4GB[^max-4gb]です。
前述したとおり、mbox形式では1つのメールフォルダーが1つのファイルとして表現されますが、32bitアプリケーションで素直に実装すると、ファイル内の特定のメールの先頭位置を示すだけでも32bitの範囲でしか表現できず、それを超える位置にあるメールは読み書きできないことになります。
また、同様の理由から、FAT32などのファイルシステムではファイルサイズの上限が約4GBとなっていました。
これらのことが原因で、かつてはThunderbirdにおいても、mbox形式のメールフォルダーの最大サイズは約4GBに制限されていました。

[^max-4gb]: 正確には4.3GiB。

ですが、現在のThunderbirdではこの制約はすでに解消されています[^over-4gb]。
また、聞き取りの結果では、Thunderbirdのユーザープロファイルが置かれているストレージはNTFS[^max-filesize-in-ntfs]の物をお使いとのことでした。
よって、Thunderbirdとしても実行環境としても、このお客さまは原則として「4GB制限」の影響を受けない状況です。

[^over-4gb]: 「設定」→「一般」→「詳細設定」で `mailnews.allowMboxOver4GB` の値が `true` になっていれば、少なくともThunderbird側では、4GBを超えるサイズのメールフォルダーを扱える状態と分かります。お客さまの環境でも、こちらの設定は `true` となっていることをご確認頂けました。
[^max-filesize-in-ntfs]: NTFSでは1ファイルあたりの最大サイズは16EiB（エクスビバイト）となっています。


#### 事例2の真の原因

以上の事を踏まえ、検証環境で約17GBの巨大なメールフォルダーを用意して再現を試みた所、そのようなフォルダーを対象として最適化が行われた際に、お問い合わせのような現象を再現することができました。
また、その現象がmboxファイルの最適化処理の仕様に起因するものであることも分かりました。

mboxファイルの最適化は、通常、以下のように進行します。

1. mboxファイルに無駄な領域が生じているメールフォルダーを検出し、最適化処理を開始する。
   * 初期状態ではThunderbirdでは、この時点で「メールフォルダーの最適化を行うかどうか」をユーザーに尋ねてくる。拒否した場合、最適化は行われない。
   * そこで「以後確認せずに自動的に最適化を行う」という選択を行った場合、それ以後は明示的な確認無しにバックグラウンドで最適化が開始されるようになる。
2. 元となるmboxファイルと同じ位置に、ファイル名の末尾に `-1` のような接尾辞を付けた空のファイルを、一時ファイルとして用意する。
   * 「アーカイブ」フォルダーは国際化のために、内部的には `archive` という名前になっていて、表示上だけ翻訳されている。最適化のために作成される一時ファイルは元のファイル名の方に基づいて命名されるため、一時ファイル名は `archive-1` のようになる。
   * Thunderbirdはプロファイルフォルダー内のファイルを能動的にはスキャンしないため、この時点では、作成された一時ファイルは「ユーザープロファイル内に存在するmboxファイルだが、Thunderbirdからはメールフォルダーとしては認識されていない」状態となっている。
3. 元のmboxファイルの内容を先頭から走査開始する。
4. メールのデータを見付けた場合、そのメールに削除済みの印が付いていれば、読み飛ばす。
   削除済みの印が付いていなければ、3で作成した一時ファイルに、読み取ったメールのデータを追記する。
5. 4の処理を、元のmboxファイルの末尾に到達するまで繰り返す。
6. 元のmboxファイルの末尾に到達したら、元のmboxファイルを削除すると同時に、2で作成した一時ファイルの名前から接尾辞（`-1` のような部分）を取り除く。
   * これにより、メールフォルダーに対応するmboxファイルが「削除済みメールによって無駄な領域が生じている状態の物」から「無駄な領域がない状態の物」に差し替わる。

このとき、最適化対象のmboxファイルが大きく、最適化後にも多くのメールが残る状態だと、読み書きが高速なSSDを使用していてもなお4の処理には非常に長い時間を要する可能性があります。
最適化処理が進行中・未完了であることに気付かないままThunderbirdを終了すると[^exit-tb]、先の2で作成された一時ファイルが残ったままになってしまいます。
そして、その状態でThunderbirdを起動すると、以下のようなことが起こります。

[^exit-tb]: 進行中の最適化処理が終わるまではThunderbirdのプロセスは終了しませんが、その状態でWindowsを終了・再起動したり、あるいは、Thunderbirdを再度起動しようとして既存プロセスが検出された際に、既存プロセスを強制終了することを選択したりすると、最適化処理が未完了のままで一時ファイルが残されることになります。

1. `～-1` のような一時ファイルを、メールフォルダーのmboxファイルとして認識し、フォルダーツリー上にメールフォルダーとして表示する。
2. mboxファイルに無駄な領域が生じているメールフォルダーを検出し、最適化処理を開始する。
   * `～-1` という名前のファイルはすでに存在していて、メールフォルダーとして認識されているため、連番を増やして `～-2` という名前で一時ファイルを作成する。
3. 以降、通常と同じ流れで最適化が進行する。

このとき元のmboxファイルは、無駄な領域があり最適化が必要な状態のままですので、Thunderbirdを起動する度に毎回「最適化が必要」と判定され、最適化処理が始まってしまいます。
これが繰り返されると、`～-3`、`～-4` といった要領で、似た名前の連番のメールフォルダーがThunderbirdを起動する度に認識されていくことになります。
これが、事例2における「メールフォルダーが増殖する」現象の正体であると考えられます。


### 共通の回避策「maildir形式」

以上の調査結果から、これらの2つのお問い合わせの現象の直接の発生原因は、mboxファイルの最適化処理にあると考えられます。

1つ目の事例については、mboxファイルの最適化処理の未修正の不具合が原因と疑われますが、そのような不具合が表面化するきっかけになり得る要素は無数にあり[^possible-triggers]、「最適化に失敗しないようにする」のは現実的には不可能と言わざるを得ません。
また、2つ目の事例についても、メールフォルダーに格納されているメールの件数が多かったりサイズが極端に大きかったりする状況では、より高速なストレージを使うようにするか、フォルダーにあるメールを削除して物理的にサイズを縮小する以外には、根本的な解決は難しいです。

[^possible-triggers]: フォルダーに格納されているメールの数や内容・状態などのThunderbird内の要素の他、OSのバージョンやシステムの負荷状況などの外部要因も考えられます。

よって、いずれにしても、対策は対症療法的なものに限られます。
具体的には以下の2つの対策が考えられます。

1. フォルダーの自動最適化を停止する。
2. フォルダーの最適化が起こらない（必要ない）ようにする。

1は、Thunderbirdの設定画面の「一般」→「ネットワークとディスク領域」→「ディスク領域を合計○○MB以上節約できるときはフォルダーを最適化する」のチェックをオフにするというものです。
ただし、この状態では論理削除されたメールが残留し続けるため、mboxファイルが肥大化していく一方になるというデメリットがあります。

2は、*メールのローカルでの保存形式をmbox形式からmaildir形式に切り替える*というもので、お勧めの選択肢です。
「maildir形式」は、個々のメールを1つのファイルとして表現する方式です。メールの削除はファイルの削除となるため、フォルダーの最適化という概念がそもそもなく、最適化がきっかけで起こる問題とは完全に無縁になれます。

#### maildir形式のデメリット

お薦めの対策であるmaildir形式への切り替えですが、ファイルの数がメールの件数分だけ大幅に増えるため、運用次第ではそれに起因するトラブルも懸念されます。

例えば、現在Windowsで現在広く採用されているNTFSにおいては、ファイル数の上限が約43億個となっています。
企業において、ThunderbirdのユーザープロファイルをWindowsの移動プロファイルの一部として管理する場合や、全ユーザーのユーザープロファイルをWindowsのファイル共有サーバー上に置いてネットワークドライブとしてマウントさせるなどの形で運用する場合には、この上限を全ユーザーで分け合う形となります。
仮にユーザーが2万人いる場合、1ユーザーあたりに許容されるメールの最大件数は平均で20万件程度となり、業務の状況によっては現実的に支障が生じ得るかもしれません。

また、小さくても多数ファイルがある状況では、ファイルのコピーや読み書き時のオーバーヘッドが大きくなる恐れがあり、以下のような影響が懸念されます。

* Windowsへのログオン時に移動プロファイルのダウンロードに時間がかかるようになる。
* ネットワークドライブ上にあるプロファイルを使用する場合に、Thunderbird上でのメールの検索に時間がかかるようになる。

ただ、*ローカルディスク上に恒常的に置かれたプロファイルを使用する運用であれば、これらの点は特に問題にならない*でしょう。
実例として、筆者はThunderbird 115の頃より、合計20万件以上のメールがあるIMAPアカウントの保存形式をmaildir形式に切り替えて使用していますが、プロファイルをSSD上に置いていることもあってか、特に支障なく安定的に運用できています。


#### mbox形式からmaildir形式への切り替え手順

Thunderbirdにおいてmbox形式で運用中のメールアカウントのデータをmaildir形式に切り替えるには、若干の手間を要します。
複数のやり方がありますので、それぞれメリットとデメリットを踏まえつつご紹介します。

##### Thunderbirdが提供する移行機能で移行する

現行版のThunderbirdは、メールアカウントごとにメールの保存形式をmbox形式からmaildir形式に変更する機能を含んでいます。
ただ、安定度などの懸念からか、Thunderbird 128時点では初期状態で機能が封印されています。
機能を解放するには、Thunderbirdの設定画面で「一般」→「ネットワークとディスク領域」→「設定エディター」を選択して、開かれた設定エディターで `mail.store_conversion_enabled` を検索して値を `true` に切り替える必要があります。

設定を変更した状態で「アカウント設定」→形式を変更したいIMAPアカウントの「サーバー設定」→「メッセージの保存」→「メッセージの格納形式」を確認すると、通常はUIが無効状態になっていて切り替えられなかった所が、ドロップダウンリストで値を切り替えられるようになります。
ここで「メッセージ単位（maildir形式）」を選択するとThunderbirdの再起動を求められますので、処理を継続して、変換が完了しThunderbirdが再起動されれば、切り替えは完了となります。

この方法のメリットは以下の通りです。

* テーブルビューでのスレッド表示のON/OFFやカラムの並び順など、Thunderbirdがローカルにのみ保持している情報も引き継ぐことができる。
* メールのデータは、必要無い限り再取得しないので、*ネットワーク帯域の消費が少なく済む*。
* *ThunderbirdのGUI上で操作が完結するため、あまり技術に明るくない人でも安心して実行できる*。

他方、デメリットは以下の通りです。

* 変換元となる元のmbox形式のデータがそのままプロファイル内に残るため、*空きディスク容量が多く必要である*。
  * 例えば、元々のメール保存先がプロファイル内の `ImapMail/imap.gmail.com/` であった場合、maildir形式のデータは `ImapMail/imap.gmail.com-maildir/` に置かれるようになり、mbox形式の `ImapMail/imap.gmail.com/` はそのまま残されている。ディスク容量を節約したい場合、この古いデータは手動操作で削除する必要がある。
* ThunderbirdのUI上での操作が必要なため、企業などにおいてシステム管理者側での一括実施はできない。
* 初期状態では封印されている機能を使用するため、通常の機能の使用に比べて、不具合の影響を受ける危険性がある。



##### メールを再取得して移行する

IMAPでは、すべてのメールは原則としてメールサーバー上にあり、ローカルにある物はキャッシュのような扱いです。
Thunderbirdが終了している状態でローカルのメールを削除しておくと、次回Thunderbird起動時にメールのデータが自動的に再ダウンロードされます。
この性質を利用すると、変換機能を使用せずにメールの保存形式を切り替えることができます。
具体的な手順は以下の要領です。

1. Thunderbirdを終了する。
2. Thunderbirdのユーザープロファイルフォルダーを開く。
3. ユーザープロファイル内の `ImapMail` フォルダー配下の各IMAPサーバーに対応するフォルダー内から、*以下のファイルを残して、それ以外をすべて削除する*。
   * `msgFilterRules.dat`（メッセージフィルターのデータ） など、拡張子が `.dat` であるファイル
   * 拡張子が `.msf` であるファイル（要約ファイル）
   * 名前の末尾が `.sbd` であるフォルダー
4. ユーザープロファイル内の `prefs.js` をテキストエディター等で編集する。
   1. 文字エンコーディングUTF-8としてファイルを開く。
   2. ファイル内で `.type", "imap");` で終わる行をすべて検索する。
      * このとき `user_pref("mail.server.server1.type", "imap");` のような行が見付かるが、この `server1` の部分が受信サーバーの識別子となるので、後で参照できるように識別子を控えておく。
   3. 前項で控えた識別子を用いて、`mail.server.(識別子).storeContractID` という形式の文字列を含む行をすべて検索する。
   4. `user_pref("mail.server.(識別子).storeContractID", "@mozilla.org/msgstore/berkeleystore;1");` のような行が見付かるので、
      `@mozilla.org/msgstore/berkeleystore;1`
      の部分を
      `@mozilla.org/msgstore/maildirstore;1`
      に書き換える。
   5. 編集を追えたファイルを上書き保存する。
5. Thunderbirdを起動する。

以上の手順で、 ThunderbirdがIMAPサーバーからメールを再ダウンロードし、ローカルにmaildir形式で保存してくれるようになります。
また、以上のような操作を行うPowerShellをログオン時に自動実行させるなどのようにすれば、システム管理者側でmaildir形式への移行を強制することも可能です。

この方法のメリットは以下の通りです。

* ローカルに無駄なデータが残らないので、*ディスク領域に余裕があまりなくてもよい*。
* テーブルビューでのスレッド表示のON/OFFやカラムの並び順など、Thunderbirdがローカルにのみ保持している情報も引き継ぐことができる。
* （仕組みを作れば）管理者側での一括実施が可能である。

他方、デメリットは以下の通りです。

* *設定ファイルの編集を伴うため、操作ミスなどで致命的な状況に陥る危険性がある*。
* メールフォルダーの数が多い場合、削除するべきファイルと残すべきファイルとの識別に手間がかかる。
  * Thunderbirdがローカルにのみ保持している情報は引き継がなくてもよければ、`ImapMail` 配下から `msgFilterRules.dat` 以外のファイル・フォルダーをすべて削除する形としてもよい。
* すべてのメールのデータが再取得の対象になるため、ネットワーク帯域の消費が（一時的に）増える。

なお、この方法は筆者がmboxからmaildirへ移行する際に採用したものです。筆者の場合は、メールフォルダーごとにローカルで保存されている情報は失われてもよかったため、`msgFilterRules.dat` を残してすべてのmboxファイルと要約ファイルを削除しました。


##### アカウントを再作成して移行する

前2者はいずれも元々のメールアカウントの情報を活かす方法ですが、PCの新調時のようにまっさらな状態で移行する方法もあります。具体的には以下の要領です。

1. Thunderbirdの（アカウント設定ではなく、全般的な動作の）設定画面を開く。
2. 「一般」→「ネットワークとディスク領域」→「索引データベース」→「新しいアカウントのメッセージ格納形式」で「メッセージ単位（maildir形式）」を選択する。
3. 新規にIMAPメールアカウントを作成する。
4. 作成したIMAPメールアカウントについて、受信サーバーなどの必要な設定を行ったり、メッセージフィルターなどの各種設定を手作業で設定し直したりする。
5. Thunderbirdのアカウント設定画面を使用し、mbox形式でメールを保存していた古いIMAPメールアカウントを削除する。

この方法のメリットは以下の通りです。

* 既存のデータを使わないため、*移行処理に起因するトラブルをすべて回避できる*。

他方、デメリットは以下の通りです。

* *メールアカウントの諸々の設定をまっさらな状態からやり直すことになり、煩雑である*。
* すべてのメールのデータが再取得の対象になるため、ネットワーク帯域の消費が（一時的に）増える。
* ThunderbirdのUI上での操作が必要なため、企業などにおいてシステム管理者側での一括実施はできない。



### まとめ

以上、Thunderbird 128における「IMAPアカウントのメールフォルダーが破損する」「メールフォルダーが増殖する」といったトラブルの原因となっている（と考えられる）背景事情と、問題の回避策としてのメール保存形式のmaildir形式への変更手順をご紹介しました。

株式会社クリアコードは、お客さまからのお問い合わせに基づいて、オープンソース製品であるThunderbirdのソースコードまでも対象にした調査や、トラブルの解決方法のご提案などを行うサポートを、有償にてご提供しています。
Thunderbirdの運用でお困りのことがある企業のシステム管理・運用ご担当者さまは、是非とも[お問い合わせフォーム]({% link contact/index.md %})よりご連絡下さい。
