# -*- ruby -*-

require "date"
require "pathname"
require "yaml"

class Linter
  class Post
    attr_reader :path
    attr_reader :id
    attr_reader :date
    attr_reader :content
    def initialize(path)
      @path = path
      @id = detect_id
      @date = detect_date
      @content = read_content
    end

    def draft?
      @path.directory.basename.to_s == "_drafts"
    end

    private
    def detect_id
      @path.basename(".md").to_s.sub(/\A\d{4}-\d{2}-\d{2}-/, "")
    end

    def detect_date
      basename = @path.basename(".md").to_s
      case basename
      when /\A(\d{4})-(\d{2})-(\d{2})/
        Date.new($1.to_i, $2.to_i, $3.to_i)
      else
        nil
      end
    end

    def read_content
      @path.read.force_encoding("UTF-8")
    end
  end

  def initialize
    @success = true
    @config = load_config
  end

  def lint
    lint_id
    lint_config
    lint_content

    raise "Failed to lint" unless @success
  end

  private
  def build_path(*components)
    File.join(__dir__, "..", *components)
  end

  def load_config
    YAML.load(Pathname(build_path("_config.yml")).read)
  end

  def posts
    current_path = Pathname.pwd
    Dir.glob(build_path("{_posts,_drafts}", "*.md")).collect do |path|
      Post.new(Pathname(path).relative_path_from(current_path))
    end
  end

  def lint_id
    ids = {}
    posts.each do |post|
      id = post.id
      next if id == "index"
      ids[id] ||= []
      ids[id] << post
    end
    ids.each do |id, posts|
      if posts.size > 1
        @success = false
        puts("Duplicated ID: #{id}:")
        posts.each do |post|
          puts("  #{post.path}")
        end
      end
    end
  end

  def lint_config
    lint_config_author_labels
    lint_config_tags
    lint_config_tag_labels
  end

  def lint_config_sorted(*keys)
    actual = @config.dig(*keys)
    if actual.is_a?(Hash)
      return if actual.keys == actual.keys.sort
    else
      return if actual == actual.sort
    end

    @success = false
    puts("Must sort #{keys.join(".")} in _config.yml:")
    actual_yaml_file = Tempfile.new(["lint-actual", ".yaml"])
    actual_yaml_file.puts(actual.to_yaml)
    actual_yaml_file.close
    expected_yaml_file = Tempfile.new(["lint-expected", ".yaml"])
    if actual.is_a?(Hash)
      expected = {}
      actual.keys.sort.each do |key|
        expected[key] = actual[key]
      end
    else
      expected = actual.sort
    end
    expected_yaml_file.puts(expected.to_yaml)
    expected_yaml_file.close
    system("diff",
           "-u",
           "--label=actual",
           "--label=expected",
           actual_yaml_file.path,
           expected_yaml_file.path)
  end

  def lint_config_author_labels
    lint_config_sorted("blog", "author_labels")
  end

  def lint_config_tags
    lint_config_sorted("blog", "tags")
  end

  def lint_config_tag_labels
    lint_config_sorted("blog", "tag_labels")
    tags = @config.dig("blog", "tags")
    tag_labels = @config.dig("blog", "tag_labels")
    return if tags == tag_labels.keys

    @success = false
    puts("blog.tags and blog.tag_labels must be synchronized in _config.yml:")
    missing_label_keys = tags - tag_labels.keys
    too_much_label_keys = tag_labels.keys - tags
    unless missing_label_keys.empty?
      puts(" blog.tag_labels misses keys:")
      missing_label_keys.sort.each do |key|
        puts("  #{key}")
      end
    end
    unless too_much_label_keys.empty?
      puts(" blog.tag_labels has too much keys:")
      too_much_label_keys.sort.each do |key|
        puts("  #{key}")
      end
    end
  end

  def lint_content
    posts.each do |post|
      unless post.content.valid_encoding?
        @success = false
        puts("Encoding must be UTF-8: #{post.path}")
        next
      end
      if post.content.include?("\r\n")
        @success = false
        puts("New line must be LF only: #{post.path}")
        next
      end
      lint_content_front_matter(post)
    end
  end

  MIGRATION_DATE = Date.new(2020, 12, 24)
  def lint_content_front_matter(post)
    unless post.content.start_with?("---")
      @success = false
      puts("Must have YAML front matter: #{post.path}")
      return
    end
    front_matter = YAML.load(post.content)
    lint_content_front_matter_author(post, front_matter)
    lint_content_front_matter_title(post, front_matter)
    lint_content_front_matter_tags(post, front_matter)

    lint_content_more(post)
    lint_content_embedded_contents_wrapper(post)
  end

  def lint_content_front_matter_author(post, front_matter)
    if post.date and post.date > MIGRATION_DATE
      unless front_matter.key?("author")
        @success = false
        puts("Must have <author> in YAML front matter: #{post.path}")
        puts(front_matter.to_yaml.gsub(/^/, "  "))
        return
      end
      author = front_matter["author"]
      author_labels = @config["blog"]["author_labels"]
      unless author_labels.key?(author)
        @success = false
        puts("Author label is missing in _config.yml: <#{author}>: #{post.path}")
        puts(author_labels.to_yaml.gsub(/^/, "  "))
        return
      end
    end
  end

  def lint_content_front_matter_title(post, front_matter)
    title = front_matter["title"] || ""
    if title.strip.empty?
      @success = false
      puts("Must have <title> in YAML front matter: #{post.path}")
      puts(front_matter.to_yaml.gsub(/^/, "  "))
      return
    end
  end

  def lint_content_front_matter_tags(post, front_matter)
    defined_tags = @config["blog"]["tags"]
    defined_tag_labels = @config["blog"]["tag_labels"]
    tags = front_matter["tags"] || []
    tags.each do |tag|
      unless defined_tags.include?(tag)
        @success = false
        puts("Tag is missing in _config.yml: <#{tag}>: #{post.path}")
        puts(defined_tags.to_yaml.gsub(/^/, "  "))
        return
      end

      unless defined_tag_labels.key?(tag)
        @success = false
        puts("Tag label is missing _config.yml: <#{tag}>: #{post.path}")
        puts(defined_tag_labels.to_yaml.gsub(/^/, "  "))
        return
      end
    end
  end

  def lint_content_more(post)
    unless /^<!--more-->$/.match?(post.content)
      @success = false
      puts("Must have <!--more-->: #{post.path}")
      return
    end
  end

  EMBEDDED_CONTENTS_WRAPPER_RULES = [
    {
      embedded: /<iframe[^>]+src="(?:https?:)?\/\/slide.rabbit-shocker.org\//,
      wrapper: /^<div class="rabbit-slide( rabbit-slide-wide)?">/,
      message: "Rabbit slides must be wrapped with <div class=\"rabbit-slide\">...</div> or <div class=\"rabbit-slide rabbit-slide-wide\">...</div>",
    },
    {
      embedded: /<iframe[^>]+src="(?:https?:)?\/\/www.slideshare.net\//,
      wrapper: /^<div class="slideshare(-wide)?">/,
      message: "SlideShare slides must be wrapped with <div class=\"slideshare\">...</div> or <div class=\"slideshare-wide\">...</div>",
    },
    {
      embedded: /<iframe[^>]+src="(?:https?:)?\/\/www.youtube.com\//,
      wrapper: /^<div class="youtube(-4x3)?">/,
      message: "YouTube contents must be wrapped with <div class=\"youtube\">...</div> or <div class=\"youtube-4x3\">...</div>",
    },
    {
      embedded: /<video/,
      wrapper: /^<div class="video-640x480">/,
      message: "Video elements must be wrapped with <div class=\"video-640x480\">...</div>",
    },
    {
      embedded: /<iframe[^>]+src="(?:https?:)?\/\/ext.nicovideo.jp\//,
      wrapper: /^<div class="nicovideo-thumbnail">/,
      message: "NicoNico video contents must be wrapped with <div class=\"nicovideo-thumbnail\">...</div>",
    },
  ]

  def lint_content_embedded_contents_wrapper(post)
    EMBEDDED_CONTENTS_WRAPPER_RULES.each do |rule|
      unless post.content.scan(rule[:embedded]).size == post.content.scan(rule[:wrapper]).size
        @success = false
        puts("#{rule[:message]}: #{post.path}")
      end
    end
  end
end

desc "Lint"
task :lint do
  linter = Linter.new
  linter.lint
end
