---
layout: default
main_title: クリアコードが、Open Source Summit Japan 2023にてログ収集ソフトウェアFluentdについて講演
sub_title: 2023年12月5日（火）14:00～
type: press
keywords:
---
<div class="press-release-signature">
  <p class="date">2023年12月4日</p>
  <p>株式会社クリアコード</p>
</div>
株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平）は、2023年12月5日（火）に、Linux Foundation主催のOpen Source Summit Japan 2023において、「New Chapter of Fluentd, Rebranding and New Release Cycle(LTS)」と題し講演します。

セキュリティ、マーケティングなどの場面で活用するために様々なフォーマットのデータを集め、必要な場所へ適切なフォーマットで出力することのできるFluentd[^1]は、高い拡張性を誇り様々な場面で活用されています。今回の登壇では、Fluentdリリースから10年以上のこれまでの歴史、パッケージ[^2]名称とサポートサイクル変更の取り組み、今後の展望などをクリアコードに所属するメンテナー福田大司郎と林健太郎が、英語で紹介します。



### 講演の概要

* 日時：2023年12月5日（火）14:00-14:40
* 場所：HALL B-2
* 講演タイトル：New Chapter of Fluentd, Rebranding and New Release Cycle(LTS)
* 講演者：
  * 福田大司郎(Fluentdメンテナー、ソフトウェアエンジニア)
  * 林健太郎(Fluentdメンテナー、ソフトウェアエンジニア)
* 公式概要ページ：https://sched.co/1Typb

### イベントに関する情報

Open Source Summit Japanは、日本で開催される大規模なカンファレンスで、オープンソースエコシステムが一堂に会します。技術者やオープンソースリーダー企業が、コラボレーションと情報共有のために、そして最新のオープンソース技術を学ぶために、あるいは革新的なオープンソリューションを使った競争力の付け方を見つけるために集結します。

Open Source Summitは、今日のオープンソースに影響を与える最も重要な技術、トピック、および問題をカバーするイベントの集まりで構成される、カンファレンスアンブレラです。(公式ページより抜粋)

* 開催期間：2023年12月4日（月）～6日（木）
* 会場：有明セントラルタワーホール＆カンファレンス
* 公式ページ：https://events.linuxfoundation.org/open-source-summit-japan/
* 登録料金：https://events.linuxfoundation.org/open-source-summit-japan/reg/register-ja/


## Fluentdとは
Fluentdは、拡張性の高いログ収集ソフトウェアです。オンプレミスでのデータ収集はもちろん、クラウドネイティブな環境においても、1000以上のプラグインで、様々なサービスとのデータ連携を実現し、ユーザーにとって利用しやすいデータ収集を実現します。Apache2.0ライセンスで提供され、コミュニティで開発が行われています。

Cloud Native Computing Foundation (CNCF)[^3]により、認定されたプロジェクトの一つです。

Fluentd公式ページ（英語）：https://www.fluentd.org/


## クリアコードについて

クリアコードは2006年にフリーソフトウェア開発者を中心に設立しました。

データ収集ソフトウェアFluentd、検索エンジンGroonga[^4]、データ処理基盤Apache Arrow[^5]といった様々なソフトウェアを通して、データ利用に関する様々な顧客依頼に対し、根本的な課題解決を実現させる受託開発や、ソースコードレベルのサポートを提供してきました。

また、フリーソフトウェアとビジネスの両立を目指し、継続的なメンテナンス・新規機能開発・開発者を増やす取り組みなどにも取り組んでいます。


### 参考URL

【コーポレートサイト】{{ site.url }}{% link index.html %}

【関連サービス】{{ site.url }}{% link services/index.md %}

## 当リリースに関するお問い合わせ先

株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com

[^1]:Fluentdは、米国及びその他の国におけるThe Linux Foundationの商標または登録商標です。
[^2]:パッケージとは、本体ソフトウェアとソフトウェアが動作するために必要な関連ソフトウェアや主要プラグインを各種プラットフォーム向けにまとめたものです。
[^3]:Cloud Native Computing Foundation（CNCF）は、Linux Foundationの組織として、オープンソースのクラウドネイティブなプロジェクトをベンダーの垣根を作らずに、より広く、より継続的にしていくためのサポートを提供しています。多くのプロジェクトがサポート受けており、プロジェクトの成長段階により、Sandbox、Incubating、Graduatingと分けられています。
[^4]:Groongaはオープンソースのカラムストア機能付き全文検索エンジンです。Groongaを使うことにより、高性能な全文検索機能を備えたアプリケーションを容易に開発することができます。Groonga公式ページ：https://groonga.org/ja/
[^5]:Apache Arrow, the Apache Arrow project logo are either registered trademarks or trademarks of The Apache Software Foundation in the United States and other countries.（Apache Arrowの商標およびロゴはApache Software Foundationの商標です。） Apache Arrowはこれからさらに必要とされている大規模データの処理をより高速にスムーズに行うため、データの処理・交換・変換等を行うモジュールで使えるデータ処理基盤を開発するプロジェクトです。特定のプログラミング言語に特化しておらず、多くのプログラミング言語で使えるようになっているため、複数の言語で実装されたデータ処理システムでも活用できます。Apache Arrow公式ページ（英語）：https://arrow.apache.org/
