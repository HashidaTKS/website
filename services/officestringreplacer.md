---
layout: default
main_title: OfficeStringReplacer(ファイル文字列検索・置換ツール)
sub_title:
type: service
---

## OfficeStringReplacerとは

OfficeStringReplacer は、クリアコードが開発したMicrosoft Office 製品各種[^1]のファイル内の文字列を検索・置換するツールです。

<div class='grid-x grid-padding-x'>
  <div class='cell large-shrink'>
    <iframe style='max-width:100%;' width="409" height="240" src="https://www.youtube.com/embed/QlWW2v8LZ44?si=kwLMNH7J43Nb_AXE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
  <div class='cell large-auto'>
  <p>近年、ペーパーレスへの移行やリモートワークの普及により、多くの企業がファイルサーバーなどで様々なドキュメント・ファイルを共有で保管しています。
  
  <p>さらに、近年のデータ量の増加に伴い、ファイルサーバーの移行なども行われることが多くあります。ファイルサーバーの移行や棚卸しに際して、莫大な量のファイルに対して、特定の語句の入ったドキュメントの検索や、文言の書き換えなどが必要となるケースがあります。こういったニーズに応える製品として、OfficeStringReplacerは開発されました。
  </div>
</div>

<table>
<caption></caption>
<tr>
<td>検索・置換が可能なファイル形式</td>
<td>
<ul>
<li>Excel:<code>.xlsx</code>, <code>.xlsm</code>, <code>.xls</code></li>
<li>Word :<code>.docx</code>, <code>.docm</code>, `<code>.doc</code></li>
<li>PowerPoint:<code>.pptx</code>, <code>.pptm</code>, <code>.ppt</code></li>
<li>Access:<code>.accdb</code>, <code>.mdb</code></li>
<li>link:<code>.lnk</code></li>
<p>
<p><b>制限事項</b><br>
   次のファイルはサポート対象外となります。
   <li>パスワードロックされたもの</li>
   <li>書き込み権限がないもの</li>
   <li>Accessマクロ</li>
   <li>ExcelのOfficeスクリプト</li>

</ul>
</tr>
<tr>
<td>想定する利用シーン</td>
<td>
<ul>
<li>ファイルサーバーの移行によりファイルのファイルパスが変更になった。ファイル内にある他のファイルへのリンクを特定ルールに基づいて書き換えたい。</li>
<li>会社名や部署名の変更に伴い、旧会社名や旧部署名が記載されている文書を探して、修正を行いたい。</li>
<li>特定の取引会社名の関わる文書を探して、引き継ぎたい。</li>
<li>「ＷＥＢサイト」と「WEBサイト」など表記ゆれがあるとき、適切な表記に統一したい。</li>
</td>
</tr>
</table>

## OfficeStringReplacer法人利用パックメニュー

OfficeStringReplacer法人利用パックは、OfficeStringReplacerを利用する法人ユーザーを対象に、OfficeStringReplacerのプログラム一式とサポートを提供するものです。お客様のニーズに合わせてサポート時間をお選びいただけます。

<table>
<caption>法人利用パック内容</caption>
<tr>
<td>OfficeStringReplacer一式</td>
<td>
<ul>
<li>OfficeStringReplacerのプログラム本体(ClearCode署名入り)</li>
<li>OfficeStringReplacer利用マニュアル</li>
<li>OfficeStringReplacerの問い合わせサポート</li>
</ul>
</td>
</tr>
<tr>
<td>OfficeStringReplacer技術サポート</td>
<td>
<ul>
<li>仕様に関する問合せ対応</li>
<li>OfficeStringReplacerの使用・設定方法に関する問合せ対応</li>
<li>障害発生時の原因調査と解決策の提案(※1)</li>
<li>不具合修正版やバージョンアップ版の無償提供</li>
<li>Office製品、OSのバージョンアップ時の動作検証</li>

<br>
<p><small>
(※1)障害発生時の原因調査と解決策の提案は契約毎に対応時間数の上限を定めています。対応時間数の上限を超える場合は別途お見積もりの上での対応となります。なお、障害の原因がOfficeStringReplacerの不具合であった場合はOfficeStringReplacerの改修のための作業時間は対応時間に計上しませんが、問題切り分けのための作業時間は対応時間に計上します。</small></p>
</ul>
</td>
</tr>
<tr>
<td>サポートお問合せ方法</td>
<td>
<ul>
<li>専用メールアドレスへの電子メールによる問い合わせを受け付けます。</li>
<li>問い合わせへの回答は電子メールによります。電話やオンサイトでのサポートには対応しません。</li>
</ul>
</td>
</tr>
<tr>
<td>サポート受付時間</td>
<td>
平日(※) 10:00から17:00
※年末年始、弊社が指定する休日を除く
</td>
</tr>
</table>

<table>
  <caption>OSR法人利用パック価格</caption>
  <thead>
  <td>価格(税別)</td>
  <td>サポート対応時間</td>
  <td>ユーザー数</td>
  </thead>
  <tbody>
  <tr>
  <td>￥1,200,000</td>
  <td>20時間まで</td>
  <td>無制限</td>
 </tr>
 <tr>
  <td>￥1,500,000</td>
  <td>30時間まで</td>
  <td>無制限</td>
 </tr>
 <tr>
  <td>￥2,000,000</td>
  <td>50時間まで</td>
  <td>無制限</td>
 </tr>
 <tr>
  <td>￥4,000,000</td>
  <td>無制限</td>
  <td>無制限</td>
 </tr>

  </tbody>
</table>
<ul>
<li>契約開始日は契約月の1日とします。</li>
<li>契約期間は1年とします。</li>
<li>契約更新されない期間が一定期間あり、再度契約をお申込みいただく場合は遡及にて対応します。</li>
<li>年間サポート時間をすべて消化した場合、サポート時間の追加が可能です。OSR法人利用パック（サポート時間追加）にて20時間を追加します。費用は600,000円です。追加したサポート時間の有効期間はもととなる契約期間と同じです。</li>
</ul>

## サービスに関する問い合わせ

OfficeStringReplacerについて詳しく知りたい方は、こちらの[お問い合わせフォーム]({% link contact/index.md %}) からお気軽にご連絡ください。製品ならびにサービスの紹介、デモの要望にも対応しております。

### 評価版について

お客様環境の検索/置換対象ファイルにおいて、OfficeStringReplacerが期待する動作をするのか、またOfficeStringReplacerの使用感を確認するため、評価版をお試しください。
評価版では検索機能のみ有効で、文字列の置換機能はご利用いただけません。

評価版のご利用に当たっては、こちらの [お問い合わせフォーム]({% link contact/index.md %})から、評価版のご利用希望及びご連絡先、お名前をお送りください。受け付け後、OfficeStringReplacer評価版申込書」をお送りいたしますので必要事項を記載の上、ご提出ください。

お問合せに1週間以内に返信がない場合は、お手数をおかけして恐縮ですが、改めてお問合せください。


[^1]:Microsoft Office, Access, Excel, PowerPoint, Word は、マイクロソフト コーポレーションの商標です

