---
layout: default
main_title: 国文学研究資料館様向けのGroongaサポートサービスで行った改良について紹介している動画が公開されました。
sub_title:
type: topic
---

2023年10月24日に、[大学共同利用機関法人 人間文化研究機構 国文学研究資料館様のYouTubeチャンネル](https://www.youtube.com/@NIJL)に、国書データベースの異体字検索の改良を取り上げた[Tech Talk動画](https://www.youtube.com/watch?v=sNwBKeyfBGk)が公開されました。動画には、改良を実際にサポートしたクリアコードの堀本と須藤も参加しています。

<div class="youtube">
  <iframe width="560"
          height="315"
          src="https://www.youtube.com/embed/sNwBKeyfBGk?si=L5gBLmMYoyddvrjc"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowfullscreen></iframe>
</div>

国書データベース https://kokusho.nijl.ac.jp/ は、2023年3月からサービスが始まった「日本古典籍総合目録データベース」（館蔵和古書目録データベースを含む）と「新日本古典籍総合データベース」を統合した新たなデータベースです。
詳しい情報は、https://kokusho.nijl.ac.jp/page/about.html を参照してください。

国書データベースは、検索対象が江戸時代以前の書籍であるため、旧字体など現代とは異なる字体であることも多く、異体字検索は重要な機能の1つですが、運用当初から異体字検索は遅く、場合によっては国書データベースが止まってしまうという課題がありました。

この課題を[PGroonga](https://pgroonga.github.io/ja/)を使って解決しました。本動画では、PGroongaを使ってどのように課題を解決したか、また解決した結果どのくらい検索が速くなったかについてわかりやすく解説しています。


## クリアコードとGroonga

クリアコードは2008年からGroongaの前身となる全文検索エンジンSennaの開発に参加し、その後、2010年の1.0.0のリリースからGroongaの開発・メンテナンスに参加しています。
これまで、Groongaは、レストラン検索サイト、医療機関検索サイト、不動産物件情報サイト等様々な「検索」が重要な場所で採用されています。

クリアコードでは、Groonga、PGroongaやMroongaといった関連プロジェクトに関わる導入サポートや、検索速度向上のための改良サポートなど[法人向けの各種サービス]({% link services/groonga.md %})を提供しています。






